#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# ------------------------------------------------------------------------------
# This program is provided without any guarantee.
#
# The license is  LGPL-v3
# For details, see GNU General Public License, version 3 or later.
#                        "https://www.gnu.org/licenses/gpl.html"
# ------------------------------------------------------------------------------
import sys
import os
import math

# ------------------------------------------------------------------------------
# import local lib
path_exec=os.path.dirname(os.path.dirname( __file__ ))
if not path_exec :
    path_exec='..'
sys.path.append(path_exec)
sys.path.append(os.path.join(path_exec,"gimp2"))

import gp2_tools as tools
import gp2_func  as gimp
from libTools    import GetPointsVector

Rt=6371   # Rayon terrestre en km
# ==============================================================================
class CDisk:
    def __init__(self,timg, tdrawable, color, width,  font, font_color, font_size ):
        self.param     = (timg, tdrawable, color, width)
        self.font_text = (font, font_color, font_size )

        self.x_pts     = [0,0,0]
        self.y_pts     = [0,0,0]
        self.xc        = 0
        self.yc        = 0
        self.radius    = 0
        self.entier     = False
        self.ratio_aire = 1
    # --------------------------------------------------------------------------
    def CreateBrush(self, width) :
        self.current_brush = gimp.context_get_brush()
        brush = gimp.brush_new("MyBrush")
        gimp.brush_set_hardness(brush, 1.0)
        gimp.brush_set_shape(brush, "CIRCLE")
        gimp.brush_set_spacing(brush, 1)
        gimp.context_set_brush(brush)
        gimp.context_set_brush_size(width)
        self.brush=brush

    # --------------------------------------------------------------------------
    def DeleteBrush(self) :
        gimp.brush_delete(self.brush)
        gimp.context_set_brush( self.current_brush )

    # --------------------------------------------------------------------------
    def MessageWarning(self) :
        gimp.message( _("Create a path with three points on the perimeter")
                  + "\n" + _("(warning! the 3 points should be non aligned)")
                  + "\n" + "\n" + _("and next, launch again the function") )
    # --------------------------------------------------------------------------
    def Get3Points(self, remove_vector=True):
        timg = self.param[0]

        xy_pts =  GetPointsVector( timg, 3, remove_vector=True )
        if xy_pts is  None :
            return True
        for ii in range(3):
            self.x_pts[ii] = xy_pts[ii][0]
            self.y_pts[ii] = xy_pts[ii][1]
            
        return False
    # --------------------------------------------------------------------------
    def CalculCercle( self ) :
        (x1,x2,x3) = self.x_pts
        (y1,y2,y3) = self.y_pts

        x1_2=x1*x1
        x2_2=x2*x2
        x3_2=x3*x3

        y1_2=y1*y1
        y2_2=y2*y2
        y3_2=y3*y3

        if (y2==y3) or (y2==y1) :
            return True

        denominateur = - ((x2-x1)/(y2-y1)) + ((x3-x2)/(y3-y2))
        if denominateur == 0 :
            return True

        xc = (x3_2 - x2_2 + y3_2 - y2_2)/(2*(y3-y2))
        xc-= (x2_2 - x1_2 + y2_2 - y1_2)/(2*(y2-y1))
        xc/= denominateur

        yc= (x2_2 - x1_2 + y2_2 - y1_2)/(2*(y2-y1)) - xc * (x2-x1)/(y2-y1)

        self.radius= math.sqrt( (xc-x1)*(xc-x1) + (yc-y1)*(yc-y1) )

        (self.xc, self.yc  ) = (xc, yc)

        self.DisqueEntier()

        return False

    def DisqueEntier(self):
        xc, yc, rr  = self.xc, self.yc, self.radius
        tdrawable = self.param[1]
        width  = gimp.drawable_width(  tdrawable)
        height = gimp.drawable_height( tdrawable)

        x_min=xc-rr
        x_max=xc+rr
        y_min=yc-rr
        y_max=yc+rr

        self.entier = ( x_min > 0 ) and ( x_min <= width )
        self.entier =  self.entier and ( x_max >= 0 ) and ( x_max <= width )
        self.entier =  self.entier and ( y_min >= 0 ) and ( y_min <= height )
        self.entier =  self.entier and ( y_max >= 0 ) and ( y_max <= height )

        aire_image = width * height
        aire_disque = math.pi*rr*rr

        self.ratio_aire = aire_disque / aire_image

        return self.entier
    # --------------------------------------------------------------------------
    def NewLayer(self, title, drw_width=0, drw_height=0 ) :
        timg,tdrawable = self.param[0:2]
        if drw_width == 0 or drw_height == 0 :
            drw_width  = gimp.drawable_width(  tdrawable)
            drw_height = gimp.drawable_height( tdrawable)

        if gimp.drawable_is_rgb(tdrawable):
            image_type= "RGBA_IMAGE"
        else:
            image_type= "GRAYA_IMAGE"

        layer_new = gimp.layer_new( timg, title, drw_width, drw_height,
                                image_type , 100.0,"NORMAL")

        gimp.image_insert_layer( timg, layer_new, None,-1)
        gimp.image_set_active_layer( timg, layer_new)
        gimp.layer_add_alpha(layer_new)
        return layer_new

    # --------------------------------------------------------------------------
    def DrawCircle(self, layer_drw,  xc, yc, radius ) :
        self.DrawEllipse(layer_drw,  xc, yc, radius, radius )

    # --------------------------------------------------------------------------
    def DrawEllipse(self, layer_drw,  xc, yc, xradius, yradius ) :
        timg  = self.param[0]
        color = self.param[2]
        vectorsId=gimp.vectors_new(timg, 'Vectors!')
        gimp.vectors_bezier_stroke_new_ellipse(vectorsId,xc, yc, xradius, yradius, 0  )
        gimp.image_add_vectors(timg, vectorsId, 0)
        gimp.context_set_foreground( color )
        gimp.edit_stroke_vectors(layer_drw, vectorsId)
        gimp.image_remove_vectors(timg,vectorsId)

    # --------------------------------------------------------------------------
    def DrawLine(self, layer_drw,  x1, y1, x2, y2, angle=0 ) :
        timg  = self.param[0]
        color = self.param[2]
        vectorsId=gimp.vectors_new(timg, 'Vectors!')
        controlpoints   = (x1,y1, x1,y1, x1,y1, x2,y2, x2,y2, x2,y2)
        strokeId=gimp.vectors_stroke_new_from_points(vectorsId, 0, controlpoints, False )
        if angle != 0 :
            angle_deg = angle * 180 / math.pi
            gimp.vectors_stroke_rotate(vectorsId,strokeId,self.xc, self.yc, angle_deg)
        gimp.image_add_vectors(timg, vectorsId, 0)
        gimp.context_set_foreground( color )
        gimp.edit_stroke_vectors(layer_drw, vectorsId)
        gimp.image_remove_vectors(timg,vectorsId)

# ==============================================================================
class CSelectDisk(CDisk):
    def __init__(self,timg, tdrawable) :
        color=(1,1,1,0)
        width=12
        font="Courier"
        font_size=12
        CDisk.__init__(self,timg, tdrawable, color, width, font, color, font_size)
        
    def Processing(self, remove_vector=True):
        timg = self.param[0]
        # important sinon traitement en bpp=8
        gimp.plugin_enable_precision()
        gimp.image_undo_group_start(timg)

        if self.Get3Points(remove_vector)  :
            self.MessageWarning()
            gimp.image_undo_group_end(timg)
            return True

        if self.CalculCercle() :
            self.MessageWarning()
            gimp.image_undo_group_end(timg)
            return True
       
        xc, yc, rr  = self.xc, self.yc, self.radius
        gimp.image_select_ellipse(timg, "REPLACE",xc-rr,yc-rr, 2*rr,2*rr )
        
        gimp.image_undo_group_end(timg)
        gimp.displays_flush()
        
        return False
        
# ==============================================================================
class CGridGraduation(CDisk):
    def __init__(self,timg, tdrawable, color, width, angle, rotate, axisNS, axisWE, font, font_size ):
        CDisk.__init__(self,timg, tdrawable, color, width,font, color, font_size)

        self.nb_graduation = int(90/angle)
        self.angle0 = math.pi*angle/180
        self.axisNS = axisNS
        self.axisWE = axisWE
        self.rotate = rotate*math.pi/180

        self.layer_drw  = 0

    def Processing(self):
        timg  = self.param[0]
        width = self.param[3]
        # important sinon traitement en bpp=8
        gimp.plugin_enable_precision()
        gimp.image_undo_group_start(timg)

        if self.Get3Points()  :
            self.MessageWarning()
            gimp.image_undo_group_end(timg)
            return

        if self.CalculCercle() :
            self.MessageWarning()
            gimp.image_undo_group_end(timg)
            return

        self.CreateBrush(width)
        self.layer_drw = self.NewLayer("Grid")
        self.DrawGrid(self.layer_drw)
        self.layer_drw = self.DrawNSaxis(self.layer_drw)
        self.layer_drw = self.DrawWEaxis(self.layer_drw)
        self.DeleteBrush()

        if (self.rotate != 0) and  (self.entier == True) :
            gimp.selection_all(timg)
            layer_sel_flottante=gimp.item_transform_rotate(self.layer_drw,
                                            self.rotate, False,self.xc,self.yc)
            gimp.floating_sel_anchor(layer_sel_flottante)

        gimp.image_undo_group_end(timg)
        gimp.displays_flush()

    # --------------------------------------------------------------------------
    def DrawGrid(self, layer_drw) :
        xc, yc, rr  = self.xc, self.yc, self.radius
        gimp.context_set_foreground( self.param[2] )
        self.DrawLine(layer_drw, xc-rr, yc, xc+rr, yc )
        self.DrawLine(layer_drw, xc, yc-rr, xc, yc+rr )
        self.DrawMeridien(layer_drw)
        self.DrawLatitude(layer_drw)
    # --------------------------------------------------------------------------
    def DrawMeridien(self, layer_drw) :
        xc, yc, rr  = self.xc, self.yc, self.radius

        for ii in range(self.nb_graduation) :
            angle_rad= self.angle0 * (ii+1)
            xradius =rr*math.sin(angle_rad)
            self.DrawEllipse( layer_drw,  xc, yc, xradius, rr )
    # --------------------------------------------------------------------------
    def DrawLatitude(self, layer_drw) :
        xc, yc, rr  = self.xc, self.yc, self.radius
        for ii in range(self.nb_graduation) :
            angle_rad= self.angle0 * (ii+1)
            hh =rr*math.sin(angle_rad)
            ww =rr*math.cos(angle_rad)
            self.DrawLine(layer_drw, xc+ww, yc-hh, xc-ww, yc-hh )
            self.DrawLine(layer_drw, xc+ww, yc+hh, xc-ww, yc+hh )
    # --------------------------------------------------------------------------
    def DrawWEaxis(self, layer_drw) :
        if self.axisWE == 0 :
            return layer_drw
        timg = self.param[0]
        xc, yc, rr  = self.xc, self.yc, self.radius
        hh = rr * 1.1
        self.DrawLine(layer_drw, xc-hh, yc, xc+hh, yc )

        text=( " " +  _('West'), " " + _('East') )
        if self.axisWE == 2 :
            text=( text[1], text[0])

        (font, font_color, szfont )=self.font_text
        tlayer = tools.SetText(timg, xc-rr-szfont,yc-2*szfont, text[0],font, szfont, font_color, 1 )[0]
        gimp.image_merge_down( timg,tlayer, "EXPAND_AS_NECESSARY")

        tlayer = tools.SetText(timg, xc+rr+szfont,yc+szfont, text[1],font, szfont, font_color, 0 )[0]
        layer_drw = gimp.image_merge_down( timg,tlayer, "EXPAND_AS_NECESSARY")
        return layer_drw
    # --------------------------------------------------------------------------
    def DrawNSaxis(self, layer_drw) :
        if self.axisNS == 0 :
            return layer_drw
        timg = self.param[0]
        xc, yc, rr  = self.xc, self.yc, self.radius
        hh = rr * 1.1
        self.DrawLine(layer_drw, xc, yc-hh, xc, yc+hh )
        text=( " " +  _('North'), " " + _('South') )
        if self.axisNS == 2 :
            text=( text[1], text[0])

        (font, font_color, szfont )=self.font_text
        tlayer = tools.SetText(timg, xc-szfont,yc-rr-2*szfont, text[0],font, szfont, font_color, 1 )[0]
        gimp.image_merge_down( timg,tlayer, "EXPAND_AS_NECESSARY")

        tlayer = tools.SetText(timg, xc+szfont,yc+rr+szfont, text[1],font, szfont, font_color, 0 )[0]
        layer_drw = gimp.image_merge_down( timg,tlayer, "EXPAND_AS_NECESSARY")
        return layer_drw

# ==============================================================================
class CDiskGraduation(CDisk):

    def __init__(self,timg, tdrawable, color, width, Rd,
                 nb_graduation_perimetre, ext_graduation, QuarterGrad, diskname,
                 font, font_color, font_size ):
        CDisk.__init__(self,timg, tdrawable, color, width,font, font_color, font_size)
        self.Rd = Rd
        self.nb_graduation = nb_graduation_perimetre
        self.diskname = diskname
        self.ext_graduation = ext_graduation
        self.quarter_graduation = QuarterGrad
        self.layer_drw =  0

    # --------------------------------------------------------------------------
    def Processing(self):
        timg = self.param[0]
        width =self.param[3]

        # important sinon traitement en bpp=8
        gimp.plugin_enable_precision()
        gimp.image_undo_group_start(timg)

        if self.Get3SPoints()  :
            self.MessageWarning()
            gimp.image_undo_group_end(timg)
            return

        if self.CalculCercle() :
            self.MessageWarning()
            gimp.image_undo_group_end(timg)
            return

        self.CreateBrush(width)
        self.layer_drw = self.NewLayer(b"graduation")
        self.DrawGraduationDisque(self.layer_drw)
        self.AddQuarterGraduation( self.quarter_graduation)
        self.DeleteBrush()

        (font, font_color, szfont ) = self.font_text
        tab = "    "
        txt_fmt = _("Calculated information from the 3 points") + ": \n\n"
        txt_fmt+= _("Scale") + ": 1 pixels / %.2f km\n" % ( self.Rd /self.radius  )
        txt_fmt+= _("1 graduation ") +": %d km\n\n"  % (2*math.pi*self.Rd/self.nb_graduation )
        txt_fmt+= _("Diameter")     +":\n"
        txt_fmt+= tab + self.diskname    + "=%.1f pixels\n" % ( 2*self.radius )
        if self.Rd > Rt :
            txt_fmt+= tab + _("Earth") + "=%.1f pixels\n\n" % (2*self.radius*Rt/self.Rd)
        txt_fmt+= _("Center")       + "=(%.1f , %.1f )" % (self.xc, self.yc)
        tools.SetText(timg, szfont, szfont, txt_fmt,font, szfont, font_color, 0 )

        gimp.image_undo_group_end(timg)
        gimp.displays_flush()

    # --------------------------------------------------------------------------
    def DrawGraduationDisque(self, layer_drw ) :
        # raccourci
        timg  = self.param[0]
        color = self.param[2]
        width = self.param[3]
        xc, yc, rr  = self.xc, self.yc, self.radius

        gimp.context_set_foreground( color )

        trait = 4.0 * width

        # 1) Trace  les rayons pour graduer
        szline = rr + trait
        angle0 = 2.0 *math.pi / self.nb_graduation
        for ii in range(self.nb_graduation) :
            angle_rad= angle0 * ii
            xx  = xc + szline*math.sin(angle_rad)
            yy  = yc - szline*math.cos(angle_rad)
            self.DrawLine(layer_drw, xx, yy, xc, yc )

        # 2) supprime le contenu du disque
        Rmin=rr-width/2.0
        gimp.image_select_ellipse(timg, "REPLACE",xc-Rmin,yc-Rmin, 2*Rmin,2*Rmin )

        gimp.edit_cut(layer_drw)
        gimp.selection_none(timg)

        flag = self.entier
        if flag == False :
            if self.ratio_aire <= 4 :
                flag = True

        if flag :
            # 3) Trace axes O-E et N-S
            self.DrawLine(layer_drw, xc-rr, yc, xc+rr, yc )
            self.DrawLine(layer_drw, xc, yc-rr, xc, yc+rr )

            for ii in range(int(self.nb_graduation/4)-1) :
                delta=rr*math.sin(angle0 * (ii+1) )
                self.DrawLine(layer_drw, xc+delta, yc-trait, xc+delta, yc+trait )
                self.DrawLine(layer_drw, xc-delta, yc-trait, xc-delta, yc+trait )
                self.DrawLine(layer_drw, xc-trait, yc+delta, xc+trait, yc+delta )
                self.DrawLine(layer_drw, xc-trait, yc-delta, xc+trait, yc-delta )
        else:
            angle = math.atan2( (self.y_pts[1]-yc), (self.x_pts[1]-xc) )
            self.DrawLine(layer_drw, xc, yc, self.x_pts[1], self.y_pts[1] )
            for ii in range(int(self.nb_graduation/4)-1) :
                delta=rr*math.sin(angle0 * ii)
                self.DrawLine(layer_drw, xc+delta, yc-trait, xc+delta, yc+trait, angle )

        nb_cercle=1
        if  self.ext_graduation :
            nb_cercle=5

        step_km = (2.0*math.pi*self.Rd/self.nb_graduation )
        step_pixel = step_km*self.radius/self.Rd
        for ii in range(nb_cercle) :
            self.DrawCircle(layer_drw,  xc, yc, rr + ii*step_pixel )

    def AddQuarterGraduation(self, quarter_graduation):
        if quarter_graduation < 1 or quarter_graduation > 15:
            return

        timg = self.param[0]
        xc, yc, rr  = self.xc, self.yc, self.radius

        angle0 = 2.0 *math.pi / self.nb_graduation
        layer_drw2 = self.NewLayer(b"graduation")
        for ii in range(int(self.nb_graduation/4)-1) :
            delta=rr*math.sin(angle0 * (ii+1) )
            self.DrawCircle(layer_drw2,  xc, yc, delta )

        gimp.selection_none(timg)

        if quarter_graduation == 15 : return # cercle complet

        quarters={
                "1" : [ 1 ]      , "2": [ 2 ],
                "3" : [ 3 ]      , "4": [ 4 ],
                "5" : [ 1, 3 ]   , "6": [ 2, 4 ],
                "7" : [ 1, 2 ]   , "8": [ 3, 4 ],
                "9" : [ 1, 4 ]   ,"10": [ 2, 3 ],
                "11": [ 1, 2, 3 ],"12": [ 1, 2, 4 ],
                "13": [ 1, 3, 4 ],"14": [ 2, 3, 4 ]
                }
        for quarter in quarters[str(quarter_graduation)] :
            # quarter =1
            x1=xc-rr
            y1=yc-rr
            if quarter == 2 :
                x1+=rr
            if quarter == 3 :
                x1+=rr
                y1+=rr
            if quarter == 4 :
                y1+=rr
            gimp.image_select_rectangle(timg, "ADD", x1, y1, rr, rr )

        gimp.selection_invert(timg)
        gimp.edit_cut(layer_drw2)
        gimp.selection_none(timg)
        gimp.image_merge_down( timg, layer_drw2, "EXPAND_AS_NECESSARY")

