#! /usr/bin/env python
# -*- coding: UTF-8 -*-

# ------------------------------------------------------------------------------
# This program is provided without any guarantee.
#
# The license is  LGPL-v3
# For details, see GNU General Public License, version 3 or later.
#                        "https://www.gnu.org/licenses/gpl.html"
# ------------------------------------------------------------------------------
import sys
import os
import math

# ------------------------------------------------------------------------------
# import local lib
path_exec=os.path.dirname(os.path.dirname( __file__ ))
if not path_exec :
    path_exec='..'
sys.path.append(path_exec)
sys.path.append(os.path.join(path_exec,"gimp2"))

import gp2_tools as tools
import gp2_func  as gimp

# ------------------------------------------------------------------------------
class CHisto:
    def __init__(self):
        self.nb_bins    =  0
        self.channels   = {}
        self.hdata      = [0]*2 # donne de l'histogramme calcule
        self.hdata_lisse= [0]*2 # donne de l'histogramme calcule et lisse

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # exportation de l'histogramme au format CSV  (excel)
    def export_csv(self, filename, sep=";" , point="." ):
        nb_bins  = self.nb_bins
        channels = self.channels
        histo    = self.hdata[0]

        numstr= str(1.1)
        pt=numstr[1]
        step=1.0/nb_bins

        with open(filename, "wt") as fd:
            chaine   =                "bin"       + sep + str(nb_bins)
            chaine   = chaine + sep + "step"      + sep + str(step)
            fd.write(chaine.replace(pt,point) +"\n")

            chaine= "range"
            for channel in channels.keys():
                chaine = chaine + sep + channel
            fd.write(chaine.replace(pt,point) +"\n")

            pix_range=0.0
            for ii in range(nb_bins):
                chaine=str(pix_range)
                for channel in channels.keys():
                    chaine= chaine + sep + str(histo[channel][ii])
                fd.write(chaine.replace(pt,point) +"\n")
                pix_range=pix_range+step

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # export de l'histogramme au format gnuplot
    def export_gplot(self, filename, sep=" " , point="." ):
        nb_bins  = self.nb_bins
        channels = self.channels
        histo    = self.hdata[0]

        numstr= str(1.1)
        pt=numstr[1]
        step=1.0/nb_bins

        with open(filename, "wt") as fd:
            chaine   =               "#bin"         + sep + str(nb_bins)
            chaine   = chaine + sep + "step"        + sep + str(step)
            fd.write(chaine.replace(pt,point) +"\n")

            chaine= "# range"
            for channel in channels.keys():
                chaine = chaine + sep + channel
            fd.write(chaine.replace(pt,point) +"\n")

            pix_range=0.0
            for ii in range(nb_bins):
                chaine=str(pix_range)
                for channel in channels.keys():
                    chaine= chaine + sep + str(histo[channel][ii])
                fd.write(chaine.replace(pt,point) +"\n")
                pix_range=pix_range+step

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # Calcul l'histogramme
    def Algo(self, timg, tdrawable, normalisation=True, MsgProgress="" ):
        gimp.plugin_enable_precision()

        precision= gimp.image_get_precision(timg)
        if tools.GetInfoImg(precision)[1] == 0 :
            gimp.message("image : precision non traitee" + tools.GetInfoImg(precision)[0] + "!!!" )
            return

        if gimp.drawable_is_rgb(tdrawable):
            channels = {"RED"  :[ (1.0,0.0,0.0,0.0),0],
                        "GREEN":[ (0.0,1.0,0.0,0.0),1],
                        "BLUE" :[ (0.0,0.0,1.0,0.0),2]}
        else:
            channels = {"VALUE":[ (1.0,1.0,1.0,0.0),0]}
        nb_channel = len(channels)

        nb_bins=512
        if tools.GetInfoImg(precision)[1] > 0 :
            nb_bins=min(nb_bins, tools.GetInfoImg(precision)[4])

        x0=0
        y0=0
        width =gimp.drawable_width(tdrawable)
        height=gimp.drawable_height(tdrawable)
        bb =  gimp.selection_bounds(timg)[1]
        if bb[0] != 0 :
            width  = bb[3]-bb[1]
            height = bb[4]-bb[2]
            x0=bb[1]
            y0=bb[2]

        gimp.selection_none(timg)

        gimp.progress_set_text( MsgProgress + " ... Region loading ... "
             + str(x0) + "," + str(y0) + " " + str(width) + "x" + str(height)  )

        srcImgPix = tools.GetRegionRect(tdrawable, tools.GetInfoImg(precision)[2], x0, y0,width, height)[2]

        # histogramme par couche
        alpha=0
        if gimp.drawable_has_alpha(tdrawable): alpha=1
        histo={}
        maxhisto={}
        if tools.GetInfoImg(precision)[1] > 0 :
            pw2_bins=int(math.log(nb_bins,2))
            coef =1.0/float(pow((tools.GetInfoImg(precision)[1]-pw2_bins),2))
        else:
            coef = nb_bins-1

        for keystr,value in channels.items():
            histo[keystr]= [0] * nb_bins
            maxhisto[keystr]=[-1,0]
            ii=value[1]
            gimp.progress_set_text( MsgProgress+" ... Histogram processing " + keystr )
            for ll in range(height):
                for cc in range(width):
                    index_histo = int( srcImgPix[ii]* coef )
                    index_histo = min( nb_bins-1 , index_histo )
                    index_histo = max(         0 , index_histo )
                    ii = ii + nb_channel + alpha
                    histo[keystr][index_histo] += 1

        # normalise par rapport au pic
        for keystr,value in channels.items():
            gimp.progress_set_text( MsgProgress+" ... Histogram normalizing  " + keystr )
            maxhisto[keystr] = self.GetMaxData(histo[keystr])
            if normalisation :
                coef=float(maxhisto[keystr][1])
                for ii in range(nb_bins):
                    gimp.progress_pulse()
                    histo[keystr][ii]= float(histo[keystr][ii])/coef

        self.nb_bins  = nb_bins
        self.channels = channels
        self.hdata    = [ histo, maxhisto , width*height]

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # recherche la position du max et max d'une courbe
    def GetMaxData(self,data_in):
        max_data=[0,data_in[0]]
        for ii in range(1,len(data_in)):
            if data_in[ii]>max_data[1]:
                max_data[1] = data_in[ii]
                max_data[0] = ii
        return max_data
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # Courbe lisse par une moyenne glissante
    def MoyenneGlissante(self, data_in, demi_prof=1 ):
        nb_pts   = len(data_in)
        prof     = 2*demi_prof+1
        data_out = [0]*nb_pts
        regist=[0]*prof
        #preambule
        cumul             = data_in[0]
        regist[demi_prof] = data_in[0]
        for ii in range(1,demi_prof+1):
            regist[demi_prof-ii] = data_in[ii]
            regist[demi_prof+ii] = data_in[ii]
            cumul               += 2*data_in[ii]
        #lissage
        kk=demi_prof+1
        ii=0
        for jj in range(0,nb_pts-demi_prof-1):
            data_out[jj]  = cumul/prof
            cumul        += data_in[kk]-regist[ii]
            regist[ii]    = data_in[kk]
            kk+=1
            ii+=1
            ii%=prof
        #postambule
        for jj in range(nb_pts-demi_prof-1,nb_pts):
            data_out[jj]  = cumul/prof
            cumul        += data_in[kk-1]-regist[ii]
            regist[ii]    = data_in[kk-1]
            kk-=1
            ii+=1
            ii%=prof
        return data_out

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # lissage de l'histogramme
    def Lissage(self, demi_prof=1 ):
        channels = self.channels
        histo    = self.hdata[0]

        histo_m = {}
        maxhisto= {}
        for keystr in channels.keys():
            histo_m[keystr]  = self.MoyenneGlissante(histo[keystr],demi_prof)
            maxhisto[keystr] = self.GetMaxData(histo_m[keystr])

        self.hdata_lisse = [ histo_m , maxhisto, self.hdata[2] ]

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # Affichage de l'histogramme
    def Display(self, timg, tdrawable, bLisse=False,  bMaxLisse=False,  bGrid=True, bLinePic=False, bTextPic=True ):
        nb_bins  = self.nb_bins
        channels = self.channels
        if bLisse :
            histo    = self.hdata_lisse[0]
            maxhisto = self.hdata_lisse[1]
        else:
            histo    = self.hdata[0]
            maxhisto = self.hdata[1]
            if bMaxLisse :
                maxhisto = self.hdata_lisse[1]
            else:
                maxhisto = self.hdata[1]

        Noir=(0,0,0,0)
        Gris=(0.25,0.25,0.25,0)
        nb_channel = len(channels)

        gimp.progress_set_text( "... Histogram creating" )
        # affichage dans une autre image
        nb_xpoint=1000
        nb_ypoint=500
        nb_ypoint_all=2*nb_ypoint
        img_hist   = gimp.image_new_with_precision( nb_xpoint, nb_ypoint_all, "RGB", "U8_LINEAR" )

        gimp.image_undo_group_start(img_hist)
        layer_hist = gimp.layer_new(img_hist, "histo", nb_xpoint, nb_ypoint_all, "RGB_IMAGE", 100, "NORMAL")
        gimp.image_insert_layer(img_hist,layer_hist, None,-1)
        gimp.context_set_background( Noir )
        gimp.drawable_fill( layer_hist, "BACKGROUND_FILL")

        current_brush = gimp.context_get_brush()
        brush = gimp.brush_new("MyBrush")
        gimp.brush_set_hardness(brush, 1.0)
        gimp.brush_set_shape(brush, "CIRCLE")
        gimp.brush_set_spacing(brush, 1)
        gimp.context_set_brush(brush)
        gimp.context_set_brush_size(1.0)

        # dessine une grille
        if bGrid :
            gimp.progress_set_text( "... Grid creating "  )
            gimp.context_set_foreground( Gris )
            hh  = float(nb_ypoint)/float(nb_channel)
            # 1) horizontal
            for keystr,value in channels.items():
                for y0 in [ value[1]*hh, (value[1]+nb_channel)*hh ]:
                    for ii in [ 1,2,3]:
                        ligne = ( nb_xpoint, y0+hh*ii/4.0, 0, y0+hh*ii/4.0 )
                        gimp.pencil( layer_hist, ligne )
            # 2) vertical
            for ii in range(1,8):
                ligne= ( nb_xpoint*ii/8.0, 0, nb_xpoint*ii/8.0 , nb_ypoint*2.0 )
                gimp.pencil( layer_hist, ligne )

        # histo lineaire
        text     =_("Linear Histogram")
        fontname = "Courier"
        szfont   = 20.0
        tw = gimp.text_get_extents_fontname( text, szfont,"PIXELS", fontname )[1][0]
        gimp.context_set_foreground( Gris )
        tlayer=gimp.text_fontname( img_hist, -1 , nb_xpoint-tw-10, hh/4,
                                     text , -1, True, szfont,"PIXELS", fontname)

        layer_hist=gimp.image_merge_down( img_hist,tlayer, "EXPAND_AS_NECESSARY")
        xscale = float(nb_xpoint)/float(nb_bins)
        for keystr,value in channels.items():
            gimp.context_set_foreground( value[0] )
            gimp.progress_set_text( "... Linear histogram tracing  " +  keystr )
            y0 = float(value[1])*hh
            courbe=[]
            for ii in range(nb_bins):
                courbe.append( float(ii)*xscale)
                courbe.append( y0+((1.0-histo[keystr][ii])*hh))
            gimp.pencil( layer_hist, courbe )

        # histo logarithme
        gimp.context_set_foreground( Gris )
        text=_("Log Histogram")
        tw = gimp.text_get_extents_fontname( text, szfont,"PIXELS", fontname )[1][0]
        tlayer=gimp.text_fontname( img_hist, -1 , nb_xpoint-tw-10, nb_ypoint+hh/4,
                                   text , -1, True, szfont, "PIXELS", fontname)

        layer_hist=gimp.image_merge_down( img_hist,tlayer, "EXPAND_AS_NECESSARY")
        yscale=hh/(math.log(float(nb_ypoint-1)/0.5,10))
        for keystr,value in channels.items():
            gimp.context_set_foreground( value[0] )
            gimp.progress_set_text( "... Log histogram tracing" +  keystr )
            y0 = (value[1]+nb_channel)*hh
            courbe=[]
            for ii in range(nb_bins):
                courbe.append( ii*xscale )
                y=max(0.5,float(nb_ypoint-1)*histo[keystr][ii])
                y=math.log((y+0.0)/(nb_ypoint-1),10)*yscale
                y = max( nb_ypoint-1, y0 - y)
                courbe.append( y )
            gimp.pencil( layer_hist, courbe )

        if bLinePic :
            for keystr,value in channels.items():
                gimp.context_set_foreground( value[0] )
                xx=maxhisto[keystr][0]*1000.0/nb_bins
                ligne=( xx, 0, xx , nb_ypoint*2 )
                gimp.pencil( layer_hist, ligne )

        if bTextPic :
            for keystr,value in channels.items():
                gimp.context_set_foreground( value[0] )
                yy = float(value[1])*hh + hh/4

                text=_("peak") + " => %0.1f" %(float(maxhisto[keystr][0])/nb_bins*100.0)
                tlayer=gimp.text_fontname( img_hist, -1 , nb_xpoint/2, yy,
                                           text, -1, True, szfont, "PIXELS", fontname)
                gimp.image_merge_down( img_hist,tlayer, "EXPAND_AS_NECESSARY")
                for keystr2 in channels.keys():
                    if keystr == keystr2 :
                        continue
                    yy+=hh/4
                    text="%s-%s: %0.1f" %(keystr,keystr2,float(maxhisto[keystr][0]-maxhisto[keystr2][0])/nb_bins*100.0)
                    tlayer=gimp.text_fontname( img_hist, -1 , nb_xpoint/2, yy,
                                                text, -1, True, szfont, "PIXELS", fontname)
                    gimp.image_merge_down( img_hist,tlayer, "EXPAND_AS_NECESSARY")
        gimp.brush_delete(brush)
        gimp.context_set_brush( current_brush )

        gimp.display_new(img_hist)
        gimp.image_undo_group_end(img_hist)


    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # Recherche le delta entre les pics de l'histogramme d'une image RVB
    def GetPicDelta(self, bLisse=False ):
        nb_bins  = self.nb_bins
        channels = self.channels
        if bLisse :
            maxhisto = self.hdata_lisse[1]
        else:
            maxhisto = self.hdata[1]

        nb_channel = len(channels)
        if nb_channel  != 3 :
            return ( None, None, None,  None, None )
        for keystr in channels.keys():
            delta=[]
            for keystr2 in channels.keys():
                if keystr == keystr2 :
                    continue
                delta.append( [ float(maxhisto[keystr][0]-maxhisto[keystr2][0])/nb_bins*100.0,keystr2] )
            print( keystr,":",delta )
            if (delta[0][0] < 0) and (delta[1][0] < 0) :
                return ( keystr, delta[0][1], abs(delta[0][0]), delta[1][1], abs(delta[1][0]) )
        return ( None, None, None,  None, None )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # Recherche le delta entre la position gauche des pics/2 de l'histogramme
    # d'une image RVB
    def GetDemiPicGaucheDelta(self, bLisse=False ):
        nb_bins  = self.nb_bins
        channels = self.channels

        nb_channel = len(channels)
        if nb_channel  != 3 :
            return ( None, None, None,  None, None )

        demipic = self.find_DemiPicGauche( bLisse )
        print( demipic)
        for keystr in channels.keys():
            delta=[]
            for keystr2 in channels.keys():
                if keystr == keystr2 :
                    continue
                delta.append( [ float(demipic[keystr][0]-demipic[keystr2][0])/nb_bins*100.0,keystr2] )
            print( keystr,":",delta )
            if (delta[0][0] < 0) and (delta[1][0] < 0) :
                return ( keystr, delta[0][1], abs(delta[0][0]), delta[1][1], abs(delta[1][0]) )
        return ( None, None, None,  None, None )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # Recherche la position gauche des pics/2  sur l'histogrammes d'une image RVB
    def find_DemiPicGauche(self, bLisse=False ):
        channels = self.channels
        if bLisse :
            histo    = self.hdata_lisse[0]
            maxhisto = self.hdata_lisse[1]
        else:
            histo    = self.hdata[0]
            maxhisto = self.hdata[1]
        DemiPicGauche = {}
        for channel in channels.keys():
            ii           = maxhisto[channel][0]
            demi_hauteur = histo[channel][ii]/2.0
            while ii>0 and histo[channel][ii]>=demi_hauteur:
                ii = ii - 1
            if ii > 0 :
                DemiPicGauche[channel]  = [ ii , histo[channel][ii] ]
            else:
                DemiPicGauche[channel]  = [ 0 , histo[channel][0] ]
        return DemiPicGauche

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # Recherche la position droite des pics/2  sur l'histogrammes d'une image RVB
    def find_DemiPicDroit(self, bLisse=False ):
        channels = self.channels
        if bLisse :
            histo    = self.hdata_lisse[0]
            maxhisto = self.hdata_lisse[1]
        else:
            histo    = self.hdata[0]
            maxhisto = self.hdata[1]
        DemiPicDroit = {}
        for channel in channels.keys():
            ii           = maxhisto[channel][0]
            demi_hauteur = histo[channel][ii]/2.0
            borne = len(histo[channel])
            while ii< borne and histo[channel][ii]>=demi_hauteur:
                ii = ii + 1
            if ii < borne :
                DemiPicDroit[channel]  = [ ii , histo[channel][ii] ]
            else:
                DemiPicDroit[channel]  = [ borne-1 , histo[channel][borne-1] ]
        return DemiPicDroit

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # Retourne la premiere position supérieur au seuil entre tous les canaux
    # en partant du debut de l'histo
    def GetMinValue(self, bLisse=False, seuil=0.001 ):
        nb_bins  = self.nb_bins
        channels = self.channels
        if bLisse :
            histo    = self.hdata_lisse[0]
        else:
            histo    = self.hdata[0]

        # cherche la premiere valeur non-nul
        ii_minhisto=nb_bins
        for keystr in channels.keys():
            for ii in range(nb_bins):
                if histo[keystr][ii]>seuil:
                    ii_minhisto= min( ii, ii_minhisto )
                    break
        return ii_minhisto

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # Retourne les positions et les max des canaux
    def GetMaxhisto(self, bLisse=False ):
        if bLisse :
            maxhisto = self.hdata_lisse[1]
        else:
            maxhisto = self.hdata[1]
        return maxhisto

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # Retourne le nombre de bin de l'histogramme
    def GetNbBins(self):
        return self.nb_bins
