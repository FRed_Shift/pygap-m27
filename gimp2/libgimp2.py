#! /usr/bin/env python
# -*- coding: UTF-8 -*-

# ------------------------------------------------------------------------------
# This program is provided without any guarantee.
#
# The license is  LGPL-v3
# For details, see GNU General Public License, version 3 or later.
#                        "https://www.gnu.org/licenses/gpl.html"
# ------------------------------------------------------------------------------
from ctypes import CDLL
from sys    import platform

__ALL__ = [ 'gimp2', 'gegl'  ]

def load_library (library_name):
    if platform == "linux" or platform == "linux2":
        library_name = library_name + '.so.0'
    elif platform == "win32":
        from ctypes.util import find_library
        library_name = find_library (library_name + "-0")
    else:
        raise BaseException ("TODO")

    return CDLL(library_name)

gimp2 = load_library ('libgimp-2.0')
gegl  = load_library ('libgegl-0.4')
gegl.gegl_init (None, None)
