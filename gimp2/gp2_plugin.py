#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# ------------------------------------------------------------------------------
# This program is provided without any guarantee.
#
# The license is  LGPL-v3
# For details, see GNU General Public License, version 3 or later.
#                        "https://www.gnu.org/licenses/gpl.html"
# ------------------------------------------------------------------------------
import os

from   ctypes import Structure,POINTER,CFUNCTYPE,byref,c_char_p,c_int
from   sys    import argv

from libgimp2  import gimp2

from gp2_type  import GimpParam
from gp2_type  import GimpReturnValues

from gp2_enums import GimpPDBProcType    as ProcType
from gp2_enums import GimpPDBArgType     as ArgType
from gp2_enums import GimpPDBStatusType  as StatusType

from gp2_func  import tobytes

# ------------------------------------------------------------------------------
class GimpParamDef (Structure):
    _fields_ = [ ('type', c_int),
                 ('name', c_char_p),
                 ('description', c_char_p) ]

# ------------------------------------------------------------------------------
GimpRunProc = CFUNCTYPE (None,
                     c_char_p,
                     c_int,
                     POINTER(GimpParam),
                     POINTER(c_int),
                     POINTER(POINTER (GimpParam))
                     )
# ------------------------------------------------------------------------------
void_FUNC_void = CFUNCTYPE (None)

# ------------------------------------------------------------------------------
class GimpPlugInInfo (Structure):
    _fields_ = [ ('init_proc', void_FUNC_void),
                 ('quit_proc', void_FUNC_void),
                 ('query_proc', void_FUNC_void),
                 ('run_proc', GimpRunProc) ]
    def __init__ (self,
                  query_proc,
                  run_proc,
                  init_proc = void_FUNC_void (),
                  quit_proc = void_FUNC_void ()):
        Structure.__init__ (self)
        self.init_proc  = init_proc
        self.quit_proc  = quit_proc
        self.query_proc = query_proc
        self.run_proc   = run_proc

# ==============================================================================
class CPlugin:
    """ class mere pour creer le  plugin """
    def __init__(self, sname, sblurb, shelp, sinfo_right, slabel, smenu, simage_types, parameters, gui, dir_scriptfu="" ):
        """ Information pour créer le plugin:
            name
            blurb
            help
            info:author,copyright,date
            label
            image_types
            menu path
            parameters_list
            gui
        """
        self.plugin_info = [ sname, sblurb, shelp, slabel, smenu, simage_types ]
        self.params      = parameters
        self.gui         = gui
        self.right       = sinfo_right
        self.dir_scriptfu = dir_scriptfu

        self.ret_values = GimpReturnValues()

    def register(self):
        print("loading " + self.plugin_info[0] )
        PLUG_IN_INFO = GimpPlugInInfo (void_FUNC_void (self.query), GimpRunProc (self.run))
        Argc = len (argv)
        charArrayN = c_char_p * Argc
        Argv = charArrayN (*[ai.encode ('utf8') for ai in argv])
        gimp2.gimp_main( byref(PLUG_IN_INFO), Argc, Argv)

    def SetDefaultValue( self, param, gui ):
        if not  "%" in gui["VALBOX"] :
            return gui["VALBOX"]
        if  gui["BOX"] == "SF-COLOR" :
            return gui["VALBOX"] % ( int(param["value"][0]*255),
                                     int(param["value"][1]*255),
                                     int(param["value"][2]*255) )
        return  gui["VALBOX"] % ( param["value"] )

    def pluginScm(self,guiname=""):
        (sname, sblurb, shelp, slabel, smenu, simage_types) = self.plugin_info
        _unused_ = ( sblurb, shelp ) # unused variable , suppress pylint warning
        sauthor    = self.right["author"]
        scopyright = self.right["copyright"]
        sdate      = self.right["date"]

        if guiname=="":
            guiname=sname

        shelp_fmt = self.help_str()
        txt =";; :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::\n"
        txt+= "(define (scm-" + guiname + " "
        if simage_types != "" :
            txt+= "image drawable "
        for pp in self.params :
            txt+= pp["name"] + " "
        txt+= ")\n\t(%s " % ("py" + sname )
        if simage_types != "" :
            txt+= "image drawable "
        else:
            txt+= "0 0 "
            
        for pp in self.params :
            txt+= pp["name"] + " "
        txt+= ")\n\t(gimp-displays-flush)\n)\n\n"

        txt+= '(script-fu-register "scm-%s"\n\t"%s"\n\t"%s"\n\t"%s"\n\t"%s"\n\t"%s"\n\t"%s"' % (guiname, slabel,shelp_fmt, sauthor,scopyright,sdate,simage_types)
        if simage_types != "" :
            txt+= '\n\tSF-IMAGE        "Input image"        0' 
            txt+= '\n\tSF-DRAWABLE     "Input drawable"     0'
        for pp in self.params :
            gui=self.gui[pp['name']]
            valbox = self.SetDefaultValue(pp,gui)
            txt+= '\n\t%-15s %-20s %s' % ( gui["BOX"], '"'+pp["info"]+'"', valbox )

        txt+= '\n)\n(script-fu-menu-register "scm-%s" "%s" )\n\n' % (guiname, smenu )
        return txt

    def CreerPluginScm(self,guiname="",force=False):
        if guiname=="":
            guiname=self.plugin_info[0]

        filename = guiname + ".scm"
        if self.dir_scriptfu != "" :
            filename = os.path.join( self.dir_scriptfu, filename )

        if os.path.isfile(filename) and force == False :
            return

        scm_txt = self.pluginScm(guiname)
        try:
            with open(filename ,"w") as fd :
                fd.write( scm_txt )
        except Exception as e :
            print(e)
            gimp2.message(e)

    # ------------------------------------------------------------------------------
    def help_str( self ):
        shelp = self.plugin_info[2]
        cr     = "\n"
        bullet = "  o "
        
        end_msg  = cr + _("Author")  + ": "+ self.right["author"]  + cr
        end_msg += cr + _("Version") + ": "+ self.right["version"] + " - " + self.right["date"]  +cr
        end_msg += cr + _("License") + ": "+ self.right["license"] + cr

        msg  = _("Description") + ":" + cr
        if ( type(shelp) is list) or (type(shelp) is tuple) :
            shelp = cr.join(shelp)
        msg += shelp + cr

        # limitation de la taille de help_str dans le scriptfu
        max_str=990;        
        param_msg = cr + _("Parameters") + ":" + cr
        for pp in self.params :
            newparam_msg = bullet + pp["name"] +  ": " +  pp["info"] + cr
            len_msg = len(msg)+len(param_msg)+ len(newparam_msg)+len(end_msg)
            if len_msg > max_str :
                param_msg += bullet + "..."  + cr
                len_msg_new = len(msg)+len(param_msg)+ len(end_msg)
                print(self.plugin_info[0] + " *** truncated help *** %d > %d (=>%d)" % (  len_msg , max_str,len_msg_new)  )
                break
            param_msg += newparam_msg
        return msg+param_msg+end_msg

    # --------------------------------------------------------------------------
    def query(self):
        (sname, sblurb, shelp, slabel, smenu, simage_types) = self.plugin_info
        _unused_ = (shelp,smenu) # unused variable , suppress pylint warning
        params = []
        params.append( GimpParamDef( ArgType["IMAGE"] , tobytes("image") , tobytes("Input image")) )
        params.append( GimpParamDef( ArgType["DRAWABLE"] , tobytes("drawable") , tobytes("Input drawable")) )
        for pp in self.params :
            params.append( GimpParamDef( ArgType[pp["type"]] , tobytes(pp["name"]) , tobytes(pp["info"])) )
        nb_params = len (params) 
        GimpParamDefN = GimpParamDef * nb_params

        sauthor    = self.right["author"]
        scopyright = self.right["copyright"]
        sdate      = self.right["date"]
        shelp_fmt = self.help_str()

        gimp2.gimp_install_procedure(
                tobytes("py" + sname), tobytes(sblurb),tobytes(shelp_fmt),
                tobytes(sauthor), tobytes(scopyright),tobytes(sdate),
                tobytes(slabel),tobytes(simage_types),
                ProcType["GIMP_PLUGIN"],
                nb_params, 0 ,
                GimpParamDefN(*params), None )

    # --------------------------------------------------------------------------
    def run(self, proc_name, n_params, params, n_return_vals, return_vals):
        for nn in range(n_params-2):
            self.params[nn]['value'] = self.GetParamData( self.params[nn]['type'],params[nn+2].data)

        self.proc_name = proc_name
        status = self.Processing(params[0].data.d_image, params[1].data.d_drawable)
        return_vals[0]          = self.ret_values
        self.ret_values[0].type = ArgType['STATUS']

        if not status[0] in  StatusType :
            status[0] = "EXECUTION_ERROR"
            status[1] = "UNKNOWN STATUS"

        self.ret_values[0].data.d_status = StatusType[status[0]]
        n_return_vals[0] = 1

        if status[0] != "SUCCESS" :
            self.ret_values[1].type          = ArgType[ "STRING" ]
            self.ret_values[1].data.d_string = tobytes(status[1])
            n_return_vals[0] = 2
            
    def get_parameters_value(self):
        return ( pp["value"] for pp in self.params)

    def GetParamData( self,dtype, data ):
        if dtype == "FLOAT"    :  return data.d_float
        if dtype == "STRING"   :  return data.d_string
        if dtype == "COLOR"    :  return data.d_color
        if dtype == "IMAGE"    :  return data.d_image
        if dtype == "DRAWABLE" :  return data.d_drawable
        if dtype == "STATUS"   :  return data.d_status
        if dtype == "INT32"    :  return data.d_int
        #if dtype == ProcType["BOOL"] :  return data.d_bool
        return None
    
    def Param(self,p_type,p_name,p_info,p_value ):
        return { "type":p_type, "name":p_name , "info":p_info, "value": p_value}
    def Box(self,p_box,p_valbox ):
        return { "BOX":p_box, "VALBOX":p_valbox }
