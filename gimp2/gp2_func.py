#! /usr/bin/env python
# -*- coding: UTF-8 -*-

# ------------------------------------------------------------------------------
# This program is provided without any guarantee.
#
# The license is  LGPL-v3
# For details, see GNU General Public License, version 3 or later.
#                        "https://www.gnu.org/licenses/gpl.html"
# ------------------------------------------------------------------------------
import traceback

from ctypes    import c_double,c_char_p,c_int,c_bool,c_byte,POINTER,byref
from libgimp2  import gimp2

from gp2_type  import GeglBuffer
from gp2_type  import GimpParam
from gp2_type  import GimpRGB                 as ColorRGB
from gp2_type  import GimpPixelRgn            as PixelRgn
from gp2_type  import GimpDrawable            as SDrawable

from gp2_enums import GimpPDBStatusType       as StatusType
from gp2_enums import GimpPDBArgType          as ArgType
from gp2_enums import GimpLayerMode           as LayerMode
from gp2_enums import GimpHistogramChannel    as HistogramChannel
from gp2_enums import GimpMergeType           as MergeType
from gp2_enums import GimpSizeType            as SizeType
from gp2_enums import GimpChannelOps          as ChannelOps
from gp2_enums import GimpImageType           as ImageType
from gp2_enums import GimpFillType            as FillType
from gp2_enums import GimpImageBaseType       as ImageBaseType
from gp2_enums import GimpBrushGeneratedShape as BrushGenShape
from gp2_enums import GimpAddMaskType         as AddMaskType
from gp2_enums import GimpPrecision           as Precision
from gp2_enums import GimpDesaturateMode      as DesaturateMode
from gp2_enums import GimpHueRange            as HueRange

# ==============================================================================
WarningPrefix="Warning [gp2func.py] in "


def tobytes( chaine , code = 'utf-8' ) :
    """ conversion du chaine de caractere en byte en python 3"""
    try:
        bCond = type(chaine) is str
        if bCond :
            return bytes(str(chaine).encode(code, errors='ignore'))
    except: # python 2.7
        pass
    return chaine
    
def set_color(color):
    len_color = len(color)
    if len_color == 4 :  # RGBA
        color = ColorRGB(color[0],color[1],color[2],color[3])
    if len_color == 3 : # RGB
        color = ColorRGB(color[0],color[1],color[2],0)
    if  len_color == 1 : # Grayscale
        color = ColorRGB(color[0],color[0],color[0],0)
    return color

# ==============================================================================
def context_set_foreground( color ):
    return gimp2.gimp_context_set_foreground( byref(set_color(color)) )

def context_set_background( color ):
    return gimp2.gimp_context_set_background( byref(set_color(color)) )

def context_get_foreground( ):
    color_tmp =  ColorRGB()
    cr = gimp2.gimp_context_get_foreground( byref(color_tmp) )
    return (cr, (color_tmp.r,color_tmp.g,color_tmp.b,color_tmp.a ) )

def context_get_background( ):
    color_tmp =  ColorRGB()
    cr = gimp2.gimp_context_get_background( byref(color_tmp) )
    return (cr, (color_tmp.r,color_tmp.g,color_tmp.b,color_tmp.a ) )


def context_set_brush_size( size ):
    return gimp2.gimp_context_set_brush_size(c_double(size))
    
def context_push():
    return gimp2.gimp_context_push()
    
def context_pop():
    return gimp2.gimp_context_pop()
    
def context_set_brush(brush):
    return gimp2.gimp_context_set_brush(brush)

def context_get_brush():
    gimp2.gimp_context_get_brush.restype = c_char_p
    return gimp2.gimp_context_get_brush()

# ------------------------------------------------------------------------------
def text_get_extents_fontname( text, size, size_type, font ):
    if size <= 0 :
        return ( False,(0,0,0,0))

    if not size_type in SizeType :
        size_type = "PIXELS"
        traceback.print_stack()
        print( WarningPrefix + __name__  + " unknown size type ", size_type )

    width   = c_int(0)
    height  = c_int(0)
    ascent  = c_int(0)
    descent = c_int(0)

    cr = gimp2.gimp_text_get_extents_fontname(tobytes(text),c_double(size), SizeType[size_type],
                                             tobytes(font),
                                             byref(width),byref(height),
                                             byref(ascent),byref(descent)   );

    return (cr,(width.value,height.value,ascent.value,descent.value))

def  text_fontname(  img, drawable, x, y, text, border, antialias, size, size_type, fontname) :
    if not size_type in SizeType :
        size_type = "PIXELS"
        traceback.print_stack()
        print( WarningPrefix + __name__  + " unknown size type ", size_type )

    return gimp2.gimp_text_fontname(  img, drawable,
                    c_double(x), c_double(y), tobytes(text),
                    border, antialias,
                    c_double(size), SizeType[size_type], tobytes(fontname))

# ------------------------------------------------------------------------------
def get_layers( img_id ):
    layers=[]
    num_layers = c_int(0)
    gimp2.gimp_image_get_layers(img_id, byref(num_layers))
    if num_layers.value > 0 :
        gimp2.gimp_image_get_layers.restype = POINTER(c_int*num_layers.value)
        ptr_items = gimp2.gimp_image_get_layers(img_id, byref(num_layers))

        for ii in range( num_layers.value ) :
            layers.append( (ptr_items.contents)[ii] )

    return layers

# ------------------------------------------------------------------------------
def image_list( ):
    images=[]
    nbret = c_int(0)
    gimp2.gimp_image_list(byref(nbret))
    if nbret.value > 0 :
        gimp2.gimp_image_list.restype = POINTER(c_int*nbret.value)
        ptr_items = gimp2.gimp_image_list(byref(nbret))

        for ii in range( nbret.value ) :
            images.append( (ptr_items.contents)[ii] )

    return images

# ==============================================================================

def item_is_group(item):
    return gimp2.gimp_item_is_group(item)

def item_is_vectors(item):
    return gimp2.gimp_item_is_vectors(item)

def item_delete(item):
    return gimp2.gimp_item_delete(item)

def item_set_name(item, name):
    return gimp2.gimp_item_set_name(item, tobytes(name) )

def item_get_name(item):
    gimp2.gimp_item_get_name.restype = c_char_p
    return gimp2.gimp_item_get_name(item )

def item_transform_rotate(item,angle,autocenter,x,y ):
    return gimp2.gimp_item_transform_rotate( item, c_double(angle), autocenter,
                                            c_double(x),c_double(y))
def item_set_visible(item,visible):
    return gimp2.gimp_item_set_visible(item,visible)

def item_get_visible(item):
    return gimp2.gimp_item_get_visible(item)

def item_set_expanded(item,expanded):
    return gimp2.gimp_item_set_expanded(item,expanded)
# ==============================================================================
def item_transform_matrix(item, coef00,coef01, coef02, coef10,coef11, coef12, coef20,coef21, coef22):
    return gimp2.gimp_item_transform_matrix( item, c_double(coef00),c_double(coef01), c_double(coef02), 
                                                   c_double(coef10),c_double(coef11), c_double(coef12), 
                                                   c_double(coef20),c_double(coef21), c_double(coef22) )
    
def item_transform_translate( item, x_offset, y_offset):
    return gimp2.gimp_item_transform_translate( item, c_double(x_offset),c_double(y_offset) ) 

# ==============================================================================
def plugin_enable_precision():
    return gimp2.gimp_plugin_enable_precision()

def display_new(timg):
    return gimp2.gimp_display_new(timg)

def display_delete(timg):
    return gimp2.gimp_display_delete(timg)

def displays_flush():
    return gimp2.gimp_displays_flush()

# ==============================================================================
def image_get_precision( img ):
    return gimp2.gimp_image_get_precision( img )

def image_undo_disable(timg):
    return gimp2.gimp_image_undo_disable(timg)
    
def image_undo_enable(timg):
    return gimp2.gimp_image_undo_enable(timg)
    
def image_undo_group_start(timg):
    return gimp2.gimp_image_undo_group_start(timg)

def image_undo_group_end(timg):
    return gimp2.gimp_image_undo_group_end(timg)

def image_duplicate(timg):
    return gimp2.gimp_image_duplicate(timg)

def image_delete(timg):
    return gimp2.gimp_image_delete(timg)

def image_flatten(timg):
    return gimp2.gimp_image_flatten(timg)

def image_insert_layer( img,layer, parent, position ) :
    return gimp2.gimp_image_insert_layer(img,layer, parent, position)

def image_set_active_layer( img, layer ) :
    return gimp2.gimp_image_set_active_layer( img, layer )

def image_merge_down( img, layer, merge_type):
    if not merge_type in MergeType :
        traceback.print_stack()
        print( WarningPrefix + __name__  + " unknown merge type ", merge_type )
        merge_type="EXPAND_AS_NECESSARY"
    return gimp2.gimp_image_merge_down( img, layer, MergeType[merge_type])

def image_get_active_drawable( img ) :
    return gimp2.gimp_image_get_active_drawable( img )

def image_get_filename( img ):
    gimp2.gimp_image_get_filename.restype = c_char_p
    return gimp2.gimp_image_get_filename( img )

def image_set_filename( img , filename ):
    return gimp2.gimp_image_set_filename( img , tobytes(filename) )

def image_convert_rgb( img ) :
    return gimp2.gimp_image_convert_rgb(img)

def image_width(img):
    return gimp2.gimp_image_width(img)

def image_height (img):
    return gimp2.gimp_image_height(img)

def image_scale( img, width, height ) :
    return gimp2.gimp_image_scale(img, int(width+0.5), int(height+0.5) )

def image_resize( img, width, height, offx, offy) :
    return gimp2.gimp_image_resize( img, int(width), int(height), int(offx), int(offy))

def image_lower_layer_to_bottom(img, layer) :
    return gimp2.gimp_image_lower_layer_to_bottom(img, layer)

def image_add_layer(img, layer, position):
    traceback.print_stack()
    print( WarningPrefix + __name__  + " *** deprecated *** " )
    return gimp2.gimp_image_add_layer(img, layer, position)

def image_remove_layer(img, layer):
    return gimp2.gimp_image_remove_layer(img, layer)

def image_select_color(img, operation, drawable, color):
    if not operation in ChannelOps :
        traceback.print_stack()
        print( WarningPrefix + __name__  + " unknown ChannelOps ", operation )
        operation="REPLACE"
    return gimp2.gimp_image_select_color(img, ChannelOps[operation], drawable, byref(set_color(color)) )

def image_new_with_precision(width,height,basetype,precision):
    if not basetype in ImageBaseType :
        traceback.print_stack()
        print( WarningPrefix + __name__  + " unknown ImageBaseType ", basetype )
        basetype="RGB"
    if not precision in Precision :
        traceback.print_stack()
        print( WarningPrefix + __name__  + " unknown Precision ", precision )
        precision="U8_LINEAR"
    return gimp2.gimp_image_new_with_precision(width,height,ImageBaseType[basetype],Precision[precision])

def image_base_type(img) :
    value = gimp2.gimp_image_base_type(img)
    for key in ImageBaseType.keys() :
        if ImageBaseType[key] == value :
            return key
    return ""

def image_reorder_item(img,item,parent,position):
    return gimp2.gimp_image_reorder_item(img,item,parent,position)

def image_lower_item(img,item):
    return gimp2.gimp_image_lower_item(img,item)
    

# ==============================================================================
def drawable_levels(drawable,channel,low_in, high_in, clamp_in, gamma, low_out, high_out, clamp_out):
    if not channel in HistogramChannel :
        traceback.print_stack()
        print( WarningPrefix + __name__  + " unknown channel ", channel )
        channel='VALUE'
    return gimp2.gimp_drawable_levels(drawable,HistogramChannel[channel],
            c_double(low_in), c_double(high_in), c_int(clamp_in),
            c_double(gamma),
            c_double(low_out), c_double(high_out), c_int(clamp_out))

def drawable_is_rgb( drawable):
    return gimp2.gimp_drawable_is_rgb(drawable)

def drawable_width(drawable):
    return gimp2.gimp_drawable_width(drawable)

def drawable_height(drawable):
    return gimp2.gimp_drawable_height(drawable)

def drawable_has_alpha(drawable):
    return gimp2.gimp_drawable_has_alpha(drawable)

def layer_add_alpha( layer ) :
    return gimp2.gimp_layer_add_alpha( layer )

def drawable_get(drawable):
    gimp2.gimp_drawable_get.restype= POINTER(SDrawable)
    return gimp2.gimp_drawable_get(drawable)

def drawable_update(drawable, x, y, width, height):
    return gimp2.gimp_drawable_update(drawable, int(x), int(y), int(width), int(height) )

def drawable_equalize(drawable, mask_only):
    return gimp2.gimp_drawable_equalize(drawable, mask_only)

def drawable_fill(drawable, fill_type):
    if not fill_type in FillType :
        traceback.print_stack()
        print( WarningPrefix + __name__  + " unknown fill type ", fill_type )
        fill_type='FOREGROUND_FILL'
    return gimp2.gimp_drawable_fill(drawable, FillType[fill_type])

def drawable_get_buffer(drawable):
    gimp2.gimp_drawable_get_buffer.restype = POINTER(GeglBuffer)
    return gimp2.gimp_drawable_get_buffer(drawable)

def drawable_get_shadow_buffer(drawable):
    gimp2.gimp_drawable_get_shadow_buffer.restype = POINTER(GeglBuffer)
    return gimp2.gimp_drawable_get_shadow_buffer(drawable)

def drawable_mask_intersect(drawable):
    x = c_int (0)
    y = c_int (0)
    w = c_int (0)
    h = c_int (0)
    cr = gimp2.gimp_drawable_mask_intersect(drawable, byref(x), byref(y), byref(w), byref(h))
    return ( cr, (x.value, y.value, w.value, h.value) )

def drawable_merge_shadow(drawable,undo):
    return gimp2.gimp_drawable_merge_shadow(drawable,undo)
    
def drawable_flush():
    return gimp2.gimp_drawable_flush()

def drawable_type(drawable):
    drw_type = gimp2.gimp_drawable_type(drawable)
    for key in ImageType.keys() :
        if ImageType[key] == drw_type :
            return key
    return ""
def drawable_histogram(id_drw, chan, start_range, end_range ):
    if not chan in HistogramChannel :
        traceback.print_stack()
        print( WarningPrefix + __name__  + " unknown channel ", chan )
        chan='VALUE'
    mean       = c_double(0)
    std_dev    = c_double(0)
    median     = c_double(0)
    pixels     = c_double(0)
    count      = c_double(0)
    percentile = c_double(0)

    cr = gimp2.gimp_drawable_histogram (id_drw, HistogramChannel[chan],
                        c_double(start_range), c_double(end_range),
                        byref(mean), byref(std_dev), byref(median),
                        byref(pixels),byref(count),byref(percentile))

    if cr :
        ret_values=( mean.value, std_dev.value, median.value, pixels.value, count.value, percentile.value)
    else:
        ret_values = ( 0, 0, 0, 0, 0, 0 )
    return (cr,ret_values)

def drawable_edit_fill(drawable,fill_type) :
    if not fill_type in FillType :
        traceback.print_stack()
        print( WarningPrefix + __name__  + " unknown fill type ", fill_type )
        fill_type='FOREGROUND_FILL'
    return gimp2.gimp_drawable_edit_fill(drawable, FillType[fill_type])
    
    
def drawable_desaturate(drawable,mode): #gimp2.10
    if  not mode in DesaturateMode :
        traceback.print_stack()
        print( WarningPrefix + __name__  + " unknown Desaturate Mod ", mode )
        mode='LIGHTNESS'
    return gimp2.gimp_drawable_desaturate(drawable,DesaturateMode[mode])
    
def drawable_threshold(drawable, channel, low_threshold,high_threshold): #gimp2.10
    if  not channel in HistogramChannel :
        traceback.print_stack()
        print( WarningPrefix + __name__  + " unknown Desaturate Mod ", channel )
        channel='VALUE'
    return gimp2.gimp_drawable_threshold(drawable,HistogramChannel[channel],
                c_double(low_threshold), c_double(high_threshold) )
    
def drawable_brightness_contrast(drawable, brightness, contrast): 
    return gimp2.gimp_drawable_brightness_contrast(drawable, c_double(brightness), c_double(contrast) )
                
def drawable_invert( drawable, linear ):
    return gimp2.gimp_drawable_invert( drawable, linear )
# ==============================================================================

def selection_all(img):
    return gimp2.gimp_selection_all(img)

def selection_none(img):
    return gimp2.gimp_selection_none(img)

def selection_invert(img):
    return gimp2.gimp_selection_invert(img)

def selection_value(img, x, y):
    return gimp2.gimp_selection_value(img, x, y)

def selection_bounds( img ):
    non_empty = c_int(0)
    x1        = c_int(0)
    y1        = c_int(0)
    x2        = c_int(0)
    y2        = c_int(0)
    cr = gimp2.gimp_selection_bounds( img, byref(non_empty),
                                    byref(x1), byref(y1), byref(x2), byref(y2) )
    if cr :
        selection = ( non_empty.value, x1.value, y1.value, x2.value, y2.value )
    else:
        selection = ( False, 0, 0, 0, 0 )
    #print( selection )
    return (cr,selection)

def image_select_rectangle(img, operation, x, y, width, height ):
    if not operation in ChannelOps :
        traceback.print_stack()
        print( WarningPrefix + __name__  + " unknown Channel Operation ", operation )
        operation="REPLACE"
    return gimp2.gimp_image_select_rectangle( img, ChannelOps[operation] ,
                c_double(x),c_double(y), c_double(width), c_double(height))


def image_select_ellipse(img, operation, x, y, xradius, yradius):
    if not operation in ChannelOps :
        traceback.print_stack()
        print( WarningPrefix + __name__  + " unknown Channel Operation ", operation )
        operation="REPLACE"
    return gimp2.gimp_image_select_ellipse(img, ChannelOps[operation],
                 c_double(x), c_double(y), c_double(xradius), c_double(yradius))

# ==============================================================================
def edit_fill(drawable,fill_type):
    if not fill_type in FillType :
        traceback.print_stack()
        print( WarningPrefix + __name__  + " unknown fill type ", fill_type )
        fill_type='FOREGROUND_FILL'
    return gimp2.gimp_edit_fill( drawable,FillType[fill_type] )

def edit_cut(drawable):
    return gimp2.gimp_edit_cut(drawable)

def edit_copy(drawable):
    return gimp2.gimp_edit_copy(drawable)

def edit_paste(drawable,paste_into):
    return gimp2.gimp_edit_paste(drawable,paste_into)


def floating_sel_anchor( sel_id):
    return gimp2.gimp_floating_sel_anchor(sel_id)

def floating_sel_attach( layer_id, drawable_id):
    return gimp2.gimp_floating_sel_attach(layer_id, drawable_id)

# ==============================================================================
def pixel_rgn_init(drawable, x, y, width, height, dirty,shadow ):
    sdrawable = drawable_get(drawable)
    sPixelRgn = PixelRgn()
    gimp2.gimp_pixel_rgn_init(byref(sPixelRgn), sdrawable, x, y, width, height, dirty, shadow )
    return sPixelRgn

def pixel_rgn_get_rect(sPixelRgn, x, y, width, height) :
    region_type    = c_byte * ( width * height * sPixelRgn.bpp )
    bufImgRgn = region_type()
    gimp2.gimp_pixel_rgn_get_rect(byref(sPixelRgn),byref(bufImgRgn),  x, y, width, height)
    return bufImgRgn

def pixel_rgn_set_rect(sPixelRgn, bufRgn, x, y, width, height):
    gimp2.gimp_pixel_rgn_set_rect(byref(sPixelRgn),byref(bufRgn),x, y, width, height)


# ==============================================================================

def layer_copy(layer,add_alpha):
    return gimp2.gimp_layer_copy(layer,add_alpha)

def layer_set_mode(layer,mode) :
    # ancien  mode 
    #if not mode in LayerModeEffects :
    #    traceback.print_stack()
    #    print( WarningPrefix + __name__  + " unknown Layer Mode Effects ", mode )
    #    mode = 'NORMAL'
    # nouveau  mode  2.10
    if not mode in LayerMode :
        traceback.print_stack()
        print( WarningPrefix + __name__  + " unknown Layer Mode Effects ", mode )
        mode = 'NORMAL'
    return gimp2.gimp_layer_set_mode(layer,LayerMode[mode])

def layer_get_mode(layer) :
    return gimp2.gimp_layer_get_mode(layer)

def layer_set_opacity(layer,opacity) :
    return gimp2.gimp_layer_set_opacity(layer,c_double(opacity))

def layer_get_opacity(layer) :
    gimp2.gimp_layer_get_opacity.restype = c_double
    return gimp2.gimp_layer_get_opacity(layer)

def layer_new_from_drawable(layer,img) :
    return gimp2.gimp_layer_new_from_drawable(layer,img)

def layer_new( img, name, width, height, img_type, opacity, effect_mode):
    if  not img_type in  ImageType:
        traceback.print_stack()
        print( WarningPrefix + __name__  + " unknown image type ", img_type )
        img_type='RGBA_IMAGE'
    #if  not effect_mode in  LayerModeEffects:
    #    traceback.print_stack()
    #    print( WarningPrefix + __name__  + " unknown layer mode effects ", effect_mode )
    #    effect_mode='NORMAL'
    if  not effect_mode in  LayerMode:
        traceback.print_stack()
        print( WarningPrefix + __name__  + " unknown layer mode effects ", effect_mode )
        effect_mode='NORMAL'

    return gimp2.gimp_layer_new(img, tobytes(name), int(width), int(height),
                ImageType[img_type], c_double(opacity), LayerMode[effect_mode])

def layer_group_new(img):
    return gimp2.gimp_layer_group_new(img)

def layer_create_mask( layer, masktype ) :
    if  not masktype in  AddMaskType:
        traceback.print_stack()
        print( WarningPrefix + __name__  + " unknown image type ", masktype )
        masktype='BLACK'
    return gimp2.gimp_layer_create_mask( layer, AddMaskType[masktype])

def layer_add_mask(layer,mask_layer):
    return gimp2.gimp_layer_add_mask(layer,mask_layer)

def layer_set_edit_mask(layer,edit_mask):
    return gimp2.gimp_layer_set_edit_mask(layer,edit_mask)

def layer_resize_to_image_size(layer):
    return gimp2.gimp_layer_resize_to_image_size(layer)
    
# ==============================================================================
def progress_init(message):
    return gimp2.gimp_progress_init(tobytes(message))
def progress_end():
    return gimp2.gimp_progress_end()
def progress_pulse():
    return gimp2.gimp_progress_pulse()
def progress_update(percentage):
    return gimp2.gimp_progress_update(c_double(percentage) )

def message(msg):
    return gimp2.gimp_message( tobytes(str(msg)) )

def progress_set_text(msg):
    return gimp2.gimp_progress_set_text(tobytes(str(msg)) )
# ==============================================================================

def brush_new( brushname):
    gimp2.gimp_brush_new.restype = c_char_p
    return gimp2.gimp_brush_new(tobytes(brushname))

def brush_delete(brush):
    return gimp2.gimp_brush_delete(brush)

def brush_set_hardness( brush, hardness_in) :
    return gimp2.gimp_brush_set_hardness(brush, c_double(hardness_in))

def brush_set_shape(brush, genshape):
    if  not genshape in  BrushGenShape:
        traceback.print_stack()
        print( WarningPrefix + __name__  + " unknown Brush Generated Shape ", genshape )
        genshape="CIRCLE"
    return gimp2.gimp_brush_set_shape(brush, BrushGenShape[genshape])

def brush_set_spacing(brush, spacing):
    return gimp2.gimp_brush_set_spacing(brush,spacing)

# ==============================================================================
def image_get_active_vectors(img):
    return gimp2.gimp_image_get_active_vectors(img)

def image_remove_vectors(image, vector_id):
    return gimp2.gimp_image_remove_vectors(image, vector_id)

def image_add_vectors(image, vector_id, position):
    return gimp2.gimp_image_add_vectors(image, vector_id, position)

def edit_stroke_vectors(layer, vector_id):
    return gimp2.gimp_edit_stroke_vectors(layer, vector_id)

def vectors_new(image, vector_name):
    return gimp2.gimp_vectors_new(image, tobytes(vector_name) )

def vectors_bezier_stroke_new_ellipse(vector_id, x, y, xradius, yradius, angle):
    return gimp2.gimp_vectors_bezier_stroke_new_ellipse(vector_id,
                    c_double(x), c_double(y),
                    c_double(xradius), c_double(yradius), c_double(angle))

def vectors_get_strokes( vector_id,size_max=10):
        num_strokes = c_int(0)
        gimp2.gimp_vectors_get_strokes.restype = POINTER(c_int*size_max)
        ptr_stroke_ids = gimp2.gimp_vectors_get_strokes(vector_id,byref(num_strokes))
        return (num_strokes.value, ptr_stroke_ids)

def vectors_stroke_get_points(vector_id,stroke_id,max_points =30):
    num_points    = c_int(max_points)
    closed        = c_bool(0)
    controlpoints = POINTER(c_double*max_points)()

    vectors_type = gimp2.gimp_vectors_stroke_get_points(vector_id, stroke_id,
                           byref(num_points), byref(controlpoints), byref(closed))
    return ( vectors_type, num_points.value, controlpoints.contents, closed.value )

def vectors_stroke_new_from_points( vectorsId, stroke_type, list_points, close ) :
    # infos_par_point = 6 # {controle1} {centre} {controle2}
    closed          = c_bool(0)
    num_points      = len(list_points)
    controlpoints   = (c_double*num_points)()
    for ii in range(num_points):
        controlpoints[ii] = list_points[ii]

    return gimp2.gimp_vectors_stroke_new_from_points(vectorsId, stroke_type,
                        num_points, controlpoints, closed )

def vectors_stroke_rotate(vector_id, stroke_id, x, y, angle):
    return gimp2.gimp_vectors_stroke_rotate(vector_id, stroke_id,
                                           c_double(x),c_double(y),c_double(angle))

# ==============================================================================
def pencil( drawable, strokes ):
    num_strokes = len(strokes)
    dstrokes    = (c_double*num_strokes)()
    for ii in range(num_strokes):
        dstrokes[ii] = strokes[ii]
    return gimp2.gimp_pencil( drawable, num_strokes, dstrokes )

# ==============================================================================
def hue_saturation(drawable, hue_range, hue_offset, lightness, saturation):
    if  not  hue_range in HueRange :
        traceback.print_stack()
        print( WarningPrefix + __name__  + " unknown Hue Range ", hue_range )
        hue_range="ALL_HUES"
    return gimp2.gimp_hue_saturation(drawable, HueRange[hue_range], 
             c_double(hue_offset), c_double(lightness), c_double(saturation))
             
# ==============================================================================
def file_load(filename):
    return gimp2.gimp_file_load( 0, c_char_p(tobytes(filename)),c_char_p(tobytes(filename)))
    
# ==============================================================================
def run_procedure( proc_name, list_param ):
    ptr_proc_str=c_char_p(proc_name)
    params=[]
    for param in list_param :
        gp = GimpParam()
        gp.type = param[0]
        # TODO : color ...
        if param[0] == ArgType["STRING"] :
            gp.data.d_string=c_char_p(tobytes(param[1]))
        elif param[0] == ArgType["FLOAT"] :
            gp.data.d_float = param[1]
        elif param[0] == ArgType["FLOATARRAY"] :
            arr = (c_double * len(param[1]))(*param[1])
            gp.data.d_floatarray =  arr
        elif param[0] == ArgType["INT32ARRAY"] :
            arr = (c_int * len(param[1]))(*param[1])
            gp.data.d_intarray =  arr
        else:
            gp.data.d_int = param[1]
        params.append(gp)

    GimpParamN = GimpParam * len (params)
    nbret = c_int(0)
    gimp2.gimp_run_procedure2.restype= POINTER(GimpParam)
    ptr_retvalues = gimp2.gimp_run_procedure2(
                                ptr_proc_str , byref(nbret),
                                len (params), GimpParamN (*params)
                        )

    if ptr_retvalues[0].type == ArgType["STATUS"] and  ptr_retvalues[0].data.d_status != StatusType["SUCCESS"]:
        print( "content 0:", ptr_retvalues[0].type , ptr_retvalues[0].data.d_status )
        if ptr_retvalues[0].data.d_status == StatusType["CALLING_ERROR"] :
            print("*** Error *** CALLING_ERROR ***" , proc_name)
        if ptr_retvalues[0].data.d_status == StatusType["EXECUTION_ERROR"] :
            print("*** Error *** EXECUTION_ERROR ***" , proc_name)
        if ptr_retvalues[0].data.d_status == StatusType["PASS_THROUGH"] :
            print("*** Error *** PASS_THROUGH ***" , proc_name)
        if ptr_retvalues[0].data.d_status == StatusType["CANCEL"] :
            print("*** Error *** CANCEL ***" , proc_name)
        try:
            print( "content 1:", ptr_retvalues[1].type )
            print( "          ", ptr_retvalues[1].data.d_status )
        except:
            pass
        return None

    ret_values =[]
    for ii in range(nbret.value) :
        #print( "content:", ii, ptr_retvalues[ii].type , ptr_retvalues[ii].data.d_status )
        ret_values.append(ptr_retvalues[ii].data.d_int)

    return ret_values
