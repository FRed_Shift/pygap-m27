#! /usr/bin/env python
# -*- coding: UTF-8 -*-

# ------------------------------------------------------------------------------
# This program is provided without any guarantee.
#
# The license is  LGPL-v3
# For details, see GNU General Public License, version 3 or later.
#                        "https://www.gnu.org/licenses/gpl.html"
# ------------------------------------------------------------------------------

from ctypes import Union,Structure,POINTER
from ctypes import c_double,c_char_p,c_bool,c_int,c_uint,c_short

# ------------------------------------------------------------------------------
class GeglBuffer(Structure):
    pass

class GimpProgress(Structure):
    pass

# ------------------------------------------------------------------------------
class GimpParamData (Union):
    _fields_ = [ ('d_float'      , c_double),
                 ('d_string'     , c_char_p),
                 ('d_color'      , c_double * 4),
                 ('d_image'      , c_int),
                 ('d_drawable'   , c_int),
                 ('d_status'     , c_int),
                 ('d_int'        , c_int),
                 ('d_floatarray' , POINTER(c_double)),
                 ('d_intarray'   , POINTER(c_int)),
                 ('d_bool'       , c_bool)
                 ]

# ------------------------------------------------------------------------------
class GimpParam (Structure):
    _fields_ = [ ('type', c_int),
                 ('data', GimpParamData) ]

GimpReturnValues = GimpParam * 2

# ------------------------------------------------------------------------------
class GimpRGB(Structure):
    _fields_ = [ ('r', c_double ), ('g', c_double ), ('b', c_double ), ('a', c_double ) ]

# ------------------------------------------------------------------------------
class GimpDrawable (Structure):
    pass

class GimpTile (Structure):
    _fields_ = [ ('ewidth'   , c_uint  ),
                 ('eheight'  , c_uint  ),
                 ('bpp'      , c_uint  ),
                 ('tile_num' , c_uint  ),
                 ('ref_count', c_short ),
                 ('dirty'    , c_uint,1),
                 ('shadow'   , c_uint,1),
                 ('data'     , c_char_p),
                 ('drawable' , POINTER(GimpDrawable) )]

GimpDrawable._fields_ = [   ('data'        , c_int ),
                            ('width'       , c_uint),
                            ('height'      , c_uint),
                            ('bpp'         , c_uint),
                            ('ntile_rows'  , c_uint),
                            ('ntile_cols'  , c_uint),
                            ('tiles'       , POINTER(GimpTile) ),
                            ('shadow_tiles', POINTER(GimpTile) ) ]

# ------------------------------------------------------------------------------
class GimpPixelRgn (Structure):
    _fields_ = [ ('data'         , c_char_p ),
                 ('drawable'     , POINTER(GimpDrawable) ),
                 ('bpp'          , c_uint),
                 ('rowstride'    , c_uint),
                 ('x'            , c_uint),
                 ('y'            , c_uint),
                 ('w'            , c_uint),
                 ('h'            , c_uint),
                 ('dirty'        , c_uint,1),
                 ('shadow'       , c_uint,1),
                 ('process_count', c_uint  )]
