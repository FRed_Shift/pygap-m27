#! /usr/bin/env python
# -*- coding: UTF-8 -*-

# ------------------------------------------------------------------------------
# This program is provided without any guarantee.
#
# The license is  LGPL-v3
# For details, see GNU General Public License, version 3 or later.
#                        "https://www.gnu.org/licenses/gpl.html"
# ------------------------------------------------------------------------------
from ctypes    import c_double, c_void_p
from libgimp2  import gegl

import gp2_func as gimp

from gp2_func  import tobytes
from gp2_enums import GimpPDBArgType     as ArgType
from gp2_enums import GimpRunMode        as RunMode

# ------------------------------------------------------------------------------
def decompose( image, drawable, decompose_type, layer_mode  ):
    return gimp.run_procedure( b"plug-in-decompose",
                           ((ArgType["INT32"]    , RunMode["NONINTERACTIVE"] ),
                            (ArgType["IMAGE"]    , image                     ),
                            (ArgType["DRAWABLE"] , drawable                  ),
                            (ArgType["STRING"]   , tobytes(decompose_type)   ),
                            (ArgType["INT32"]    , layer_mode                ) )
                        )
    #return ret[1:]

# ------------------------------------------------------------------------------
def drawable_compose( image, drawable1, drawable2, drawable3, drawable4, compose_type  ):
    return gimp.run_procedure( b"plug-in-drawable-compose",
                           ((ArgType["INT32"]    , RunMode["NONINTERACTIVE"] ),
                            (ArgType["IMAGE"]    , image                 ),
                            (ArgType["DRAWABLE"] , drawable1             ),
                            (ArgType["DRAWABLE"] , drawable2             ),
                            (ArgType["DRAWABLE"] , drawable3             ),
                            (ArgType["DRAWABLE"] , drawable4             ),
                            (ArgType["STRING"]   , tobytes(compose_type) ) )
                    )

# ------------------------------------------------------------------------------
def compose(id_img, id_drw, id_img2,id_img3,id_img4,compose_type):
    if id_drw  is None : id_drw  = -1
    if id_img2 is None : id_img2 = -1
    if id_img3 is None : id_img3 = -1
    if id_img4 is None : id_img4 = -1
    return gimp.run_procedure( b"plug-in-compose",
                       ((ArgType["INT32"]    , 1             ),
                        (ArgType["IMAGE"]    ,id_img         ),
                        (ArgType["DRAWABLE"] ,id_drw         ),
                        (ArgType["IMAGE"]    ,id_img2        ),
                        (ArgType["IMAGE"]    ,id_img3        ),
                        (ArgType["IMAGE"]    ,id_img4        ),
                        (ArgType["STRING"]   ,tobytes(compose_type) ))
                        )
    #return ret[1:]
# ------------------------------------------------------------------------------
def convmatrix(id_img, id_drw, argc_matrix, matrix, alpha_alg, divisor, offset,
                               argc_channel, channels, bmode ):
    border_mode={ "EXTEND3":0, "WRAP" : 1, "CLEAR":2} 
    bmode=border_mode[bmode]
                              
    return  gimp.run_procedure( b"plug-in-convmatrix",
                       ((ArgType["INT32"]      , RunMode["NONINTERACTIVE"] ),
                        (ArgType["IMAGE"]      , id_img      ),
                        (ArgType["DRAWABLE"]   , id_drw      ),
                        (ArgType["INT32"]      , argc_matrix ),
                        (ArgType["FLOATARRAY"] , matrix      ),
                        (ArgType["INT32"]      , alpha_alg   ),
                        (ArgType["FLOAT"]      , divisor     ),
                        (ArgType["FLOAT"]      , offset      ),
                        (ArgType["INT32"]      , argc_channel),
                        (ArgType["INT32ARRAY"] , channels    ),
                        (ArgType["INT32"]      , bmode       ))
                        )
  
# ------------------------------------------------------------------------------
def colors_channel_mixer(id_img, id_drw, mono, rr_gain, rg_gain, rb_gain,  gr_gain, gg_gain, gb_gain, br_gain, bg_gain, bb_gain ):
    return gimp.run_procedure( b"plug-in-colors-channel-mixer",
                       ((ArgType["INT32"]    , RunMode["NONINTERACTIVE"] ),
                        (ArgType["IMAGE"]    ,id_img    ),
                        (ArgType["DRAWABLE"] ,id_drw    ),
                        (ArgType["INT32"]    ,int(mono) ),
                        (ArgType["FLOAT"]    ,rr_gain   ),
                        (ArgType["FLOAT"]    ,rg_gain   ),
                        (ArgType["FLOAT"]    ,rb_gain   ),
                        (ArgType["FLOAT"]    ,gr_gain   ),
                        (ArgType["FLOAT"]    ,gg_gain   ),
                        (ArgType["FLOAT"]    ,gb_gain   ),
                        (ArgType["FLOAT"]    ,br_gain   ),
                        (ArgType["FLOAT"]    ,bg_gain   ),
                        (ArgType["FLOAT"]    ,bb_gain   ))
                    )
  
# ------------------------------------------------------------------------------
def unsharp_mask(id_img, id_drw, radius, amount, threshold ):
    return gimp.run_procedure( b"plug-in-unsharp-mask",
                       ((ArgType["INT32"]    , RunMode["NONINTERACTIVE"] ),
                        (ArgType["IMAGE"]    ,id_img    ),
                        (ArgType["DRAWABLE"] ,id_drw    ),
                        (ArgType["FLOAT"]    ,radius    ),
                        (ArgType["FLOAT"]    ,amount    ),
                        (ArgType["INT32"]    ,int(threshold) ))
                    )
# ------------------------------------------------------------------------------
def gauss(id_img, id_drw, horizontal, vertical, method ):
    return gimp.run_procedure( b"plug-in-gauss",
                       ((ArgType["INT32"]    , 1        ),
                        (ArgType["IMAGE"]    ,id_img    ),
                        (ArgType["DRAWABLE"] ,id_drw    ),
                        (ArgType["FLOAT"]    ,horizontal),
                        (ArgType["FLOAT"]    ,vertical  ),
                        (ArgType["INT32"]    ,method    ))
                        )
    #return ret[1:]

# ------------------------------------------------------------------------------
def gauss_iir(id_img, id_drw, radius, horizontal, vertical ):
    return gimp.run_procedure( b"plug-in-gauss-iir",
                       ((ArgType["INT32"]    , 1          ),
                        (ArgType["IMAGE"]    ,id_img      ),
                        (ArgType["DRAWABLE"] ,id_drw      ),
                        (ArgType["FLOAT"]    ,radius      ),
                        (ArgType["INT32"]    ,horizontal  ),
                        (ArgType["INT32"]    ,vertical    ) )
                        )

# ------------------------------------------------------------------------------
def rgb_noise(id_img, id_drw, independant, correlated, noise1, noise2, noise3, noise4 ):
    return gimp.run_procedure( b"plug-in-rgb-noise",
                       ((ArgType["INT32"]    , 1          ),
                        (ArgType["IMAGE"]    ,id_img      ),
                        (ArgType["DRAWABLE"] ,id_drw      ),
                        (ArgType["INT32"]    ,independant ),
                        (ArgType["INT32"]    ,correlated  ),
                        (ArgType["FLOAT"]    ,noise1      ),
                        (ArgType["FLOAT"]    ,noise2      ),
                        (ArgType["FLOAT"]    ,noise3      ),
                        (ArgType["FLOAT"]    ,noise4      ) )
                        )


# ------------------------------------------------------------------------------
def png_save(id_img, id_drw, filename ):
    return  gimp.run_procedure( b"file-png-save-defaults",
                       ((ArgType["INT32"]    , RunMode["NONINTERACTIVE"] ),
                        (ArgType["IMAGE"]    ,id_img    ),
                        (ArgType["DRAWABLE"] ,id_drw    ),
                        (ArgType["STRING"]   ,tobytes(filename)  ),
                        (ArgType["STRING"]   ,tobytes(filename)  ))
                    )

# ------------------------------------------------------------------------------
def png_load(filename):
    return  gimp.run_procedure( b"file-png-load",
                       ((ArgType["INT32"] , RunMode["NONINTERACTIVE"] ),
                        (ArgType["STRING"],tobytes(filename)  ),
                        (ArgType["STRING"],tobytes(filename)  ))
                        )

# ------------------------------------------------------------------------------
def gegl_high_pass(tdrawable, std_dev, contrast):
    ( cr, (x, y, w, h) ) = gimp.drawable_mask_intersect(tdrawable)
    if not cr :
        return
    source = gimp.drawable_get_buffer(tdrawable)
    target = gimp.drawable_get_shadow_buffer(tdrawable)
    gegl.gegl_render_op( source, target,  b"gegl:high-pass",
                         b"std-dev"  , c_double(std_dev),
                         b"contrast" , c_double(contrast),
                         c_void_p ())
    gegl.gegl_buffer_flush(target)
    gimp.drawable_merge_shadow(tdrawable, True)
    gimp.drawable_update(tdrawable, x, y, w, h)
    gimp.displays_flush()


def autocrop_layer(id_img, id_drw):
    return  gimp.run_procedure( b"plug-in-autocrop-layer",
                       ((ArgType["INT32"]    ,RunMode["NONINTERACTIVE"] ),
                        (ArgType["IMAGE"]    ,id_img    ),
                        (ArgType["DRAWABLE"] ,id_drw    ))
                    )
    