#! /usr/bin/env python
# -*- coding: UTF-8 -*-

# ------------------------------------------------------------------------------
# This program is provided without any guarantee.
#
# The license is  LGPL-v3
# For details, see GNU General Public License, version 3 or later.
#                        "https://www.gnu.org/licenses/gpl.html"
# ------------------------------------------------------------------------------
import sys
from ctypes import *
from array  import *
from sys    import argv, platform

__ALL__ = [ 'load_library', 'GimpParamData' , 'GimpParam'     ,
            'GimpRunProc' , 'void_FUNC_void', 'GimpPlugInInfo',
            'GimpParamDef', 'GimpReturnValues', 'GimpPDBArgType',
            'GimpHistogramChannel', 'run_procedure',  'GetLayers',
            'SelectionBounds', 'DrawableHistogram', 'GetRegionRect',
            'info_msg', 'GetInfoImg', 'gegl_high_pass'
             ]

def load_library (library_name):
    if platform == "linux" or platform == "linux2":
        library_name = library_name + '.so.0'
    elif platform == "win32":
        from ctypes.util import find_library
        library_name = find_library (library_name + "-0")
    else:
        raise BaseException ("TODO")

    return CDLL (library_name)

gimp = load_library ('libgimp-2.0')

# ------------------------------------------------------------------------------
class GimpParamData (Union):
    _fields_ = [ ('d_float'   , c_double),
                 ('d_string'  , c_char_p),
                 ('d_color'   , c_double * 4),
                 ('d_image'   , c_int),
                 ('d_drawable', c_int),
                 ('d_status'  , c_int),
                 ('d_int'     , c_int),
                 ('d_bool'    , c_bool)
                 ]

# ------------------------------------------------------------------------------
class GimpParam (Structure):
    _fields_ = [ ('type', c_int),
                 ('data', GimpParamData) ]

GimpReturnValues = GimpParam * 2

GimpRunProc = CFUNCTYPE (None,
                         c_char_p,
                         c_int,
                         POINTER (GimpParam),
                         POINTER (c_int),
                         POINTER (POINTER (GimpParam)))

void_FUNC_void = CFUNCTYPE (None)

# ------------------------------------------------------------------------------
class GimpPlugInInfo (Structure):
    _fields_ = [ ('init_proc', void_FUNC_void),
                 ('quit_proc', void_FUNC_void),
                 ('query_proc', void_FUNC_void),
                 ('run_proc', GimpRunProc) ]
    def __init__ (self,
                  query_proc,
                  run_proc,
                  init_proc = void_FUNC_void (),
                  quit_proc = void_FUNC_void ()):
        Structure.__init__ (self)
        self.init_proc  = init_proc
        self.quit_proc  = quit_proc
        self.query_proc = query_proc
        self.run_proc   = run_proc

# ------------------------------------------------------------------------------
class GimpParamDef (Structure):
    _fields_ = [ ('type', c_int),
                 ('name', c_char_p),
                 ('description', c_char_p) ]
# ------------------------------------------------------------------------------
class GimpDrawable (Structure):
    pass

class GimpTile (Structure):
    _fields_ = [ ('ewidth'   , c_uint  ),
                 ('eheight'  , c_uint  ),
                 ('bpp'      , c_uint  ),
                 ('tile_num' , c_uint  ),
                 ('ref_count', c_short ),
                 ('dirty'    , c_uint,1),
                 ('shadow'   , c_uint,1),
                 ('data'     , c_char_p),
                 ('drawable' , POINTER(GimpDrawable) )]

GimpDrawable._fields_ = [   ('data'        , c_int ),
                            ('width'       , c_uint),
                            ('height'      , c_uint),
                            ('bpp'         , c_uint),
                            ('ntile_rows'  , c_uint),
                            ('ntile_cols'  , c_uint),
                            ('tiles'       , POINTER(GimpTile) ),
                            ('shadow_tiles', POINTER(GimpTile) ) ]

# ------------------------------------------------------------------------------
class GimpPixelRgn (Structure):
    _fields_ = [ ('data'         , c_char_p ),
                 ('drawable'     , POINTER(GimpDrawable) ),
                 ('bpp'          , c_uint),
                 ('rowstride'    , c_uint),
                 ('x'            , c_uint),
                 ('y'            , c_uint),
                 ('w'            , c_uint),
                 ('h'            , c_uint),
                 ('dirty'        , c_uint,1),
                 ('shadow'       , c_uint,1),
                 ('process_count', c_uint  )]

# ------------------------------------------------------------------------------
class GimpRGB(Structure):
    _fields_ = [ ('r', c_double ), ('g', c_double ), ('b', c_double ), ('a', c_double ) ]

# ==============================================================================
GimpPDBArgType= {
            "INT32"      : 0,
            "INT16"      : 1,
            "INT8"       : 2,
            "FLOAT"      : 3,
            "STRING"     : 4,
            "INT32ARRAY" : 5,
            "INT16ARRAY" : 6,
            "INT8ARRAY"  : 7,
            "FLOATARRAY" : 8,
            "STRINGARRAY": 9,
            "COLOR"      : 10,
            "ITEM"       : 11,
            "DISPLAY"    : 12,
            "IMAGE"      : 13,
            "LAYER"      : 14,
            "CHANNEL"    : 15,
            "DRAWABLE"   : 16,
            "SELECTION"  : 17,
            "COLORARRAY" : 18,
            "VECTORS"    : 19,
            "PARASITE"   : 20,
            "STATUS"     : 21,
            "END"        : 22
            }

GimpPDBStatusType={
            "EXECUTION_ERROR" : 0,
            "CALLING_ERROR"   : 1,
            "PASS_THROUGH"    : 2,
            "SUCCESS"         : 3,
            "CANCEL"          : 4
    }

GimpLayerMode={
        "NORMAL_LEGACY"            : 0  ,
        "DISSOLVE"                 : 1  ,
        "BEHIND_LEGACY"            : 2  ,
        "MULTIPLY_LEGACY"          : 3  ,
        "SCREEN_LEGACY"            : 4  ,
        "OVERLAY_LEGACY"           : 5  ,
        "DIFFERENCE_LEGACY"        : 6  ,
        "ADDITION_LEGACY"          : 7  ,
        "SUBTRACT_LEGACY"          : 8  ,
        "DARKEN_ONLY_LEGACY"       : 9  ,
        "LIGHTEN_ONLY_LEGACY"      : 10 ,
        "HSV_HUE_LEGACY"           : 11 ,
        "HSV_SATURATION_LEGACY"    : 12 ,
        "HSL_COLOR_LEGACY"         : 13 ,
        "HSV_VALUE_LEGACY"         : 14 ,
        "DIVIDE_LEGACY"            : 15 ,
        "DODGE_LEGACY"             : 16 ,
        "BURN_LEGACY"              : 17 ,
        "HARDLIGHT_LEGACY"         : 18 ,
        "SOFTLIGHT_LEGACY"         : 19 ,
        "GRAIN_EXTRACT_LEGACY"     : 20 ,
        "GRAIN_MERGE_LEGACY"       : 21 ,
        "COLOR_ERASE_LEGACY"       : 22 ,
        "OVERLAY"                  : 23 ,
        "LCH_HUE"                  : 24 ,
        "LCH_CHROMA"               : 25 ,
        "LCH_COLOR"                : 26 ,
        "LCH_LIGHTNESS"            : 27 ,
        "NORMAL"                   : 28 ,
        "BEHIND"                   : 29 ,
        "MULTIPLY"                 : 30 ,
        "SCREEN"                   : 31 ,
        "DIFFERENCE"               : 32 ,
        "ADDITION"                 : 33 ,
        "SUBTRACT"                 : 34 ,
        "DARKEN_ONLY"              : 35 ,
        "LIGHTEN_ONLY"             : 36 ,
        "HSV_HUE"                  : 37 ,
        "HSV_SATURATION"           : 38 ,
        "HSL_COLOR"                : 39 ,
        "HSV_VALUE"                : 40 ,
        "DIVIDE"                   : 41 ,
        "DODGE"                    : 42 ,
        "BURN"                     : 43 ,
        "HARDLIGHT"                : 44 ,
        "SOFTLIGHT"                : 45 ,
        "GRAIN_EXTRACT"            : 46 ,
        "GRAIN_MERGE"              : 47 ,
        "VIVID_LIGHT"              : 48 ,
        "PIN_LIGHT"                : 49 ,
        "LINEAR_LIGHT"             : 50 ,
        "HARD_MIX"                 : 51 ,
        "EXCLUSION"                : 52 ,
        "LINEAR_BURN"              : 53 ,
        "LUMA_DARKEN_ONLY"         : 54 ,
        "LUMA_LIGHTEN_ONLY"        : 55 ,
        "LUMINANCE"                : 56 ,
        "COLOR_ERASE"              : 57 ,
        "ERASE"                    : 58 ,
        "MERGE"                    : 59 ,
        "SPLIT"                    : 60 ,
        "PASS_THROUGH"             : 61
        }
GimpLayerModeEffects = {
        "NORMAL"       :  0,
        "DISSOLVE"     :  1,
        "BEHIND"       :  2,
        "MULTIPLY"     :  3,
        "SCREEN"       :  4,
        "OVERLAY"      :  5,
        "DIFFERENCE"   :  6,
        "ADDITION"     :  7,
        "SUBTRACT"     :  8,
        "DARKEN_ONLY"  :  9,
        "LIGHTEN_ONLY" : 10,
        "HUE"          : 11,
        "SATURATION"   : 12,
        "COLOR"        : 13,
        "VALUE"        : 14,
        "DIVIDE"       : 15,
        "DODGE"        : 16,
        "BURN"         : 17,
        "HARDLIGHT"    : 18,
        "SOFTLIGHT"    : 19,
        "GRAIN_EXTRACT": 20,
        "GRAIN_MERGE"  : 21,
        "COLOR_ERASE"  : 22
    } 
GimpHistogramChannel={
      "VALUE" : 0,
      "RED"   : 1,
      "GREEN" : 2,
      "BLUE"  : 3,
      "ALPHA" : 4
    }

GimpChannelOps={
        "ADD"        : 0,  # Add to the current selection
        "SUBTRACT"   : 1,  # Subtract from the current selection
        "REPLACE"    : 2,  # Replace the current selection
        "INTERSECT"  : 3   # Intersect with the current selection
    }

GimpImageType={
        "RGB_IMAGE"     : 0,
        "RGBA_IMAGE"    : 1,
        "GRAY_IMAGE"    : 2,
        "GRAYA_IMAGE"   : 3,
        "INDEXED_IMAGE" : 4,
        "INDEXEDA_IMAGE": 5
}

GimpMergeType={
        "EXPAND_AS_NECESSARY" : 0 ,
        "CLIP_TO_IMAGE"       : 1 ,
        "CLIP_TO_BOTTOM_LAYER": 2 ,
        "FLATTEN_IMAGE"       : 3
    }
GimpBrushGeneratedShape={
        "CIRCLE"  : 0,
        "SQUARE"  : 1,
        "DIAMOND" : 2
    }
GimpFillType={
        "FOREGROUND_FILL" : 0,
        "BACKGROUND_FILL" : 1,
        "WHITE_FILL"      : 2,
        "TRANSPARENT_FILL": 3,
        "PATTERN_FILL"    : 4
    }
GimpAddMaskType={
        "WHITE"         : 0, # White (full opacity)
        "BLACK"         : 1, # Black (full transparency)
        "ALPHA"         : 2, # Layer's _alpha channel
        "ALPHA_TRANSFER": 3, # Transfer layer's alpha channel
        "SELECTION"     : 4, # Selection
        "COPY"          : 5, # Grayscale copy of layer
        "CHANNEL"       : 6  # C_hannel
    }
GimpSizeType = {
        "PIXELS" : 0,
        "POINTS" :1
    } 
GimpPrecision={
    "U8_LINEAR"         : 100, # 8-bit linear integer
    "U8_NON_LINEAR"     : 150, # 8-bit non-linear integer
    "U8_PERCEPTUAL"     : 175, # 8-bit perceptual integer
    "U16_LINEAR"        : 200, # 16-bit linear integer
    "U16_NON_LINEAR"    : 250, # 16-bit non-linear integer
    "U16_PERCEPTUAL"    : 275, # 16-bit perceptual integer
    "U32_LINEAR"        : 300, # 32-bit linear integer
    "U32_NON_LINEAR"    : 350, # 32-bit non-linear integer
    "U32_PERCEPTUAL"    : 375, # 32-bit perceptual integer
    "HALF_LINEAR"       : 500, # 16-bit linear floating point
    "HALF_NON_LINEAR"   : 550, # 16-bit non-linear floating point
    "HALF_PERCEPTUAL"   : 575, # 16-bit perceptual floating point
    "FLOAT_LINEAR"      : 600, # 32-bit linear floating point
    "FLOAT_NON_LINEAR"  : 650, # 32-bit non-linear floating point
    "FLOAT_PERCEPTUAL"  : 675, # 32-bit perceptual floating point
    "DOUBLE_LINEAR"     : 700, # 64-bit linear floating point
    "DOUBLE_NON_LINEAR" : 750, # 64-bit non-linear floating point
    "DOUBLE_PERCEPTUAL" : 775  # 64-bit perceptual floating point
    ## #ifndef GIMP_DISABLE_DEPRECATED
    ## GIMP_PRECISION_U8_GAMMA      = GIMP_PRECISION_U8_NON_LINEAR,
    ## GIMP_PRECISION_U16_GAMMA     = GIMP_PRECISION_U16_NON_LINEAR,
    ## GIMP_PRECISION_U32_GAMMA     = GIMP_PRECISION_U32_NON_LINEAR,
    ## GIMP_PRECISION_HALF_GAMMA    = GIMP_PRECISION_HALF_NON_LINEAR,
    ## GIMP_PRECISION_FLOAT_GAMMA   = GIMP_PRECISION_FLOAT_NON_LINEAR,
    ## GIMP_PRECISION_DOUBLE_GAMMA  = GIMP_PRECISION_DOUBLE_NON_LINEAR
    ## #endif
    }

# ==============================================================================
InfoPrecision = {
    GimpPrecision["U8_LINEAR"         ]: ["U8_LINEAR"         ,  8, 'B' , 0, (2**8)-1 ],
    GimpPrecision["U8_NON_LINEAR"     ]: ["U8_NON_LINEAR"     ,  8, 'B' , 0, (2**8)-1 ],
    GimpPrecision["U8_PERCEPTUAL"     ]: ["U8_PERCEPTUAL"     ,  8, 'B' , 0, (2**8)-1 ],
    GimpPrecision["U16_LINEAR"        ]: ["U16_LINEAR"        , 16, 'H' , 0, (2**16)-1],
    GimpPrecision["U16_NON_LINEAR"    ]: ["U16_NON_LINEAR"    , 16, 'H' , 0, (2**16)-1],
    GimpPrecision["U16_PERCEPTUAL"    ]: ["U16_PERCEPTUAL"    , 16, 'H' , 0, (2**16)-1],
    GimpPrecision["U32_LINEAR"        ]: ["U32_LINEAR"        , 32, 'L' , 0, (2**32)-1],
    GimpPrecision["U32_NON_LINEAR"    ]: ["U32_NON_LINEAR"    , 32, 'L' , 0, (2**32)-1],
    GimpPrecision["U32_PERCEPTUAL"    ]: ["U32_PERCEPTUAL"    , 32, 'L' , 0, (2**32)-1],
    GimpPrecision["HALF_LINEAR"       ]: ["HALF_LINEAR"       ,  0, '-' , 0, 0      ],
    GimpPrecision["HALF_NON_LINEAR"   ]: ["HALF_NON_LINEAR"   ,  0, '-' , 0, 0      ],
    GimpPrecision["HALF_PERCEPTUAL"   ]: ["HALF_PERCEPTUAL"   ,  0, '-' , 0, 0      ],
    GimpPrecision["FLOAT_LINEAR"      ]: ["FLOAT_LINEAR"      ,-32, 'f' , 0, 1.0    ],
    GimpPrecision["FLOAT_NON_LINEAR"  ]: ["FLOAT_NON_LINEAR"  ,-32, 'f' , 0, 1.0    ],
    GimpPrecision["FLOAT_PERCEPTUAL"  ]: ["FLOAT_PERCEPTUAL"  ,-32, 'f' , 0, 1.0    ],
    GimpPrecision["DOUBLE_LINEAR"     ]: ["DOUBLE_LINEAR"     ,  0, '-' , 0, 0      ],
    GimpPrecision["DOUBLE_NON_LINEAR" ]: ["DOUBLE_NON_LINEAR" ,  0, '-' , 0, 0      ],
    GimpPrecision["DOUBLE_PERCEPTUAL" ]: ["DOUBLE_PERCEPTUAL" ,  0, '-' , 0, 0      ]
}


def GetInfoImg( precision ):
    return InfoPrecision[precision]

# ------------------------------------------------------------------------------

def __tobytes__( chaine , code = 'utf-8' ) :
    try:
            bCond = type(chaine) is str
            if bCond :
                return bytes(str(chaine).encode(code))
    except: # python 2.7
            pass
    return chaine
    
def GetTextBoundingBox( text, size, font ):
    if size <= 0 :
        return (0,0,0,0)
            
    text = __tobytes__(text)    
    font = __tobytes__(font)
    ww = c_int(0)
    hh = c_int(0)
    h0 = c_int(0)
    h1 = c_int(0)
    cr = gimp.gimp_text_get_extents_fontname(text,c_double(size), GimpSizeType["PIXELS"],
                                             font, byref(ww),byref(hh),byref(h0),byref(h1));
    ww = ww.value
    hh = hh.value
    h0 = h0.value
    h1 = h1.value

    return (ww,hh,h0,h1)
    
# ------------------------------------------------------------------------------   
def SetText(timg, x,y,text,fontname, szfont, color, justify ):
    # histo lineaire
    text     = __tobytes__(text)
    fontname = __tobytes__(fontname)
    (tw,th,th0,th1) = GetTextBoundingBox( text, szfont, fontname )
    gimp.gimp_context_set_foreground( color )
    xx=x
    yy=y
    if justify==1 :
        xx = x - tw
    if justify==2 :
        xx = x - tw/2
       
    tlayer=gimp.gimp_text_fontname( timg, -1 , c_double(xx), c_double(yy),
                                 text , -1, True, c_double(szfont),
                                 GimpSizeType["PIXELS"], fontname)
    return (tlayer,(xx,yy,tw,th,th0,th1)) 
    
# ------------------------------------------------------------------------------   

'''
nbret = c_int(0)

ptr_lab_str=c_char_p(b"LAB")
ptr_proc_str=c_char_p(b"plug-in-decompose-registered")
print(ptr_lab_str, ptr_proc_str )
pdb.gimp_run_procedure.restype= POINTER(GimpParam*2)
ptr_retvalues = pdb.gimp_run_procedure(
                            ptr_proc_str , byref(nbret),
                            ArgType["INT32"]    , 1     ,
                            ArgType["IMAGE"]    ,timg     ,
                            ArgType["DRAWABLE"] ,tdrawable,
                            ArgType["STRING"]   , ptr_lab_str    ,
                            ArgType["INT32"]    ,True     ,
                            ArgType["END"]
                    )

print( "nbret:",nbret);
print( "ret: " , ptr_retvalues )
print( "content0:", (ptr_retvalues.contents)[0].type , (ptr_retvalues.contents)[0].data.d_status )
print( "content1:", (ptr_retvalues.contents)[1].type , (ptr_retvalues.contents)[1].data.d_string )
'''
# ------------------------------------------------------------------------------
def  run_procedure( proc_name, list_param ):
    ptr_proc_str=c_char_p(proc_name)
    params=[]
    for param in list_param :
        gp = GimpParam()
        gp.type = param[0]
        # TODO : color ... 
        if param[0] == GimpPDBArgType["STRING"] :
            gp.data.d_string=c_char_p(__tobytes__(param[1]))
        else:
            if param[0] == GimpPDBArgType["FLOAT"] :
                gp.data.d_float = param[1]
            else:
                gp.data.d_int = param[1]
        params.append(gp)

    GimpParamN = GimpParam * len (params)

    nbret = c_int(0)
    gimp.gimp_run_procedure2.restype= POINTER(GimpParam)
    ptr_retvalues = gimp.gimp_run_procedure2(
                                ptr_proc_str , byref(nbret),
                                len (params), GimpParamN (*params)
                        )

    if ptr_retvalues[0].type == GimpPDBArgType["STATUS"] and  ptr_retvalues[0].data.d_status != GimpPDBStatusType["SUCCESS"]:
        print( "content 0:", ptr_retvalues[0].type , ptr_retvalues[0].data.d_status )
        if ptr_retvalues[0].data.d_status == GimpPDBStatusType["CALLING_ERROR"] :
            print("*** Error *** CALLING_ERROR ***" , proc_name)
        if ptr_retvalues[0].data.d_status == GimpPDBStatusType["EXECUTION_ERROR"] :
            print("*** Error *** EXECUTION_ERROR ***" , proc_name)
        if ptr_retvalues[0].data.d_status == GimpPDBStatusType["PASS_THROUGH"] :
            print("*** Error *** PASS_THROUGH ***" , proc_name)
        if ptr_retvalues[0].data.d_status == GimpPDBStatusType["CANCEL"] :
            print("*** Error *** CANCEL ***" , proc_name)
        try:
            print( "content 1:", ptr_retvalues[1].type )
            print( "          ", ptr_retvalues[1].data.d_status )
        except:
            pass
        return None

    ret_values =[]
    for ii in range(nbret.value) :
        #print( "content:", ii, ptr_retvalues[ii].type , ptr_retvalues[ii].data.d_status )
        ret_values.append(ptr_retvalues[ii].data.d_int)

    return ret_values

# ------------------------------------------------------------------------------
def GetLayers( img_id, reverse_ord=False ):
    layers=[]
    num_layers = c_int(0)
    gimp.gimp_image_get_layers(img_id, byref(num_layers))
    if num_layers.value > 0 :
        gimp.gimp_image_get_layers.restype = POINTER(c_int*num_layers.value)
        ptr_items = gimp.gimp_image_get_layers(img_id, byref(num_layers))

        for ii in range( num_layers.value ) :
            layers.append( (ptr_items.contents)[ii] )

        if reverse_ord == True :
            layers.reverse()
    return layers

# ------------------------------------------------------------------------------
def GetImages( reverse_ord=False ):
    images=[]
    nbret = c_int(0)
    gimp.gimp_image_list(byref(nbret))
    if nbret.value > 0 :
        gimp.gimp_image_list.restype = POINTER(c_int*nbret.value)
        ptr_items = gimp.gimp_image_list(byref(nbret))

        for ii in range( nbret.value ) :
            images.append( (ptr_items.contents)[ii] )

        if reverse_ord == True :
            images.reverse()

    return images

# ------------------------------------------------------------------------------
def SelectionBounds( id_img ):
    non_empty = c_int(0)
    x1        = c_int(0)
    y1        = c_int(0)
    x2        = c_int(0)
    y2        = c_int(0)
    cr = gimp.gimp_selection_bounds( id_img, byref(non_empty),
                                    byref(x1), byref(y1), byref(x2), byref(y2) )
    if cr :
        selection = ( non_empty.value, x1.value, y1.value, x2.value, y2.value )
    else:
        selection = ( False, 0, 0, 0, 0 )
    #print( selection )
    return selection

# ------------------------------------------------------------------------------
def DrawableHistogram(id_drw, chan, start_range, end_range ):
    mean       = c_double(0)
    std_dev    = c_double(0)
    median     = c_double(0)
    pixels     = c_double(0)
    count      = c_double(0)
    percentile = c_double(0)

    cr = gimp.gimp_drawable_histogram (id_drw, chan, c_double(start_range), c_double(end_range),
                        byref(mean), byref(std_dev), byref(median), byref(pixels),byref(count),byref(percentile))

    if cr :
        ret_values=( mean.value, std_dev.value, median.value, pixels.value, count.value, percentile.value)
    else:
        ret_values = ( 0, 0, 0, 0, 0, 0 )
    return ret_values

# ------------------------------------------------------------------------------
def GetRegionRect(tdrawable, fmt, x0, y0, width, height ):
    gimp.gimp_drawable_get.restype= POINTER(GimpDrawable)
    sdrawable = gimp.gimp_drawable_get(tdrawable)
    sPixelRgn = GimpPixelRgn()
    gimp.gimp_pixel_rgn_init(byref(sPixelRgn), sdrawable, x0, y0, width, height,False, False )

    region_type    = c_byte * ( width * height * sPixelRgn.bpp )
    srcImgRgn = region_type()
    gimp.gimp_pixel_rgn_get_rect(byref(sPixelRgn),byref(srcImgRgn), x0, y0,width, height)

    srcImgPix = array(fmt)
    try:
        srcImgPix.frombytes(srcImgRgn)
    except: # python 2.7
        srcImgPix.fromstring(srcImgRgn)

    return (sPixelRgn, srcImgRgn, srcImgPix )

# ------------------------------------------------------------------------------

def UpdateRegionRect(tdrawable, sPixelRgn, srcImgRgn,srcImgPix ):
    width  = gimp.gimp_drawable_width (tdrawable)
    height = gimp.gimp_drawable_height(tdrawable)
    try:
        srcImgRgn[:] = srcImgPix.tobytes()
    except: # python 2.7
        srcImgRgn[:] = [ ord(b) for b in srcImgPix.tostring() ]

    gimp.gimp_pixel_rgn_set_rect(byref(sPixelRgn),byref(srcImgRgn),0,0,width, height)
    gimp.gimp_drawable_update(tdrawable, 0, 0, width, height)

# ------------------------------------------------------------------------------
def info_msg(*args ):
    chaine = ""
    for arg in args :
        try:
            bCond = type(arg) is str
        except: # python 2.7
            bCond = type(arg) is types.StringTypes
        if bCond :
            chaine = chaine + arg
        else:
            chaine = chaine + str(arg)
    if sys.version_info[0] < 3 : 
        gimp.gimp_message( chaine )
    else:
        gimp.gimp_message( bytes(str(chaine).encode("utf-8")) )

# ------------------------------------------------------------------------------
def plug_in_decompose(id_img, id_drw, decompose_type, layers_mode):
    ret = run_procedure( b"plug-in-decompose",
                       ((GimpPDBArgType["INT32"]    , 1             ),
                        (GimpPDBArgType["IMAGE"]    ,id_img         ),
                        (GimpPDBArgType["DRAWABLE"] ,id_drw         ),
                        (GimpPDBArgType["STRING"]   ,decompose_type ),
                        (GimpPDBArgType["INT32"]    ,layers_mode    ) )
                        )
    if ret is None :
        print("Abort ...")
        return None

    return ret[1:]

# ------------------------------------------------------------------------------
def plug_in_compose(id_img, id_drw, id_img2,id_img3,id_img4,compose_type):
    #pdb.plug_in_compose(img_R, None, img_V, img_B, None, b"RGB")
    if id_drw  is None : id_drw  = -1
    if id_img2 is None : id_img2 = -1
    if id_img3 is None : id_img3 = -1
    if id_img4 is None : id_img4 = -1

    ret = run_procedure( b"plug-in-compose",
                       ((GimpPDBArgType["INT32"]    , 1             ),
                        (GimpPDBArgType["IMAGE"]    ,id_img         ),
                        (GimpPDBArgType["DRAWABLE"] ,id_drw         ),
                        (GimpPDBArgType["IMAGE"]    ,id_img2        ),
                        (GimpPDBArgType["IMAGE"]    ,id_img3        ),
                        (GimpPDBArgType["IMAGE"]    ,id_img4        ),
                        (GimpPDBArgType["STRING"]   ,compose_type   ))
                        )
    if ret is None :
        print("Abort ...")
        return None

    return ret[1]
# ------------------------------------------------------------------------------
def plug_in_gauss(id_img, id_drw, horizontal, vertical, method ):
    ret = run_procedure( b"plug-in-gauss",
                       ((GimpPDBArgType["INT32"]    , 1        ),
                        (GimpPDBArgType["IMAGE"]    ,id_img    ),
                        (GimpPDBArgType["DRAWABLE"] ,id_drw    ),
                        (GimpPDBArgType["FLOAT"]    ,horizontal),
                        (GimpPDBArgType["FLOAT"]    ,vertical  ),
                        (GimpPDBArgType["INT32"]    ,method    ))
                        )
    if ret is None :
        print("Abort ...")
        return None

    return ret[1:]


# ------------------------------------------------------------------------------
def plug_in_gauss_iir(id_img, id_drw, radius, horizontal, vertical ):
    #pdb.plug_in_gauss_iir(img_grad, layer_grad, rayon_blur, TRUE, TRUE )
    ret = run_procedure( b"plug-in-gauss-iir",
                       ((GimpPDBArgType["INT32"]    , 1          ),
                        (GimpPDBArgType["IMAGE"]    ,id_img      ),
                        (GimpPDBArgType["DRAWABLE"] ,id_drw      ),
                        (GimpPDBArgType["FLOAT"]    ,radius      ),
                        (GimpPDBArgType["INT32"]    ,horizontal  ),
                        (GimpPDBArgType["INT32"]    ,vertical    ) )
                        )
    if ret is None :
        print("Abort ...")
        return

# ------------------------------------------------------------------------------
def plug_in_unsharp_mask(id_img, id_drw, radius, amount, threshold ):
    ret = run_procedure( b"plug-in-unsharp-mask",
                       ((GimpPDBArgType["INT32"]    , 1        ),
                        (GimpPDBArgType["IMAGE"]    ,id_img    ),
                        (GimpPDBArgType["DRAWABLE"] ,id_drw    ),
                        (GimpPDBArgType["FLOAT"]    ,radius    ),
                        (GimpPDBArgType["FLOAT"]    ,amount    ),
                        (GimpPDBArgType["INT32"]    ,threshold ))
                        )
    if ret is None :
        print("Abort ...")
        return None

    return ret[1:]

# ------------------------------------------------------------------------------
def plug_in_png_save(id_img, id_drw, filename ):
    ret = run_procedure( b"file-png-save-defaults",
                       ((GimpPDBArgType["INT32"]    , 1        ),
                        (GimpPDBArgType["IMAGE"]    ,id_img    ),
                        (GimpPDBArgType["DRAWABLE"] ,id_drw    ),
                        (GimpPDBArgType["STRING"]   ,filename  ),
                        (GimpPDBArgType["STRING"]   ,filename  ))
                        )
    if ret is None :
        print("Abort ...")
        return None

    return ret[1:]
    
# ------------------------------------------------------------------------------
def plug_in_png_load(filename):
    ret = run_procedure( b"file-png-load",
                       ((GimpPDBArgType["INT32"]    , 1        ),
                       (GimpPDBArgType["STRING"]    ,filename  ),
                        (GimpPDBArgType["STRING"]   ,filename  ))
                        )
    if ret is None :
        print("Abort ...")
        return None

    return ret[1]

# ------------------------------------------------------------------------------
def gegl_high_pass(tdrawable, std_dev, contrast):
    class GeglBuffer(Structure):
        pass

    proc_name= b"gegl:high-pass"
    gegl     = load_library ('libgegl-0.4')
    gegl.gegl_init (None, None)

    gimp.gimp_drawable_get_shadow_buffer.restype = POINTER (GeglBuffer)
    gimp.gimp_drawable_get_buffer.restype        = POINTER (GeglBuffer)

    x = c_int ()
    y = c_int ()
    w = c_int ()
    h = c_int ()
    if gimp.gimp_drawable_mask_intersect(tdrawable, byref(x), byref(y),
                                                   byref(w), byref(h)):
        source = gimp.gimp_drawable_get_buffer(tdrawable)
        target = gimp.gimp_drawable_get_shadow_buffer(tdrawable)

        gegl.gegl_render_op( source, target, proc_name,
                             b"std-dev"  , c_double(std_dev),
                             b"contrast" , c_double(contrast),
                             c_void_p ())

        gegl.gegl_buffer_flush(target)
        gimp.gimp_drawable_merge_shadow(tdrawable, True)
        gimp.gimp_drawable_update(tdrawable, x, y, w, h)
        gimp.gimp_displays_flush()
