#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# ------------------------------------------------------------------------------
# This program is provided without any guarantee.
#
# The license is  LGPL-v3
# For details, see GNU General Public License, version 3 or later.
#                        "https://www.gnu.org/licenses/gpl.html"
# ------------------------------------------------------------------------------
import sys
import os

# ------------------------------------------------------------------------------
# import local lib
path_exec=os.path.dirname(os.path.dirname( __file__ ))
if not path_exec :
    path_exec='..'
sys.path.append(path_exec)
sys.path.append(os.path.join(path_exec,"gimp2"))
sys.path.append(os.path.join(path_exec,"lib"))
dir_scriptfu = os.path.join(path_exec, "scriptfu")

from pyGapM27 import Pkg,bullet,menu_sun,init_language
from pyGapM27 import info_right0 as info_right
init_language()

import gp2_func as gimp
from gp2_plugin    import CPlugin
from libTools      import check_compatibilty
from libDisk       import CGridGraduation

class CGridDrawingSun( CPlugin):
    def __init__(self):
        shelp = (  _("Draw a grid on the sun image")
              , bullet + _("In first, set a path with three points on the perimeter")
              , bullet + _("And next, launch the plugin") )

        infoRot= _("Grid rotation") + " (°)"
        infoNS = _("Axis") + " " + _('North')+'/'+_('South')
        infoWE = _("Axis") + " " + _('West') +'/'+_('East')
        params = [
               self.Param("COLOR" , "color"    , _("Line color")    , 0 ),
               self.Param("INT32" , "width"    , _("Line width")    , 0 ),
               self.Param("INT32" , "angle"    , _("Angle") + " (°)", 0 ),
               self.Param("INT32" , "rotate"   ,  infoRot           , 0 ),
               self.Param("INT32" , "axisNS"   ,  infoNS            , 0 ),
               self.Param("INT32" , "axisWE"   ,  infoWE            , 0 ),
               self.Param("STRING", "font"     , _("Font")          , ""),
               self.Param("INT32" , "font_size", _("Font size")     , 0 )]

        choixNS= '(list "' + _('None') +'" "' + _('North')+'-'+_('South') +'" "' + _('South')+'-'+_('North') + '" )'
        choixWE= '(list "' + _('None') +'" "' + _('West')+'-'+_('East') +'" "' + _('East')+'-'+_('West') +'" )'
        gui = {
            "color"    : self.Box( "SF-COLOR"     , '(list 255 0 0 )'           ) ,
            "width"    : self.Box( "SF-ADJUSTMENT", '(list 2 1 100 1 1 0 0 )'   ) ,
            "angle"    : self.Box( "SF-ADJUSTMENT", '(list 15 1  45 5 15 0 0 )' ) ,
            "rotate"   : self.Box( "SF-ADJUSTMENT", '(list 0 -90  90 5 15 0 0 )') ,
            "axisNS"   : self.Box( "SF-OPTION"    , choixNS                     ) ,
            "axisWE"   : self.Box( "SF-OPTION"    , choixWE                     ) ,
            "font"     : self.Box( "SF-FONT"      , '"Courier"'                 ) ,
            "font_size": self.Box( "SF-ADJUSTMENT", '(list 24 6 100  1 1 0 0 )' ) }

        CPlugin.__init__( self,
            Pkg+"GridDrawingSun"              , # name
            _("Draw a grid on the Sun image") , # blurb
            shelp                             , # help
            info_right                        , # author,copyright, ...
            _("Draw a grid")                  , # menu_label
            menu_sun                          , # menu path
            "RGB*, GRAY*"                     , # image_types
            params                            , # params
            gui, dir_scriptfu                 ) # gui

    def Processing(self, timg, tdrawable):
        (color, width, angle, rotate,
         axisNS, axisWE, font, font_size) = self.get_parameters_value()

        gimp.plugin_enable_precision() # important sinon traitement en bpp=8
        cr=check_compatibilty(self.proc_name,timg,tdrawable)
        if cr != None : return cr        

        app=CGridGraduation( timg, tdrawable, color, width, angle, rotate,
                              axisNS, axisWE, font, font_size )
        app.Processing()

        return ( "SUCCESS","")

app=CGridDrawingSun()
app.CreerPluginScm()
app.register()
