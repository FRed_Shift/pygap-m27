#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# ------------------------------------------------------------------------------
# This program is provided without any guarantee.
#
# The license is  LGPL-v3
# For details, see GNU General Public License, version 3 or later.
#                        "https://www.gnu.org/licenses/gpl.html"
# ------------------------------------------------------------------------------
import sys
import os
import math

# ------------------------------------------------------------------------------
# import local lib
path_exec=os.path.dirname(os.path.dirname( __file__ ))
if not path_exec :
    path_exec='..'
sys.path.append(path_exec)
sys.path.append(os.path.join(path_exec,"gimp2"))
sys.path.append(os.path.join(path_exec,"lib"))
dir_scriptfu = os.path.join(path_exec, "scriptfu")

from pyGapM27 import Pkg,bullet,menu_sun,init_language
from pyGapM27 import info_right0 as info_right
init_language()

import gp2_func  as gimp
from gp2_plugin    import CPlugin
from libTools      import check_compatibilty
from plug_in       import png_load
from gp2_tools     import SetText

class CAddEarth( CPlugin):
    def __init__(self):
        shelp  = (_("Add the Earth on solar image") + ":"
               , bullet + _("Set the size of the camera pixels")
               , bullet + _("Set the instrument focal")
               , bullet + _("Set the barlow value") )
        params = [
               self.Param("FLOAT" , 'PixelSize' , _('Pixel size')  , 0.0 ),
               self.Param("INT32" , 'Focal'     , _('Focal value') , 0   ),
               self.Param("FLOAT" , 'Barlow'    , _('Barlow value'), 0.0 ),
               self.Param("STRING", 'font'      , _('Font')        , ""  ),
               self.Param("COLOR" , 'font_color', _('Font color')  , (0,0,0,0)),
               self.Param("INT32" , 'font_size' , _('Font size' )  , 0.0) ]
        gui = {
            'PixelSize' : self.Box( "SF-ADJUSTMENT", '(list 2.40 0.01 15.00  0.5 1.0 2 0 )') ,
            'Focal'     : self.Box( "SF-ADJUSTMENT", '(list 1200 0 10000  100 1000 0 0 )'  ) ,
            'Barlow'    : self.Box( "SF-ADJUSTMENT", '(list 1.00 0.01 10.00  0.5 1.0 2 0 )') ,
            'font'      : self.Box( "SF-FONT"      , '"Courier"'                           ) ,
            'font_color': self.Box( "SF-COLOR"     , '(list 255 255 255 )'                 ) ,
            'font_size' : self.Box( "SF-ADJUSTMENT", '(list 12 5 100 1 1 0 1 )'            ) }

        CPlugin.__init__( self,
            Pkg+"AddEarth"                  , # name
            _("Add the Earth on solar image"), # blurb
            shelp                            , # help
            info_right                       , # author,copyright, ...
            _("Add the Earth on solar image"), # menu_label
            menu_sun                         , # menu path
            "RGB*, GRAY*"                    , # image_types
            params                           , # params
            gui, dir_scriptfu                ) # gui

    def Processing(self,timg, tdrawable):
        _unused_ = ( tdrawable ) # unused parameter , suppress pylint warning
        (PixelSize, Focal, Barlow,
         font, font_color, font_size) = self.get_parameters_value()

        gimp.plugin_enable_precision() # important sinon traitement en bpp=8
        cr=check_compatibilty(self.proc_name,timg,tdrawable)
        if cr != None : return cr

        earthpng          = os.path.join( path_exec, "data", "earth.png" )
        img_earth         = png_load(earthpng)[1]

        image_terre_pixel = gimp.image_width(img_earth)

        Distance_T_S      = 149.6E6 # km
        Rt                = 6371    # km
        resolution_sec    = 206*PixelSize/(Barlow*Focal)
        resolution_radian = resolution_sec/3600*math.pi/180
        Pixel_km          = math.tan(resolution_radian)*Distance_T_S
        Terre_en_pixel    = 2*Rt/Pixel_km
        scale             = image_terre_pixel/Terre_en_pixel

        print( image_terre_pixel )
        print( resolution_sec,Pixel_km,Terre_en_pixel,scale )
        print( int(Terre_en_pixel), "x", int(Terre_en_pixel) )

        gimp.image_undo_group_start(timg)

        gimp.image_scale( img_earth, Terre_en_pixel, Terre_en_pixel)
        layerEarth = gimp.get_layers(img_earth)[0]

        # copie d'un calque dans une autre imge
        layer_cpy = gimp.layer_new_from_drawable(layerEarth,timg)
        gimp.image_delete(img_earth)

        # insertion du calque a la position courante (hors group)
        gimp.image_insert_layer(timg,layer_cpy, None,-1)
        gimp.image_set_active_layer( timg, layer_cpy)
        gimp.item_set_name(layer_cpy,b"Earth")

        tab = "    "
        txt_fmt = _("Information") + ": \n"
        txt_fmt+= tab + _("Focal") + ":  %d mm\n" % ( Focal  )
        if Barlow != 1 :
            txt_fmt+= tab + _("Barlow") + ":  %.2f x\n" % ( Barlow  )
        txt_fmt+= tab + _("Pixel size") + ":  %.2f µm\n\n" % ( PixelSize  )

        txt_fmt+= tab + _("Separation Capacity") + ":  %.2f ''arc\n" % ( resolution_sec  )
        txt_fmt+= tab + _("Average distance") + " " + _("Earth")+"-" +_("Sun") + ": %.1fE6 km\n" % ( Distance_T_S/1.0E6  )
        txt_fmt+= tab + _("Scale") + ":  1 pixels / %.1f km\n" % ( Pixel_km  )
        txt_fmt+= tab + _("Earth") + ":  %.1f x %.1f pixels\n" % ( Terre_en_pixel,Terre_en_pixel  )
        SetText(timg, 10, 1.5*Terre_en_pixel, txt_fmt, font, font_size, font_color, 0 )

        gimp.image_undo_group_end(timg)

        gimp.displays_flush()

        return ( "SUCCESS", "")

app=CAddEarth()
app.CreerPluginScm()
app.register()
