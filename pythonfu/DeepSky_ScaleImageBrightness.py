#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# ------------------------------------------------------------------------------
# This program is provided without any guarantee.
#
# The license is  LGPL-v3
# For details, see GNU General Public License, version 3 or later.
#                        "https://www.gnu.org/licenses/gpl.html"
# ------------------------------------------------------------------------------
#
#   Python-Enhance-Dynamic-Range - Improve contrast in deep sky images
#   Copyright (C) 2017  Bill Smith
#
import sys
import os

# ------------------------------------------------------------------------------
# import local lib
path_exec=os.path.dirname(os.path.dirname( __file__ ))
if not path_exec :
    path_exec='..'
sys.path.append(path_exec)
sys.path.append(os.path.join(path_exec,"gimp2"))
sys.path.append(os.path.join(path_exec,"lib"))
dir_scriptfu = os.path.join(path_exec, "scriptfu")

from pyGapM27 import Pkg,menu_enhancement,init_language
from pyGapM27 import info_right1 as info_right
init_language()

import gp2_func  as gimp
from gp2_plugin    import CPlugin
from libTools      import check_compatibilty
from libTools      import scale_image_brightness

class CScaleImageBrightness( CPlugin):    
    def __init__(self):
        shelp  = _("Scale the image brightness by factor 1.0 to 5.0") 
        params = [self.Param("FLOAT", "bright", _('Brightness factor'), 0.0)]
        gui = { "bright": self.Box("SF-ADJUSTMENT", '(list  1.0 1.0 5.00  0.1  1.0 1 0 )') }

        CPlugin.__init__( self,
            Pkg+"ScaleImageBrightness"      , # name
            _("Scale the image brightness") , # blurb
            shelp                           , # help
            info_right                      , # author,copyright, ...
            _("Scale image brightness")     , # menu_label
            menu_enhancement                , # menu path
            "RGB*"                          , # image_types
            params                          , # params
            gui, dir_scriptfu               ) # gui

    def Processing(self, timg, tdrawable):
        ''' This GIMP plugin scales the brightness of an image layer.

        Parameters:
        timg     : The currently selected image.
        tdrawable: The layer of the currently selected image.
        bright : The brightness factor (1.0 to 5.0)
        '''
        (img, layer) = (timg, tdrawable)
        (bright, ) = self.get_parameters_value()

        gimp.plugin_enable_precision() # important sinon traitement en bpp=8
        cr=check_compatibilty(self.proc_name,timg,tdrawable)
        if cr != None : return cr

        scale_image_brightness(img, layer, bright)
        
        return ( "SUCCESS", "")

app=CScaleImageBrightness()
app.CreerPluginScm()
app.register()
