#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# ------------------------------------------------------------------------------
# This program is provided without any guarantee.
#
# The license is  LGPL-v3
# For details, see GNU General Public License, version 3 or later.
#                        "https://www.gnu.org/licenses/gpl.html"
# ------------------------------------------------------------------------------
import sys
import os

# ------------------------------------------------------------------------------
# import local lib
path_exec=os.path.dirname(os.path.dirname( __file__ ))
if not path_exec :
    path_exec='..'
sys.path.append(path_exec)
sys.path.append(os.path.join(path_exec,"gimp2"))
sys.path.append(os.path.join(path_exec,"lib"))
dir_scriptfu = os.path.join(path_exec, "scriptfu")

from pyGapM27 import Pkg,menu_layertools,init_language
from pyGapM27 import info_right0 as info_right
init_language()

import gp2_func as gimp
from gp2_plugin    import CPlugin
from libTools      import check_compatibilty,copy_layer_to_image

class CMergeAll( CPlugin):
    def __init__(self):
        shelp = ( _("Merge all visible layers into one.")
              , _("Note that all the layers are kept")  )
        params = [ ]
        gui    = {  }

        CPlugin.__init__( self,
            Pkg+"MergeLayers"               , # name
           _("Merge All Layers in 1 layer") , # blurb
            shelp                           , # help
            info_right                      , # author,copyright, ...
            _("Merge All Layers in 1 layer"), # menu_label
            menu_layertools                 , # menu path
            "RGB*, GRAY*"                   , # image_types
            params                          , # params
            gui, dir_scriptfu               ) # gui

    def Processing(self, timg, tdrawable):
        gimp.plugin_enable_precision() # important sinon traitement en bpp=8
        cr=check_compatibilty(self.proc_name,timg,tdrawable)
        if cr != None : return cr        

        gimp.image_undo_group_start(timg)

        img_cpy = gimp.image_duplicate(timg)
        layer_flatten =gimp.image_flatten(img_cpy)
        copy_layer_to_image(img_cpy,layer_flatten,timg,_("flatten layer"),-1)

        gimp.image_undo_group_end(timg)
        gimp.displays_flush()

        return ( "SUCCESS","")

app=CMergeAll()
app.CreerPluginScm()
app.register()
