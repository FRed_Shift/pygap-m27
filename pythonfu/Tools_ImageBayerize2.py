#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# ------------------------------------------------------------------------------
# This program is provided without any guarantee.
#
# The license is  LGPL-v3
# For details, see GNU General Public License, version 3 or later.
#                        "https://www.gnu.org/licenses/gpl.html"
# ------------------------------------------------------------------------------
#
#   Python-Set-Dark-Sky - Set the image dark sky at chosen level.
#   Copyright (C) 2014  Bill Smith
#

import sys
import os

# ------------------------------------------------------------------------------
# import local lib
path_exec=os.path.dirname(os.path.dirname( __file__ ))
if not path_exec :
    path_exec='..'
sys.path.append(path_exec)
sys.path.append(os.path.join(path_exec,"gimp2"))
sys.path.append(os.path.join(path_exec,"lib"))
dir_scriptfu = os.path.join(path_exec, "scriptfu")

from pyGapM27 import Pkg,menu_imagetools,init_language
from pyGapM27 import info_right1 as info_right
init_language()

import gp2_func  as gimp
from gp2_plugin    import CPlugin
from libTools      import check_compatibilty,copy_layer,create_layer

class CImageBayerise2( CPlugin):
    def __init__(self):
        shelp  = _("Convert image to Bayerized form using (editable) colour filter array: [BG][GR]")
        params = [ self.Param("INT32", "bayer_mask", _('Bayer mask'), 0) ]
        gui    = { "bayer_mask" : self.Box("SF-OPTION", '(list "[BGGR]" "[GBRG]" "[GRBG]" "[RGGB]" )')}

        CPlugin.__init__( self,
            Pkg+"ImageBayerise2"                     , # name
            _("Convert image to its Bayerized form") , # blurb
            shelp                                    , # help
            info_right                               , # author,copyright, ...
            _("Bayerize image"  )                    , # menu_label
            menu_imagetools+"/"+_("BAYER matrix")    , # menu path
            "RGB*"                                   , # image_types
            params                                   , # params
            gui, dir_scriptfu          )               # gui

    def Processing(self, timg, tdrawable):
        ''' This GIMP plugin constructs a Bayerized version of an image.

        Parameters:
            timg     : The currently selected image.
            tdrawable: The layer of the currently selected image.
            key      : The colour filter array as string e.g. [BG][GR]
        '''

        gimp.plugin_enable_precision() # important sinon traitement en bpp=8
        cr=check_compatibilty(self.proc_name,timg,tdrawable)
        if cr != None : return cr        

        (img, layer) = (timg, tdrawable)
        ( key, ) = self.get_parameters_value()

        Rouge =(1.0,0.0,0.0,0.0)
        Bleu  =(0.0,0.0,1.0,0.0)
        Vert  =(0.0,1.0,0.0,0.0)
        CFA_GRID=[ (Bleu,Vert,Vert,Rouge), (Vert,Bleu,Rouge,Vert),
                   (Vert,Rouge,Bleu,Vert), (Rouge,Vert,Vert,Bleu) ]
        cfa=CFA_GRID[key]

        gimp.context_push()
        gimp.image_undo_group_start(img)

        layer_cpy = copy_layer(img, layer , _("Bayerized Image"), -1)
        layer_cfa  = create_layer(img, layer_cpy,"CFA","MULTIPLY", -1,100.0)

        for ii in range(4):
            x = ii % 2
            y = ii // 2
            gimp.image_select_rectangle( img, "REPLACE",x, y, 1, 1)
            gimp.context_set_foreground( cfa[ii] )
            gimp.edit_fill(layer_cfa,"FOREGROUND_FILL")

        gimp.image_select_rectangle( img, "REPLACE",0, 0, 2, 2)
        gimp.edit_copy(layer_cfa)
        gimp.drawable_fill(layer_cfa,"PATTERN_FILL")
        gimp.selection_none(img)
        gimp.image_merge_down(img, layer_cfa, "EXPAND_AS_NECESSARY" )

        gimp.image_undo_group_end(img)
        gimp.context_pop()
        gimp.displays_flush()

        return ( "SUCCESS", "")

app=CImageBayerise2()
app.CreerPluginScm()
app.register()
