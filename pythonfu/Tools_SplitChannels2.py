#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# ------------------------------------------------------------------------------
# This program is provided without any guarantee.
#
# The license is  LGPL-v3
# For details, see GNU General Public License, version 3 or later.
#                        "https://www.gnu.org/licenses/gpl.html"
# ------------------------------------------------------------------------------
#
#   Python-Split-Channels - Splits the image into colour channels.
#   Copyright (C) 2015  Bill Smith
#
import sys
import os

# ------------------------------------------------------------------------------
# import local lib
path_exec=os.path.dirname(os.path.dirname( __file__ ))
if not path_exec :
    path_exec='..'
sys.path.append(path_exec)
sys.path.append(os.path.join(path_exec,"gimp2"))
sys.path.append(os.path.join(path_exec,"lib"))
dir_scriptfu = os.path.join(path_exec, "scriptfu")

from pyGapM27 import Pkg,menu_imagetools,init_language
from pyGapM27 import info_right1 as info_right
init_language()

import gp2_func  as gimp
from gp2_plugin    import CPlugin
from libTools      import check_compatibilty,copy_layer,create_layer,fill_layer

class CSplitChannels2( CPlugin):    
    def __init__(self):
        shelp  =  _("Split an image into its colour channels as layers.")
        params = []
        gui = {}

        CPlugin.__init__( self,
            Pkg+"SplitChannels2"           , # name
            _("Split image into colour channels") , # blurb
            shelp                          , # help
            info_right                     , # author,copyright, ...
            _("Split image into colour channels") , # menu_label
            menu_imagetools+"/"+_("Color") , # menu path
            "RGB*"                         , # image_types
            params                         , # params
            gui, dir_scriptfu              ) # gui

    def Processing(self, timg, tdrawable):
        ''' This GIMP plugin splits an image into its colour channels as layers.

        Parameters:
        timg     : The currently selected image.
        tdrawable: The layer of the currently selected image.
        '''
        gimp.plugin_enable_precision() # important sinon traitement en bpp=8
        cr=check_compatibilty(self.proc_name,timg,tdrawable)
        if cr != None : return cr        

        gimp.context_push()        
        gimp.image_undo_group_start(timg)
            
        self.composant(timg, tdrawable, _("Red")  , (1,0,0,0))
        self.composant(timg, tdrawable, _("Green"), (0,1,0,0))
        self.composant(timg, tdrawable, _("Blue") , (0,0,1,0))
        
        gimp.image_remove_layer(timg, tdrawable)
        
        gimp.image_undo_group_end(timg)       
        gimp.context_pop()
        
        gimp.displays_flush()

        return ( "SUCCESS", "")

    def composant(self,img,layer, layername, color): 
        copy_layer(img, layer , layername  , -1)
        mask = create_layer(img,layer,"Mask ...","MULTIPLY",-1)
        fill_layer(mask,color)   
        gimp.image_merge_down(img, mask, "EXPAND_AS_NECESSARY" )        
    
    
app=CSplitChannels2()
app.CreerPluginScm()
app.register()
