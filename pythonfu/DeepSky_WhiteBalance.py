#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# ------------------------------------------------------------------------------
# This program is provided without any guarantee.
#
# The license is  LGPL-v3
# For details, see GNU General Public License, version 3 or later.
#                        "https://www.gnu.org/licenses/gpl.html"
# ------------------------------------------------------------------------------
import sys
import os

# ------------------------------------------------------------------------------
# import local lib
path_exec=os.path.dirname(os.path.dirname( __file__ ))
if not path_exec :
    path_exec='..'
sys.path.append(path_exec)
sys.path.append(os.path.join(path_exec,"gimp2"))
sys.path.append(os.path.join(path_exec,"lib"))
dir_scriptfu = os.path.join(path_exec, "scriptfu")

from pyGapM27 import Pkg,bullet,menu_background,init_language
from pyGapM27 import info_right0 as info_right
init_language()

import gp2_func    as gimp
from gp2_plugin    import CPlugin
from libTools      import check_compatibilty
from libHisto      import CHisto

class CWhiteBalance( CPlugin):
    def __init__(self):
        shelp = ( ("Calculate the White Balance on a deep sky image")
              , bullet + _("In first, select a white area")
              , bullet + _("And next, launch the plugin")  )
        params = [
            self.Param("FLOAT", 'iteration', _("Iteration"), 0 ),
            self.Param("INT32", 'histo'    , _("Histogram"), 0 )]
        gui = {
            'iteration': self.Box("SF-ADJUSTMENT", '(list 3 1 10  1   1 0 0 )') ,
            'histo'    : self.Box("SF-TOGGLE"    , 'FALSE') }

        CPlugin.__init__( self,
            Pkg+"WhiteBalance"                      , # name
            _("White Balance") + " - experimental"  , # blurb
            shelp                                   , # help
            info_right                              , # author,copyright, ...
             _("White Balance") + " - experimental" , # menu_label
            menu_background                         , # menu path
            "RGB*"                                  , # image_types
            params                                  , # params
            gui, dir_scriptfu                       ) # gui

    def Processing(self, timg, tdrawable):
        (iteration, bhisto) = self.get_parameters_value()

        gimp.plugin_enable_precision() # important sinon traitement en bpp=8
        cr=check_compatibilty(self.proc_name,timg,tdrawable)
        if cr != None : return cr

        ss = gimp.selection_bounds(timg)[1]
        if ss[0] == 0 :
            gimp.message( _("Select a white area")
                      + "\n" + _("and next, launch again the function") )
            return ( "SUCCESS","")

        # important sinon traitement en bpp=8
        gimp.plugin_enable_precision()
        gimp.image_undo_group_start(timg)

        bLisse = True
        lissage= 7

        self.algo_rgb(timg, tdrawable, iteration, bLisse, lissage )

        if bhisto :
            chisto = CHisto()
            chisto.Algo( timg, tdrawable, MsgProgress="(*) ")
            chisto.Lissage(demi_prof=1 )
            chisto.Display( timg, tdrawable, bMaxLisse=True )

        gimp.image_undo_group_end(timg)
        gimp.displays_flush()

        return ( "SUCCESS","")

    def algo_rgb(self,timg, tdrawable, iteration,bLisse = False, lissage=3):
        chisto = CHisto()
        chan   = [ "RED", "GREEN","BLUE"]
        correction_max = 0.0
        cnt    =0
        bb = gimp.selection_bounds(timg)[1]
        while  correction_max<0.95 :
            if bb[0] :
                gimp.image_select_rectangle( timg, "REPLACE", bb[1], bb[2],
                                                       bb[3]-bb[1], bb[4]-bb[2])
            chisto.Algo( timg, tdrawable, MsgProgress="(%d) "%(cnt) )
            if bLisse :
                chisto.Lissage(demi_prof=lissage )

            picdroit  = chisto.find_DemiPicDroit(bLisse )
            picgauche = chisto.find_DemiPicGauche(bLisse )
            #print("picdroit:",picdroit)
            #print("picgauche:",picgauche)
            largeur={}
            maxvalue = 0
            for kk in chan:
                largeur[kk] = picdroit[kk][0]-picgauche[kk][0]
                maxvalue = max(maxvalue, largeur[kk] )

            #print("largeur mi-hauteur:",largeur," max:", maxvalue )

            correction_max = 1.0
            for kk in chan:
                correction= float(maxvalue-largeur[kk])/-2.0/float(largeur[kk]) +1.0
                print( kk, ":" , correction )
                if correction > 0  :
                    gimp.drawable_levels(tdrawable, kk, 0.0, correction, False,
                                                        1.0, 0.0, 1.0,False)
                correction_max = min(correction_max,correction)

            #print("correction_max:", correction_max )
            cnt += 1
            if cnt >= iteration :
                break

app=CWhiteBalance()
app.CreerPluginScm()
app.register()
