#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# ------------------------------------------------------------------------------
# This program is provided without any guarantee.
#
# The license is  LGPL-v3
# For details, see GNU General Public License, version 3 or later.
#                        "https://www.gnu.org/licenses/gpl.html"
# ------------------------------------------------------------------------------
import sys
import os

# ------------------------------------------------------------------------------
# import local lib
path_exec=os.path.dirname(os.path.dirname( __file__ ))
if not path_exec :
    path_exec='..'
sys.path.append(path_exec)
sys.path.append(os.path.join(path_exec,"gimp2"))
sys.path.append(os.path.join(path_exec,"lib"))
dir_scriptfu = os.path.join(path_exec, "scriptfu")

from pyGapM27 import Pkg,bullet,menu_background,init_language
from pyGapM27 import info_right0 as info_right
init_language()

import gp2_func as gimp
from gp2_plugin import CPlugin
from libTools   import check_compatibilty,copy_layer,copy_layer_to_image

class CDarkSky( CPlugin):
    def __init__(self):
        shelp = ( _("Make a dark sky:")
              , bullet + _("Copy the layer ")
              , bullet + _("Equalize the layer ")
              , bullet + _("Set the multiply mode ")
              , bullet + _("Set the opacity level ") )
        params = [
               self.Param("INT32", 'iteration', _("Iteration"), 0  ),
               self.Param("FLOAT", 'opacity'  , _("Opacity")  , 0.0)]
        gui = {
            'iteration': self.Box("SF-ADJUSTMENT", '(list 3 1 10  1   1 0 0 )' ) ,
            'opacity'  : self.Box("SF-ADJUSTMENT", '(list 8 1 50  0.1 1 1 0 )' ) }

        CPlugin.__init__( self,
            Pkg+"DarkSky"       , # name
            _("Make a dark sky"), # blurb
            shelp               , # help
            info_right          , # author,copyright, ...
            _("Dark Sky")       , # menu_label
            menu_background     , # menu path
            "RGB*, GRAY*"       , # image_types
            params              , # params
            gui, dir_scriptfu   ) # gui

    def Processing(self, timg, tdrawable):
        (iteration, opacity) = self.get_parameters_value()

        gimp.plugin_enable_precision() # important sinon traitement en bpp=8
        cr=check_compatibilty(self.proc_name,timg,tdrawable)
        if cr != None : return cr

        gimp.image_undo_group_start(timg)

        img_cpy       = gimp.image_duplicate(timg)
        layer_flatten = gimp.image_flatten(img_cpy)
        gimp.item_set_name(layer_flatten,"Dark Sky layer")

        for ii in range(int(iteration)):
            layer_egal = copy_layer(img_cpy,layer_flatten,"equal layer No %d"%(ii,),-1,True)
            gimp.drawable_equalize(layer_egal,False)
            gimp.layer_set_opacity(layer_egal,opacity)
            gimp.layer_set_mode(layer_egal,"MULTIPLY")
            layer_flatten = gimp.image_flatten(img_cpy)

        # copie d'un calque dans une autre imge
        copy_layer_to_image(img_cpy,layer_flatten,timg,_("Dark Sky layer"),-1)

        gimp.image_undo_group_end(timg)
        gimp.displays_flush()

        return ( "SUCCESS","")

app=CDarkSky()
app.CreerPluginScm()
app.register()
