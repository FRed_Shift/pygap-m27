#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# ------------------------------------------------------------------------------
# This program is provided without any guarantee.
#
# The license is  LGPL-v3
# For details, see GNU General Public License, version 3 or later.
#                        "https://www.gnu.org/licenses/gpl.html"
# ------------------------------------------------------------------------------
import sys
import os

# ------------------------------------------------------------------------------
# import local lib
path_exec=os.path.dirname(os.path.dirname( __file__ ))
if not path_exec :
    path_exec='..'
sys.path.append(path_exec)
sys.path.append(os.path.join(path_exec,"gimp2"))
sys.path.append(os.path.join(path_exec,"lib"))
dir_scriptfu = os.path.join(path_exec, "scriptfu")

from pyGapM27 import Pkg,bullet,menu_color,init_language
from pyGapM27 import info_right0 as info_right
init_language()

import gp2_func as gimp
from gp2_plugin import CPlugin
from libTools   import check_compatibilty,copy_layer_to_image


class CColorAccentuation1( CPlugin):
    def __init__(self):
        shelp  = ( _("Color accentuation using 2 layers (luminance and soft light)")
               , bullet + _("Copy the layer")
               , bullet + _("Set the mode to 'soft light' or 'overlay' ")
               , bullet + _("Copy the luminance as a layer")
               , bullet + _("Set the mode to 'luminance'")
               , bullet + _("Set the opacity of the luminance") )
        params = [
              self.Param("INT32", 'SoftLight', _('Soft light/Overlay'), 0  ),
              self.Param("FLOAT", 'Opacity'  , _('Luminance opacity' ), 0.0) ]
        gui = {
            'SoftLight': self.Box("SF-TOGGLE"    , 'TRUE') ,
            'Opacity'  : self.Box("SF-ADJUSTMENT", '(list 25 1 100 5 10 0 0 )') }

        CPlugin.__init__( self,
            Pkg+"ColorAccent1"                                     , # name
            _("Color accentuation using 2 layers: luminance and soft light") , # blurb
            shelp                                                  , # help
            info_right                                             , # author,copyright, ...
            _("Accentuation using luminance and soft-light layers") , # menu_label
            menu_color                                             , # menu path
            "RGB*, GRAY*"                                          , # image_types
            params                                                 , # params
            gui, dir_scriptfu                                      ) # gui

    def Processing(self, timg, tdrawable):
        (SoftLight, Opacity) = self.get_parameters_value()

        gimp.plugin_enable_precision() # important sinon traitement en bpp=8
        cr=check_compatibilty(self.proc_name,timg,tdrawable)
        if cr != None : return cr

        gimp.image_undo_group_start(timg)

        # 1) copie l'image et l'aplatie
        img_cpy       = gimp.image_duplicate(timg)
        layer_flatten = gimp.image_flatten(img_cpy)

        # 2) copie le calque et le met en couche lumiere douce (tamisee) 
        #    ou superposee (incrustation)
        layer_SoftLight = gimp.layer_copy(layer_flatten,True)
        gimp.image_insert_layer(img_cpy,layer_SoftLight, None,-1)
        layer_mode = 'OVERLAY'
        if SoftLight : layer_mode = 'SOFTLIGHT'
        gimp.layer_set_mode(layer_SoftLight,layer_mode)
        gimp.layer_set_opacity(layer_SoftLight,Opacity)

        # 3) copie le calque et le met en couche luminance
        layer_Lum = gimp.layer_copy(layer_flatten,True)
        gimp.image_insert_layer(img_cpy,layer_Lum, None,-1)
        gimp.layer_set_mode(layer_Lum,'LUMINANCE')

        # 4) aplatie l'image
        layer_flatten = gimp.image_flatten(img_cpy)

        # 5) copie du calque dans timg
        copy_layer_to_image( img_cpy,layer_flatten, timg,"Color Accentuation 1 layer", -1 )
        
        gimp.image_undo_group_end(timg)
        gimp.displays_flush()

        return ( "SUCCESS", "")

app=CColorAccentuation1()
app.CreerPluginScm()
app.register()
