#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# ------------------------------------------------------------------------------
# This program is provided without any guarantee.
#
# The license is  LGPL-v3
# For details, see GNU General Public License, version 3 or later.
#                        "https://www.gnu.org/licenses/gpl.html"
# ------------------------------------------------------------------------------
import sys
import os

# ------------------------------------------------------------------------------
# import local lib
path_exec=os.path.dirname(os.path.dirname( __file__ ))
if not path_exec :
    path_exec='..'
sys.path.append(path_exec)
sys.path.append(os.path.join(path_exec,"gimp2"))
sys.path.append(os.path.join(path_exec,"lib"))
dir_scriptfu = os.path.join(path_exec, "scriptfu")

from pyGapM27 import Pkg,bullet,menu_sun,init_language
from pyGapM27 import info_right0 as info_right
init_language()

import gp2_func    as gimp
from gp2_plugin    import CPlugin
from libTools      import check_compatibilty,colorize_image

class CColorizeSun( CPlugin):
    def __init__(self):
        shelp = ( _("Colorize a monochrome solar image")
              , bullet + _("Set the level of each layer (RGB)")
              , bullet + _("Set the blur level of the color layer") )
        params = [
               self.Param("FLOAT", 'ValueLevel', _("Value level"), 0.0),
               self.Param("FLOAT", 'RedLevel'  , _("Red level")  , 0.0),
               self.Param("FLOAT", 'GreenLevel', _("Green level"), 0.0),
               self.Param("FLOAT", 'BlueLevel' , _("Blue level") , 0.0),
               self.Param("FLOAT", 'Blur'      , _("Blur level") , 0.0)]
        gui = {
            'ValueLevel' : self.Box( "SF-ADJUSTMENT", '(list 1.00 0.1 2.15  0.01 0.1 2 0 )' ) ,
            'RedLevel'   : self.Box( "SF-ADJUSTMENT", '(list 1.30 0.0 2.15  0.01 0.1 2 0 )' ) ,
            'GreenLevel' : self.Box( "SF-ADJUSTMENT", '(list 0.85 0.0 2.15  0.01 0.1 2 0 )' ) ,
            'BlueLevel'  : self.Box( "SF-ADJUSTMENT", '(list 0.20 0.0 2.15  0.01 0.1 2 0 )' ) ,
            'Blur'       : self.Box( "SF-ADJUSTMENT", '(list 2.50 0.0 10.0  0.01 0.1 2 0 )' ) }

        CPlugin.__init__( self,
            Pkg+"ColorizeSun"            , # name
            _("Colorize Sun")            , # blurb
            shelp                        , # help
            info_right                   , # author,copyright, ...
            _("Colorize Sun")            , # menu_label
            menu_sun +"/"+ _("Colorize") , # menu path
            "RGB*, GRAY*"                , # image_types
            params                       , # params
            gui, dir_scriptfu            ) # gui

    def Processing(self, timg, tdrawable):
        ( ValueLevel, RedLevel, GreenLevel, BlueLevel, Blur ) = self.get_parameters_value()

        gimp.plugin_enable_precision() # important sinon traitement en bpp=8
        cr=check_compatibilty(self.proc_name,timg,tdrawable)
        if cr != None : return cr        

        gimp.image_undo_group_start(timg)
        
        colorize_image(timg,tdrawable, ValueLevel, RedLevel, GreenLevel, BlueLevel, Blur)
            
        gimp.image_undo_group_end(timg)
        gimp.displays_flush()

        return ( "SUCCESS","")


app=CColorizeSun()
app.CreerPluginScm()
app.register()
