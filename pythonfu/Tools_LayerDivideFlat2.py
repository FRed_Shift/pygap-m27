#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# ------------------------------------------------------------------------------
# This program is provided without any guarantee.
#
# The license is  LGPL-v3
# For details, see GNU General Public License, version 3 or later.
#                        "https://www.gnu.org/licenses/gpl.html"
# ------------------------------------------------------------------------------
#
#   Python-Set-Dark-Sky - Set the image dark sky at chosen level.
#   Copyright (C) 2014  Bill Smith
#

import sys
import os

# ------------------------------------------------------------------------------
# import local lib
path_exec=os.path.dirname(os.path.dirname( __file__ ))
if not path_exec :
    path_exec='..'
sys.path.append(path_exec)
sys.path.append(os.path.join(path_exec,"gimp2"))
sys.path.append(os.path.join(path_exec,"lib"))
dir_scriptfu = os.path.join(path_exec, "scriptfu")

from pyGapM27 import Pkg,menu_layertools,init_language
from pyGapM27 import info_right1 as info_right
init_language()

import gp2_func  as gimp
from gp2_plugin    import CPlugin
from libTools      import check_compatibilty,copy_layer_to_image,create_layer,fill_layer

class CLayerDivideFlat2( CPlugin):
    def __init__(self):
        shelp  = _("Divide a stack of images in successive layers by a flat image")
        params = [{ "type":"STRING"   , "name":"flat" , "info":_('Select flat image file'), "value":""}]
        gui    = {"flat" : { "BOX": "SF-FILENAME", "VALBOX": '"flat.fit"'}}

        CPlugin.__init__( self,
            Pkg+"LayerDivideFlat2"       , # name
            _("Divide a stack of images by a flat image") , # blurb
            shelp                        , # help
            info_right                   , # author,copyright, ...
            _("Divide layers by flat"  ) , # menu_label
            menu_layertools              , # menu path
            "RGB*, GRAY*"                , # image_types
            params                       , # params
            gui, dir_scriptfu          )   # gui

    def Processing(self, timg, tdrawable):
        ''' This GIMP plugin Divide an image layer stack by a flat image .

        Parameters:
            timg     : The currently selected image.
            tdrawable: The layer of the currently selected image.
            flat     : File name of flat image
        '''
        gimp.plugin_enable_precision() # important sinon traitement en bpp=8
        cr=check_compatibilty(self.proc_name,timg,tdrawable)
        if cr != None : return cr        

        ( flat, ) = self.get_parameters_value()

        dimg = gimp.file_load(flat)
        if dimg is None:
            msgerr= _("The selected flat image file could not be opened.")
            return ( "CALLING_ERROR", msgerr  )

        gimp.context_push()
        gimp.image_undo_group_start(timg)
        layers=gimp.get_layers(dimg)
        if len(layers)>1 :
            gimp.image_flatten(dimg)
            layers=gimp.get_layers(dimg)

        K_norm=self.get_median_color( timg, layers[0] )

        layer_flat = copy_layer_to_image(dimg, layers[0],timg,_("Flat layer"),-1 )
        layer = gimp.image_merge_down(timg, layer_flat, "EXPAND_AS_NECESSARY" )
               
        layer_norm  = create_layer(timg, layer, _("normalisation"), "MULTIPLY",-1 )
        fill_layer(layer_norm, K_norm )
        layer = gimp.image_merge_down(timg, layer_norm, "EXPAND_AS_NECESSARY" )
       
        gimp.image_undo_group_end(timg)
        gimp.context_pop()
        gimp.displays_flush()

        return ( "SUCCESS", "")
        
    def get_median_color(self, timg, tdrawable ):
        _unused=(timg) # unused parameter , suppress pylint warning
        layer = tdrawable
        if gimp.drawable_is_rgb(layer) :
            red   = gimp.drawable_histogram(layer, "RED"  ,0.0,1.0)[1][2]
            green = gimp.drawable_histogram(layer, "GREEN",0.0,1.0)[1][2]
            blue  = gimp.drawable_histogram(layer, "BLUE" ,0.0,1.0)[1][2]
            return [ red, green ,blue ]
        else:
            median = gimp.drawable_histogram(layer, "VALUE"  ,0.0,1.0)[1][2]
            return [ median ]

app=CLayerDivideFlat2()
app.CreerPluginScm()
app.register()
