#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# ------------------------------------------------------------------------------
# This program is provided without any guarantee.
#
# The license is  LGPL-v3
# For details, see GNU General Public License, version 3 or later.
#                        "https://www.gnu.org/licenses/gpl.html"
# ------------------------------------------------------------------------------
import sys
import os
import math
from   datetime import datetime

# ------------------------------------------------------------------------------
# import local lib
path_exec=os.path.dirname(os.path.dirname( __file__ ))
path_exec=os.path.dirname(os.path.dirname( __file__ ))
if not path_exec :
    path_exec='..'
sys.path.append(path_exec)
sys.path.append(os.path.join(path_exec,"gimp2"))
sys.path.append(os.path.join(path_exec,"lib"))
dir_scriptfu = os.path.join(path_exec, "scriptfu")

from pyGapM27 import Pkg,menu_cartridge,init_language
from pyGapM27 import info_right0 as info_right
init_language()

import gp2_func as gimp
from gp2_plugin    import CPlugin
from libTools      import check_compatibilty
from plug_in       import gauss

# ------------------------------------------------------------------------------
NB_LINES=2
LINE1 =0
LINE2 =1
NB_JUSTIFY=3
LEFT  =0
MIDDLE=1
RIGHT =2

X=0
Y=1
W=2
H=3
# ------------------------------------------------------------------------------
class CDrawingCartridge( CPlugin):
    def __init__(self, cfg ):
        self.cfg = cfg

        shelp = _("Draw a cartridge containing some information about the solar/planet/DeepSky image")
        params = self.setParamCfg(cfg[0])
        nbcfg = str(len(cfg)-1)
        gui = {
            'id'               :  self.Box("SF-ADJUSTMENT", '(list %d  0 ' + nbcfg + ' 0 0 0 1 )' ),
            'inner_size'       :  self.Box("SF-ADJUSTMENT", '(list %d  0 100 1 1 0 1 )'),
            'inner_color'      :  self.Box("SF-COLOR"     , '(list %d %d %d )'         ),
            'outer_size'       :  self.Box("SF-ADJUSTMENT", '(list %d  0 100 1 1 0 1 )'),
            'outer_color'      :  self.Box("SF-COLOR"     , '(list %d %d %d )'         ),
            'gauss_radius'     :  self.Box("SF-ADJUSTMENT", '(list %f  0  20 1 1 1 1 )'),
            'info_height'      :  self.Box("SF-ADJUSTMENT", '(list %d  0 200 1 1 0 1 )'),
            'font'             :  self.Box("SF-FONT"      , '"%s"'                     ),
            'font_color'       :  self.Box("SF-COLOR"     , '(list %d %d %d )'         ),
            'text_left1'       :  self.Box("SF-STRING"    , '"%s"'                     ),
            'font_size_left1'  :  self.Box("SF-ADJUSTMENT", '(list %d 0 100 1 1 0 1 )' ),
            'text_left2'       :  self.Box("SF-STRING"    , '"%s"'                     ),
            'font_size_left2'  :  self.Box("SF-ADJUSTMENT", '(list %d 0 100 1 1 0 1 )' ),
            'text_middle1'     :  self.Box("SF-STRING"    , '"%s"'                     ),
            'font_size_middle1':  self.Box("SF-ADJUSTMENT", '(list %d 0 100 1 1 0 1 )' ),
            'text_middle2'     :  self.Box("SF-STRING"    , '"%s"'                     ),
            'font_size_middle2':  self.Box("SF-ADJUSTMENT", '(list %d 0 100 1 1 0 1 )' ),
            'text_right1'      :  self.Box("SF-STRING"    , '"%s"'                     ),
            'font_size_right1' :  self.Box("SF-ADJUSTMENT", '(list %d 0 100 1 1 0 1 )' ),
            'text_right2'      :  self.Box("SF-STRING"    , '"%s"'                     ),
            'font_size_right2' :  self.Box("SF-ADJUSTMENT", '(list %d 0 100 1 1 0 1 )' ),
            'resize'           :  self.Box("SF-TOGGLE"    , '%s') }

        CPlugin.__init__( self,
            Pkg+"Cartridge"       , # name
            _("Drawing Cartridge"), # blurb
            shelp                 , # help
            info_right            , # author,copyright, ...
            _("Drawing Cartridge"), # menu_label
            menu_cartridge        , # menu path
            "RGB*, GRAY*"         , # image_types
            params                , # params
            gui                   ) # gui

    def setParamCfg(self,pp):
        return [
            self.Param("INT32"  , 'id'               , _("Cartridge ID")                   , pp['id'          ]),
            self.Param("INT32"  , 'inner_size'       , _("Inner border size")              , pp['inner_size'  ]),
            self.Param("COLOR"  , 'inner_color'      , _("Inner border color")             , pp['inner_color' ]),
            self.Param("INT32"  , 'outer_size'       , _("Outer border size")              , pp['outer_size'  ]),
            self.Param("COLOR"  , 'outer_color'      , _("Outer border color")             , pp['outer_color' ]),
            self.Param("FLOAT"  , 'gauss_radius'     , _("Gauss Radius")                   , pp['gauss_radius']),
            self.Param("INT32"  , 'info_height'      , _("Bottom information height")      , pp['info_height' ]),
            self.Param("STRING" , 'font'             , _("Font")                           , pp['font'        ]),
            self.Param("COLOR"  , 'font_color'       , _("Font color")                     , pp['font_color'  ]),
            self.Param("STRING" , 'text_left1'       , _("Text (left, first line)")        , pp['text'][0][0]  ),
            self.Param("INT32"  , 'font_size_left1'  , _("Font size (left, first line)")   , pp['size'][0][0]  ),
            self.Param("STRING" , 'text_left2'       , _("Text (left, second line)")       , pp['text'][1][0]  ),
            self.Param("INT32"  , 'font_size_left2'  , _("Font size (left, second line)")  , pp['size'][1][0]  ),
            self.Param("STRING" , 'text_middle1'     , _("Text (middle, first line)")      , pp['text'][0][1]  ),
            self.Param("INT32"  , 'font_size_middle1', _("Font size (middle, first line)") , pp['size'][0][1]  ),
            self.Param("STRING" , 'text_middle2'     , _("Text (middle, second line)")     , pp['text'][1][1]  ),
            self.Param("INT32"  , 'font_size_middle2', _("Font size (middle, second line)"), pp['size'][1][1]  ),
            self.Param("STRING" , 'text_right1'      , _("Text (right, first line)")       , pp['text'][0][2]  ),
            self.Param("INT32"  , 'font_size_right1' , _("Font size (right, first line)")  , pp['size'][0][2]  ),
            self.Param("STRING" , 'text_right2'      , _("Text (right, second line)")      , pp['text'][1][2]  ),
            self.Param("INT32"  , 'font_size_right2' , _("Font size (right, second line)") , pp['size'][1][2]  ),
            self.Param("INT32"  , 'resize'           , _("automatic adaptation  the font size according to the size of the information area"), pp['resize'] ),
               ]

    def Processing(self, timg, tdrawable):
        ( uid, inner_size, inner_color, outer_size, outer_color,
        gauss_radius, info_height,  font, font_color,
        text_left1  , font_size_left1  , text_left2  , font_size_left2,
        text_middle1, font_size_middle1, text_middle2, font_size_middle2,
        text_right1 , font_size_right1 , text_right2 , font_size_right2,
        resize) = self.get_parameters_value()

        gimp.plugin_enable_precision() # important sinon traitement en bpp=8
        cr=check_compatibilty(self.proc_name,timg,tdrawable)
        if cr != None : return cr

        self.Param={}
        self.SetParam( uid,
                inner_size  , inner_color       , outer_size  , outer_color       ,
                gauss_radius, info_height       , font        , font_color        ,
                text_left1  , font_size_left1   , text_left2  , font_size_left2   ,
                text_middle1, font_size_middle1 , text_middle2, font_size_middle2 ,
                text_right1 , font_size_right1  , text_right2 , font_size_right2  ,
                resize)
        self.saveScm(uid,self.params,True)
        self.build_cartridge(timg, tdrawable)

        return ( "SUCCESS","")

    def CreerPluginScm(self,guiname="",force=False):
        _unused=(guiname) # unused parameter , suppress pylint warning       
        for pp in self.cfg :
            self.saveScm(pp['id'], self.setParamCfg(pp),force )
        return ""

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # Sauve les donnees de chaque cartouche
    def saveScm(self, uid, pp, force=True ):
        filename = Pkg+"Cartridge%d.scm"%(uid,)
        if dir_scriptfu != "" :
            filename = os.path.join( dir_scriptfu, filename )

        if os.path.isfile(filename) and force == False :
            return

        memo_plugin_info = self.plugin_info
        memo_params = self.params

        guiname = self.cfg[uid]['SF']['CmdName']
        self.plugin_info[1] = self.cfg[uid]['SF']['Info']
        self.plugin_info[2] = self.cfg[uid]['SF']['Help']
        self.plugin_info[3] = self.cfg[uid]['SF']['MenuName']
        self.params = pp
        scm_txt=CPlugin.pluginScm(self,guiname)
        try:
            with open(filename ,"w") as fd :
                fd.write( scm_txt )
        except Exception as e :
            print(e)
            gimp.message(e)

        self.params = memo_params
        self.plugin_info = memo_plugin_info
        return

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # Construction des bordures de l'image
    def mkBorderLayer( self, timg,
                border_size, border_color,
                info_height, layer_name, layers_number ) :

        current_width  = gimp.image_width(timg)
        current_height = gimp.image_height(timg)
        gimp.image_resize( timg,
                            current_width+(2*border_size),
                            current_height+(2*border_size)+info_height,
                            border_size,border_size )
        new_width  = gimp.image_width(timg)
        new_height = gimp.image_height(timg)
        border_layer = gimp.layer_new(timg, layer_name, new_width, new_height,
                                      "RGBA_IMAGE", 100.0, "NORMAL")
        gimp.image_insert_layer( timg, border_layer, None, layers_number )
        gimp.image_set_active_layer( timg, border_layer)
        gimp.context_set_foreground( border_color)
        gimp.drawable_fill( gimp.image_get_active_drawable(timg), "FOREGROUND_FILL")
        return border_layer

    def text_boundingbox( self, line, justify, text, size, font, bb_info ):
        if size <= 0 :
            return (0,0,0,0,0,0)

        (ww,hh,h0,h1) = gimp.text_get_extents_fontname(text,size, "PIXELS", font)[1]
        if justify == LEFT :
            xx = max( 0, bb_info[X] )
        if justify ==  MIDDLE :
            xx = max( 0, (bb_info[X]+bb_info[W]/2) - ww/2.0)
        if justify ==  RIGHT :
            xx = max( 0, (bb_info[X]+bb_info[W])-ww)

        if line == LINE1 :
            yy = max( 0, bb_info[Y]  )
        else:
            yy = max( 0, (bb_info[Y]+bb_info[H]) - hh )
        return (xx,yy,ww,hh,h0,h1)

    def list_bb_text(self, text,tfont,tsize, bb_info ):
        bb_text=[ ]
        for line in range(NB_LINES) :
            for justify in range(NB_JUSTIFY) :
                ss=tsize[line][justify]
                tt=text[line][justify]
                bb_text.append(self.text_boundingbox(  line, justify,tt, ss,tfont, (0, 0, bb_info[W],bb_info[H]) ))
        return bb_text

    def resize_w(self, text,tfont,tsize, bb_info, xmarge ) :
        bb_text = self.list_bb_text(text,tfont,tsize, bb_info )

        ii_x=[ (0,1,4) , (1,2,5), (3,1,4), (4,2,5) ]
        ecartx=[ ]
        for ii in ii_x :
            if  bb_text[ii[0]][X] == bb_text[ii[1]][X]:
                ecartx.append(0)
            else:
                ecartx.append( bb_text[ii[0]][X]+bb_text[ii[0]][W]+ xmarge - bb_text[ii[1]][X] )
            if  bb_text[ii[0]][X] == bb_text[ii[2]][X]:
                ecartx.append(0)
            else:
                ecartx.append( bb_text[ii[0]][X]+bb_text[ii[0]][W]+ xmarge - bb_text[ii[2]][X] )
        ecartmax=max(ecartx)

        print( ecartx, ecartmax )

        if ecartmax >0 :
            pos=0
            for ecart in ecartx:
                if ecart == ecartmax :
                    break
                pos=pos+1
            pp=ii_x[int(pos/2)]
            idx_cmp=1 + (pos&1)
            line0 = int( math.floor(pp[0]/NB_JUSTIFY) )
            line1 = int( math.floor(pp[idx_cmp]/NB_JUSTIFY) )
            jj0   = pp[0] -line0*NB_JUSTIFY
            jj1   = pp[idx_cmp] -line1*NB_JUSTIFY

            # reduire la font la + grande
            if tsize[line0][jj0] > tsize[line1][jj1] :
                ii = line0*NB_JUSTIFY+jj0
                if bb_text[ii][W] == 0 : return False
                ratio = (bb_text[ii][W]-ecartmax)/bb_text[ii][W]
                new_size = round( tsize[line0][jj0] * ratio  )
                if new_size > tsize[line1][jj1] :
                    tsize[line0][jj0] = new_size
                else:
                    tsize[line0][jj0] = tsize[line1][jj1]-2 ;
            else:
                ii = line1*NB_JUSTIFY+jj1
                if bb_text[ii][W] == 0 : return False
                ratio = (bb_text[ii][W]-ecartmax)/bb_text[ii][W]
                new_size = round( tsize[line1][jj1] * ratio  )
                if new_size > tsize[line0][jj0] :
                    tsize[line1][jj1] = new_size
                else:
                    tsize[line1][jj1] = tsize[line0][jj0]-2 ;
            return True
        else:
            return False

    def resize_h(self, text,tfont,tsize, bb_info, ymarge ) :
        bb_text = self.list_bb_text(text,tfont,tsize, bb_info )
        ii_y=[ (0,3) , (1,4), (2,5) ]
        ecarty=[ ]
        for ii in ii_y :
            ecarty.append( bb_text[ii[0]][Y]+bb_text[ii[0]][H]+ ymarge - bb_text[ii[1]][Y] )

        for pos in range(len(ecarty)) :
            if ecarty[pos] <= 0 :
                continue
            pp=ii_y[pos]
            line0 = int( math.floor(pp[0]/NB_JUSTIFY) )
            line1 = int( math.floor(pp[1]/NB_JUSTIFY) )
            jj0   = pp[0] -line0*NB_JUSTIFY
            jj1   = pp[1] -line1*NB_JUSTIFY
            if tsize[line0][jj0] > tsize[line1][jj1] :
                ii = line0*NB_JUSTIFY+jj0
            else:
                ii = line1*NB_JUSTIFY+jj1
            ratio = (bb_text[ii][H]-ecarty[pos]/2)/bb_text[ii][H]
            tsize[line0][jj0] = round( tsize[line0][jj0] * ratio  )
            tsize[line1][jj1] = round( tsize[line1][jj1] * ratio  )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # Verifie que les textes ne se superposent pas
    def CheckText(self, text,tfont,tsize, bb_info, xmarge , ymarge ) :
        print( "Verifie que les textes ne se superposent pas dans la zone info: ", bb_info[W], "x", bb_info[H] )
        self.resize_h( text,tfont,tsize, bb_info, ymarge )
        for ii in range(4):
            if self.resize_w( text,tfont,tsize, bb_info, xmarge ) == False :
                break

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # Calcule la taille min que les textes ne se superposent pas en hauteur
    def GetInfoSizeMin(self, text,tfont,tsize, bb_info, ymarge ) :
        bb_text = self.list_bb_text(text,tfont,tsize, bb_info )
        hsize=0
        for ii in [ (0,3) , (1,4), (2,5) ] :
            hsize=max(hsize, ymarge+bb_text[ii[0]][H]+ ymarge + bb_text[ii[1]][H]+ymarge)
        print("hsize min = ",hsize )
        return hsize ;

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # Construction du cartouche
    def build_cartridge(self, timg, tdrawable ) :
        pp = self.Param
        # --------------------------------------------------------------------------
        # Original Image width and height
        image_width  = gimp.image_width(timg)
        image_height = gimp.image_height(timg)
        print( "Original Image width and height: ", image_width , "x", image_height)
        # bounding box info (x,y,w,h)
        bb_info = ( pp['outer_size'] , pp['outer_size']+image_height+3*pp['inner_size'],
                    image_width,pp['info_height'])
        ss = pp['size']
        tt = pp['text']

        if pp['resize'] :
            infoSizeMin = self.GetInfoSizeMin( tt,pp['font'],ss,bb_info, pp['inner_size'] )
            if infoSizeMin > pp['info_height'] :
                bb_info = ( pp['outer_size'] , pp['outer_size']+image_height+3*pp['inner_size'],
                            image_width,pp['info_height'])
            self.CheckText( tt,pp['font'],ss,bb_info, pp['inner_size'] , pp['inner_size'] )

        gimp.image_undo_group_start(timg)

        if not gimp.drawable_is_rgb(tdrawable):
            gimp.image_convert_rgb(timg)

        # Add alpha channel to all layers, so we can move our information layer to the bottom
        layers=gimp.get_layers(timg)
        for layer in layers :
            print( "Add alpha channel to: " ,layer)
            gimp.layer_add_alpha(layer)

        layers = gimp.get_layers(timg)
        layers_number = len(layers)

        # Backup foreground color
        color_tmp = gimp.context_get_foreground()[1]

        # --------------------------------------------------------------------------
        # Add border 1 as layer
        border1_layer = self.mkBorderLayer( timg, pp['inner_size'], pp['inner_color'],
                                       0, "border in", layers_number )

        # --------------------------------------------------------------------------
        # Add border 2 as layer
        border2_layer = self.mkBorderLayer( timg, pp['outer_size'], pp['outer_color'],
                                       pp['info_height']-pp['outer_size']+2*pp['inner_size'],
                                       "border out + Information",
                                       layers_number+1 )

        # --------------------------------------------------------------------------
        # Text
        # --------------------------------------------------------------------------
        gimp.context_set_foreground( pp['font_color'] )

        text_layers = []
        font=pp['font']
        bb_text=[ [ None,None,None ] , [ None,None,None ] ]
        for line in range(NB_LINES) :
            for justify in range(NB_JUSTIFY) :
                size=ss[line][justify]
                text=tt[line][justify]
                bb_text[line][justify] = self.text_boundingbox( line, justify,text, size, font, bb_info )

            # aligne les lignes
            (max_tsize,max_jj) = max((v,max_jj) for max_jj,v in enumerate(ss[line]))
            for justify in range(NB_JUSTIFY) :
                size=ss[line][justify]
                if size <= 0 :
                    continue
                text=tt[line][justify]
                (xx, yy, ww, hh,h0,h1) = bb_text[line][justify]
                _unused_=(ww, hh) # unused variable , suppress pylint warning
                if size < max_tsize :
                    if line == LINE1 :
                        yy = yy + (bb_text[line][max_jj][4]-h0)
                    else:
                        yy = yy + (bb_text[line][max_jj][5]-h1)

                tlayer=gimp.text_fontname( timg, -1 , xx, yy, text, -1, True, size, "PIXELS", font )
                text_layers.append(tlayer)
        # --------------------------------------------------------------------------
        # Move it to the bottom
        gimp.image_lower_layer_to_bottom(timg, border1_layer)
        gimp.image_lower_layer_to_bottom(timg, border2_layer)

        # --------------------------------------------------------------------------
        # Merge all layers into one (only merge text layers if they were created;
        # empty text layers are not created. merge the resulting layer (might be border1_layer) down.
        merge_layer = gimp.image_merge_down( timg, border1_layer, "EXPAND_AS_NECESSARY")

        # --------------------------------------------------------------------------
        # smooth inner border -> outer border
        if pp['gauss_radius'] > 0 :
            gauss( timg,  merge_layer, pp['gauss_radius'], pp['gauss_radius'], 0)

        for layer in text_layers :
            merge_layer = gimp.image_merge_down( timg,layer, "EXPAND_AS_NECESSARY")

        gimp.context_set_foreground( color_tmp )

        gimp.image_undo_group_end(timg)
        gimp.displays_flush()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # Met a  jour les donnees interne du cartouche
    def SetParam(self, uid,
            inner_size  , inner_color       , outer_size  , outer_color       ,
            gauss_radius, info_height       , font        , font_color        ,
            text_left1  , font_size_left1   , text_left2  , font_size_left2   ,
            text_middle1, font_size_middle1 , text_middle2, font_size_middle2 ,
            text_right1 , font_size_right1  , text_right2 , font_size_right2  ,
            resize ):

        if sys.version_info[0] >= 3 :
            code='utf-8'
            text_left1  = text_left1.decode(code,errors='ignore')
            text_left2  = text_left2.decode(code,errors='ignore')
            text_middle1= text_middle1.decode(code,errors='ignore')
            text_middle2= text_middle2.decode(code,errors='ignore')
            text_right1 = text_right1.decode(code,errors='ignore')
            text_right2 = text_right2.decode(code,errors='ignore')
            font        = font.decode(code,errors='ignore')

        Param={}
        Param["id"  ]= uid
        Param["inner_size"  ]= inner_size
        Param["inner_color" ]= inner_color
        Param["outer_size"  ]= outer_size
        Param["outer_color" ]= outer_color
        Param["gauss_radius"]= gauss_radius
        Param["info_height" ]= info_height
        Param["font"        ]= font
        Param["font_color"  ]= font_color
        Param["size"        ]= [ [ font_size_left1, font_size_middle1, font_size_right1],
                                 [ font_size_left2, font_size_middle2, font_size_right2]]
        Param["text"        ]= [ [ text_left1, text_middle1, text_right1],
                                 [ text_left2, text_middle2, text_right2]]
        Param["resize"      ]= resize

        self.Param = Param

# ------------------------------------------------------------------------------
def InitParam():
    maintenant    = datetime.now()
    strdate       = maintenant.strftime("%H:%M %d-%m-%Y")

    Noir =(0.0,0.0,0.0,0.0)
    Blanc=(1.0,1.0,1.0,0.0)

    parameters= {}

    parameters["id"  ]        = 0
    parameters["inner_size"  ]= 2
    parameters["inner_color" ]= Blanc
    parameters["outer_size"  ]= 12
    parameters["outer_color" ]= Noir
    parameters["gauss_radius"]= 0
    parameters["info_height" ]= 50
    parameters["font"        ]= "Courier"
    parameters["font_color"  ]= Blanc
    parameters["size"        ]= [ [  0, 13, 13],
                                  [ 32, 13, 13]]
    parameters["text"        ]= [ [ "", "", "M27trognondepomme"],
                                  [ "", "", strdate ]]
    parameters["resize"      ]= "FALSE"
    return parameters

cfg=[]
prefixe = "- "
parameters = InitParam()
parameters["id"  ]= 0
parameters["SF"] ={
        'CmdName' : "SolarCartridge",
        'MenuName': prefixe + _("Sun Spot"),
        'Info'    : _("Cartridge containing information about the solar image"),
        'Help'    : _("Draw a cartridge containing some information about the solar image") }
parameters["text"][0][1] = "Helioscope T150/1200mm + HEQ5 + ASI183MM"
parameters["text"][1][0] = _("Sun spot ARxxxx")
parameters["text"][1][1] = "Filtres ND3 + Continuum / traitement AstroSuface + Gimp2.10"
cfg.append( parameters )

parameters = InitParam()
parameters["id"  ]= 1
parameters["SF"] ={
        'CmdName' : "SolarCartridgeHa",
        'MenuName': prefixe + _("Sun Spot") +  " Ha",
        'Info'    : _("Cartridge containing information about the Halpha solar image"),
        'Help'    : _("Draw a cartridge containing some information about the Halpha solar image") }
parameters["text"][0][1] = "Lunette SW 80ED600 + HEQ5 + QHY-III 174M "
parameters["text"][1][0] = _("Sun spot ARxxxx")
parameters["text"][1][1] = "Filtre DayStar Quark 0.6nm (protubérance) / traitement AstroSuface + Gimp2.10"
cfg.append( parameters )

parameters = InitParam()
parameters["id"  ]= 2
parameters["SF"] ={
        'CmdName' : "DeepSkyCartouche",
        'MenuName': prefixe + _("Deep Sky"),
        'Info'    : _("Cartridge containing information about the planet image"),
        'Help'    : _("Draw a cartridge containing some information about the planet image") }
parameters["text"][0][1] = "Telescope Carbon D 150mm/600mm F/D=4 + correcteur + HEQ5 + ASI183MM / Autoguidage GPCAM224C + Lunette SW50ED/242mm + PHD2guider"
parameters["text"][1][0] =  _("Object")
parameters["text"][1][1] = "?x180s couche L, ?x180s par couche RVB, ?x180s couche Ha / traitement SIRIL + Gimp2.10"
cfg.append( parameters )

parameters = InitParam()
parameters["id"  ]= 3
parameters["SF"] ={
        'CmdName' : "PlanetCartridge",
        'MenuName': prefixe + _("Planet"),
        'Info'    : _("Cartridge containing Deep Sky Image information"),
        'Help'    : _("Draw a cartridge containing some information about the deep sky image") }
parameters["text"][0][1] = "Celestron C8 + barlow x? + GPCAM224C"
parameters["text"][1][0] = _("Planet")
parameters["text"][1][1] = "traitements: AS3! + R6 + Gimp2.10"
cfg.append( parameters )

parameters = InitParam()
parameters["id"  ]= 4
parameters["SF"] ={
        'CmdName' : "UserCartridge",
        'MenuName': prefixe + _("User"),
        'Info'    : _("Cartridge containing Deep Sky Image (SLA) information"),
        'Help'    : _("Draw a cartridge containing some information about the deep sky image") }
parameters["text"][0][1] = "Lunette EQUINOX SW80ED500 + correcteur 0.85 + AVALON + ASI071 / Autoguidage ASI224C + Lunette SW80/400 + PHD2guider"
parameters["text"][0][2] = "Gweltaz"
parameters["text"][1][0] = _("Object")
parameters["text"][1][1] = "?x180s (G=181) soit ?H??MIN/ traitement SIRIL + Gimp2.10"
cfg.append( parameters )

app = CDrawingCartridge(cfg)
app.CreerPluginScm()
app.register()

