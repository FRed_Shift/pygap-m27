#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# ------------------------------------------------------------------------------
# This program is provided without any guarantee.
#
# The license is  LGPL-v3
# For details, see GNU General Public License, version 3 or later.
#                        "https://www.gnu.org/licenses/gpl.html"
# ------------------------------------------------------------------------------
import sys
import os

# ------------------------------------------------------------------------------
# import local lib
path_exec=os.path.dirname(os.path.dirname( __file__ ))
path_exec=os.path.dirname(os.path.dirname( __file__ ))
if not path_exec :
    path_exec='..'
sys.path.append(path_exec)
sys.path.append(os.path.join(path_exec,"gimp2"))
sys.path.append(os.path.join(path_exec,"lib"))
dir_scriptfu = os.path.join(path_exec, "scriptfu")

from pyGapM27 import Pkg,bullet,menu_composit,init_language
from pyGapM27 import info_right0 as info_right
init_language()

import gp2_func as gimp
from gp2_plugin    import CPlugin
from libTools      import check_compatibilty
from libLayerTools import GetLuminanceFromRGBImage

class CLsynth( CPlugin):
    def __init__(self):
        shelp = ( _("Make a synthetic luminance")
              , bullet + _("Transform RGB image to LAB image")
              , bullet + _("Copy layer L in RGB image")
              , bullet + _("Delete LAB image")  )
        params = []
        gui = { }

        CPlugin.__init__( self,
            Pkg+"Lsynth"            , # name
            _("Synthetic Luminance"), # blurb
            shelp                   , # help
            info_right              , # author,copyright, ...
            _("Synthetic Luminance"), # menu_label
            menu_composit           , # menu path
            "RGB*"                  , # image_types
            params                  , # params
            gui, dir_scriptfu       ) # gui

    def Processing(self, timg, tdrawable):

        gimp.plugin_enable_precision() # important sinon traitement en bpp=8
        cr=check_compatibilty(self.proc_name,timg,tdrawable)
        if cr != None : return cr

        gimp.image_undo_group_start(timg)
        
        L_layer = GetLuminanceFromRGBImage(timg, tdrawable )
        gimp.item_set_name(L_layer,"luminance layer")

        gimp.image_undo_group_end(timg)
        gimp.displays_flush()

        return ( "SUCCESS","")

app=CLsynth()
app.CreerPluginScm()
app.register()
