#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# ------------------------------------------------------------------------------
# This program is provided without any guarantee.
#
# The license is  LGPL-v3
# For details, see GNU General Public License, version 3 or later.
#                        "https://www.gnu.org/licenses/gpl.html"
# ------------------------------------------------------------------------------
#
#   Python-Enhance-Star-Colours - Boosts the colours of the star images.
#   Copyright (C) 2016  Bill Smith
#
import sys
import os

# ------------------------------------------------------------------------------
# import local lib
path_exec=os.path.dirname(os.path.dirname( __file__ ))
if not path_exec :
    path_exec='..'
sys.path.append(path_exec)
sys.path.append(os.path.join(path_exec,"gimp2"))
sys.path.append(os.path.join(path_exec,"lib"))
dir_scriptfu = os.path.join(path_exec, "scriptfu")

from pyGapM27 import Pkg,menu_stars,init_language
from pyGapM27 import info_right1 as info_right
init_language()

import gp2_func  as gimp
from gp2_plugin import CPlugin
from libTools   import check_compatibilty
from libTools   import create_layer, copy_layer, fill_layer
from plug_in    import gauss_iir

class CEnhanceStarsColours( CPlugin):    
    def __init__(self):
        shelp  = _("Enhance star colours") 
        params = [self.Param("FLOAT", "base", _('Base level'), 0.0)]
        gui    = {"base" : self.Box("SF-ADJUSTMENT",'(list 5.0 0.0 25.00  0.1 1.0 1 0 )')}

        CPlugin.__init__( self,
            Pkg+"EnhanceStarsColours"  , # name
            _("Enhance star colours")  , # blurb
            shelp                      , # help
            info_right                 , # author,copyright, ...
            _("Enhance star colours")  , # menu_label
            menu_stars                 , # menu path
            "RGB*"                     , # image_types
            params                     , # params
            gui, dir_scriptfu          ) # gui

    def Processing(self, timg, tdrawable):
        ''' This GIMP plugin boosts the colours of the star images.

        Parameters:
        timg     : The currently selected image.
        tdrawable: The layer of the currently selected image.
        base   : Base level - background noise removal
        '''
        (img, layer) = (timg, tdrawable)
        (base,) = self.get_parameters_value()

        gimp.plugin_enable_precision() # important sinon traitement en bpp=8
        cr=check_compatibilty(self.proc_name,timg,tdrawable)
        if cr != None : return cr

        base         /= 100.0
        bgcolor       = (base, base, base)
        gwdth         = 5.0
        intense       = 95.0

        mode_merge    = "EXPAND_AS_NECESSARY"
                  
        gimp.context_push()
        gimp.image_undo_group_start(img)
        
        new_layer = copy_layer(img, layer,"new_layer",-1)

        mask = create_layer(img, new_layer,"Mask...", "SUBTRACT",-1)
        fill_layer(mask, bgcolor)
        merge_layer = gimp.image_merge_down(img, mask, mode_merge)
        
        gauss_iir(img , merge_layer, gwdth, True, True)
        
        gimp.hue_saturation(merge_layer, "ALL_HUES", 0.0, 0.0, intense)
        gimp.layer_set_mode(merge_layer, "HSL_COLOR")
        
        gimp.image_merge_down(img, merge_layer, mode_merge)
        
        gimp.image_undo_group_end(img)
        gimp.context_pop()

        return ( "SUCCESS", "")

app=CEnhanceStarsColours()
app.CreerPluginScm()
app.register()
