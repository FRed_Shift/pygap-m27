#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# ------------------------------------------------------------------------------
# This program is provided without any guarantee.
#
# The license is  LGPL-v3
# For details, see GNU General Public License, version 3 or later.
#                        "https://www.gnu.org/licenses/gpl.html"
# ------------------------------------------------------------------------------

import sys
import os
from   datetime import datetime

# ------------------------------------------------------------------------------
# import local lib
path_exec=os.path.dirname(os.path.dirname( __file__ ))
path_exec=os.path.dirname(os.path.dirname( __file__ ))
if not path_exec :
    path_exec='..'
sys.path.append(path_exec)
sys.path.append(os.path.join(path_exec,"gimp2"))
sys.path.append(os.path.join(path_exec,"lib"))
dir_scriptfu = os.path.join(path_exec, "scriptfu")

from pyGapM27 import Pkg,menu_cartridge,init_language
from pyGapM27 import info_right0 as info_right
init_language()

import gp2_func as gimp
from gp2_plugin    import CPlugin
from libTools      import check_compatibilty
from plug_in       import gauss

# ------------------------------------------------------------------------------
class CSimplifiedDrawingCartridge( CPlugin):
    def __init__(self, cfg ):
        self.cfg = cfg

        shelp  = _("Draw a simplifed cartridge containing some information about the solar/planet/DeepSky image")
        params = self.setParamCfg(cfg[0])
        nbcfg  = str(len(cfg)-1)
        gui = {
            'id'             : self.Box("SF-ADJUSTMENT", '(list %d  0 ' + nbcfg + ' 0 0 0 1 )' ),
            'inner_size'     : self.Box("SF-ADJUSTMENT", '(list %d  0 100 1 1 0 1 )'),
            'inner_color'    : self.Box("SF-COLOR"     , '(list %d %d %d )'         ),
            'outer_size'     : self.Box("SF-ADJUSTMENT", '(list %d  0 100 1 1 0 1 )'),
            'outer_color'    : self.Box("SF-COLOR"     , '(list %d %d %d )'         ),
            'gauss_radius'   : self.Box("SF-ADJUSTMENT", '(list %f  0  20 1 1 1 1 )'),
            'font'           : self.Box("SF-FONT"      , '"%s"'                     ),
            'font_color'     : self.Box("SF-COLOR"     , '(list %d %d %d )'         ),
            'font_size'      : self.Box("SF-ADJUSTMENT", '(list %d 0 100 1 1 0 1 )' ),
            "t_date"         : self.Box("SF-STRING"    , '"%s"'                     ),
            "t_time"         : self.Box("SF-STRING"    , '"%s"'                     ),
            "t_author"       : self.Box("SF-STRING"    , '"%s"'                     ),
            "t_object"       : self.Box("SF-STRING"    , '"%s"'                     ),
            "t_telescope"    : self.Box("SF-STRING"    , '"%s"'                     ),
            "t_guider"       : self.Box("SF-STRING"    , '"%s"'                     ),
            "t_camera"       : self.Box("SF-STRING"    , '"%s"'                     ),
            "t_filter"       : self.Box("SF-STRING"    , '"%s"'                     ),
            "t_exptime"      : self.Box("SF-STRING"    , '"%s"'                     ),
            "t_imgprocessing": self.Box("SF-STRING"    , '"%s"'                     ),
            "t_info"         : self.Box("SF-STRING"    , '"%s"'                     ) }

        CPlugin.__init__( self,
            Pkg+"SimplifiedCartridge"        , # name
            _("Simplidied Drawing Cartridge"), # blurb
            shelp                            , # help
            info_right                       , # author,copyright, ...
            _("Simplidied Drawing Cartridge"), # menu_label
            menu_cartridge                   , # menu path
            "RGB*, GRAY*"                    , # image_types
            params                           , # params
            gui                              ) # gui

    def setParamCfg(self,pp):
        return [
               self.Param( "INT32" , 'id'             , _("Cartridge ID"      ), pp['id'          ] ),
               self.Param( "INT32" , 'inner_size'     , _("Inner border size" ), pp['inner_size'  ] ),
               self.Param( "COLOR" , 'inner_color'    , _("Inner border color"), pp['inner_color' ] ),
               self.Param( "INT32" , 'outer_size'     , _("Outer border size" ), pp['outer_size'  ] ),
               self.Param( "COLOR" , 'outer_color'    , _("Outer border color"), pp['outer_color' ] ),
               self.Param( "FLOAT" , 'gauss_radius'   , _("Gauss Radius"      ), pp['gauss_radius'] ),
               self.Param( "STRING", 'font'           , _("Font"              ), pp['font'        ] ),
               self.Param( "COLOR" , 'font_color'     , _("Font color"        ), pp['font_color'  ] ),
               self.Param( "INT32" , 'font_size'      , _("Font size"         ), pp['font_size'   ] ),
               self.Param( "STRING", "t_date"         , _("Date:"             ), pp["date"        ] ),
               self.Param( "STRING", "t_time"         , _("Time:"             ), pp["time"        ] ),
               self.Param( "STRING", "t_author"       , _("Author:"           ), pp["author"      ] ),
               self.Param( "STRING", "t_object"       , _("Object:"           ), pp["object"      ] ),
               self.Param( "STRING", "t_telescope"    , _("Telescope:"        ), pp["telescope"   ] ),
               self.Param( "STRING", "t_guider"       , _("Guider:"           ), pp["guider"      ] ),
               self.Param( "STRING", "t_camera"       , _("Camera:"           ), pp["camera"      ] ),
               self.Param( "STRING", "t_filter"       , _("Filter:"           ), pp["filter"      ] ),
               self.Param( "STRING", "t_exptime"      , _("exposure time:"    ), pp["exptime"     ] ),
               self.Param( "STRING", "t_imgprocessing", _("Image processing:" ), pp["imgprocessing"]),
               self.Param( "STRING", "t_info"         , _("Information:"      ), pp["info"        ] )
               ] 

    def Processing(self, timg, tdrawable):
        ( uid, inner_size, inner_color, outer_size, outer_color,
          gauss_radius, font, font_color, font_size,
          t_date, t_time, t_author, t_object, t_telescope, t_guider,
          t_camera, t_filter, t_exptime, t_imgprocessing, t_info) = self.get_parameters_value()

        gimp.plugin_enable_precision() # important sinon traitement en bpp=8
        cr=check_compatibilty(self.proc_name,timg,tdrawable)
        if cr != None : return cr

        self.Param={}
        self.SetParam( int(uid),
            inner_size  , inner_color, outer_size  , outer_color ,
            gauss_radius, font       , font_color  , font_size   ,
            t_date      , t_time     , t_author    , t_object    ,
            t_telescope , t_guider   , t_camera    , t_filter    ,
            t_exptime   , t_imgprocessing, t_info)
        self.saveScm(uid,self.params,True)
        self.build_cartridge(timg, tdrawable)

        return ( "SUCCESS","")

    def CreerPluginScm(self,guiname="",force=False):
        _unused=(guiname) # unused parameter , suppress pylint warning
        for pp in self.cfg :
            self.saveScm(pp['id'], self.setParamCfg(pp),force )
        return ""

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # Sauve les donnees de chaque cartouche
    def saveScm(self, uid, pp, force=True ):
        filename = Pkg+"SimplifiedCartridge%d.scm"%(uid,)
        if dir_scriptfu != "" :
            filename = os.path.join( dir_scriptfu, filename )

        if os.path.isfile(filename) and force == False :
            return

        memo_plugin_info = self.plugin_info
        memo_params = self.params

        guiname = self.cfg[uid]['SF']['CmdName']
        self.plugin_info[1] = self.cfg[uid]['SF']['Info']
        self.plugin_info[2] = self.cfg[uid]['SF']['Help']
        self.plugin_info[3] = self.cfg[uid]['SF']['MenuName']
        self.params = pp
        scm_txt=CPlugin.pluginScm(self,guiname)
        try:
            with open(filename ,"w") as fd :
                fd.write( scm_txt )
        except Exception as e :
            print(e)
            gimp.message(e)

        self.params = memo_params
        self.plugin_info = memo_plugin_info
        return

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # Construction des bordures de l'image
    def mkBorderLayer( self, timg,
                border_size, border_color,
                info_height, layer_name, layers_number ) :

        current_width  = gimp.image_width(timg)
        current_height = gimp.image_height(timg)
        gimp.image_resize( timg, int(current_width+(2*border_size)),
                                     int(current_height+(2*border_size)+info_height),
                                     int(border_size),int(border_size) )
        new_width  = gimp.image_width(timg)
        new_height = gimp.image_height(timg)
        border_layer = gimp.layer_new(timg,layer_name,new_width, new_height,
                                           "RGBA_IMAGE",100.0, "NORMAL")
        gimp.image_insert_layer( timg, border_layer, None, layers_number )
        gimp.image_set_active_layer( timg, border_layer)
        gimp.context_set_foreground( border_color)
        gimp.drawable_fill( gimp.image_get_active_drawable(timg), "FOREGROUND_FILL")
        return border_layer

    def FormatText(self):
        pp = self.Param
        line0=""
        line1=""
        line2=""
        line3=""
        line4=""
        line5=""
        line6=""

        # line0
        if  len(pp["object"]) > 0 :
            line0 += pp["object"] + "\n"

        # line1
        if  len(pp["time"]) > 0 :
            line1 += pp["time"]
        if  len(pp["date"]) > 0 :
            if len(line1) > 0 :
                line1 += "  "
            line1 += pp["date"]
        if  len(pp["author"]) > 0 :
            if len(line1) > 0 :
                line1 += " - "
            line1 += pp["author"]

        # line2
        if len(pp["telescope"]) > 0 :
            line2 += pp["telescope"]
        if len(pp["camera"]) > 0 :
            if len(line2) > 0 :
                line2 += " + "
            line2 += pp["camera"]

        # line2
        if len(pp["guider"]) > 0 :
            line3 += pp["guider"]

        # line4
        if len(pp["filter"]) > 0 :
            line4 += pp["filter"]
        if len(pp["exptime"]) > 0 :
            if len(line4) > 0 :
                line4 += " - "
            line4 += pp["exptime"]

        # line5
        if  len(pp["imgprocessing"]) > 0 :
            line5 += pp["imgprocessing"]

        # line6
        if  len(pp["info"]) > 0 :
            line6 += pp["info"]
        if len(line6) > 0 : line6+= "\n"

        line = line0
        for ll in (line1, line2, line3, line4, line5, line6) :
            if len(ll) > 0 :
                if len(line) > 0 :
                    line += "\n"
                line += ll

        return line

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # Construction du cartouche
    def build_cartridge(self, timg, tdrawable ) :
        pp = self.Param

        if pp['font_size'] == 0 :
            return

        text = self.FormatText()
        bb_txt = gimp.text_get_extents_fontname(text, pp['font_size'], "PIXELS", pp['font'])[1]
        info_height = bb_txt[1]

        # --------------------------------------------------------------------------
        # Original Image width and height
        image_width  = gimp.image_width(timg)
        image_height = gimp.image_height(timg)

        # bounding box info (x,y,w,h)
        bb_info = ( pp['outer_size'] , pp['outer_size']+image_height+3*pp['inner_size'],
                    image_width, info_height)

        gimp.image_undo_group_start(timg)

        if not gimp.drawable_is_rgb(tdrawable):
            gimp.image_convert_rgb(timg)

        # Add alpha channel to all layers, so we can move our information layer to the bottom
        layers=gimp.get_layers(timg)
        for layer in layers :
            gimp.layer_add_alpha(layer)

        layers=gimp.get_layers(timg)
        layers_number = len(layers)

        # Backup foreground color
        color_tmp = gimp.context_get_foreground()[1]

        # --------------------------------------------------------------------------
        # Add border 1 as layer
        border1_layer = self.mkBorderLayer( timg, pp['inner_size'], pp['inner_color'],
                                       0, "border in", layers_number )

        # --------------------------------------------------------------------------
        # Add border 2 as layer
        border2_layer = self.mkBorderLayer( timg, pp['outer_size'], pp['outer_color'],
                                       info_height-pp['outer_size']+2*pp['inner_size'],
                                       "border out + Information",
                                       layers_number+1 )

        # --------------------------------------------------------------------------
        # Text
        # --------------------------------------------------------------------------
        gimp.context_set_foreground( pp['font_color'] )
        tlayer =gimp.text_fontname(timg,-1, bb_info[0],bb_info[1],text,  -1, True,
                                   pp['font_size'], "PIXELS", pp['font'])

        # --------------------------------------------------------------------------
        # Move it to the bottom
        gimp.image_lower_layer_to_bottom(timg, border1_layer)
        gimp.image_lower_layer_to_bottom(timg, border2_layer)

        # --------------------------------------------------------------------------
        # Merge all layers into one (only merge text layers if they were created;
        # empty text layers are not created. merge the resulting layer (might be border1_layer) down.
        merge_layer = gimp.image_merge_down( timg, border1_layer, "EXPAND_AS_NECESSARY")

        # --------------------------------------------------------------------------
        # smooth inner border -> outer border
        if pp['gauss_radius'] > 0 :
            gauss( timg, merge_layer, pp['gauss_radius'], pp['gauss_radius'], 0)

        merge_layer = gimp.image_merge_down( timg,tlayer, "EXPAND_AS_NECESSARY")

        gimp.context_set_foreground( color_tmp )

        gimp.image_undo_group_end(timg)
        gimp.displays_flush()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # Met a  jour les donnees interne du cartouche
    def SetParam(self, uid,
            inner_size  , inner_color, outer_size  , outer_color ,
            gauss_radius, font       , font_color  , font_size   ,
            t_date      , t_time     , t_author    , t_object    ,
            t_telescope , t_guider   , t_camera    , t_filter    ,
            t_exptime   , t_imgprocessing, t_info ):

        if sys.version_info[0] >= 3 :
            code='utf-8'
            font              = font.decode(code,errors='ignore')
            t_date            = t_date.decode(code,errors='ignore')
            t_time            = t_time.decode(code,errors='ignore')
            t_author          = t_author.decode(code,errors='ignore')
            t_object          = t_object.decode(code,errors='ignore')
            t_telescope       = t_telescope.decode(code,errors='ignore')
            t_guider          = t_guider.decode(code,errors='ignore')
            t_camera          = t_camera.decode(code,errors='ignore')
            t_filter          = t_filter.decode(code,errors='ignore')
            t_exptime         = t_exptime.decode(code,errors='ignore')
            t_imgprocessing   = t_imgprocessing.decode(code,errors='ignore')
            t_info            = t_info.decode(code,errors='ignore')

        Param={}
        Param["id"  ]= uid
        Param["inner_size"    ]= inner_size
        Param["inner_color"   ]= inner_color
        Param["outer_size"    ]= outer_size
        Param["outer_color"   ]= outer_color
        Param["gauss_radius"  ]= gauss_radius
        Param["font"          ]= font
        Param["font_color"    ]= font_color
        Param["font_size"     ]= font_size
        Param["date"          ]= t_date
        Param["time"          ]= t_time
        Param["author"        ]= t_author
        Param["object"        ]= t_object
        Param["telescope"     ]= t_telescope
        Param["guider"        ]= t_guider
        Param["camera"        ]= t_camera
        Param["filter"        ]= t_filter
        Param["exptime"       ]= t_exptime
        Param["imgprocessing" ]= t_imgprocessing
        Param["info"          ]= t_info
        self.Param = Param

# ------------------------------------------------------------------------------
def InitParam():
    maintenant    = datetime.now()
    strdate       = maintenant.strftime("%d-%m-%Y")
    strtime       = maintenant.strftime("%H:%M")
    Noir =(0.0,0.0,0.0,0.0)
    Blanc=(1.0,1.0,1.0,0.0)

    parameters= {}
    parameters["id"  ]        = 0
    parameters["inner_size"  ]= 2
    parameters["inner_color" ]= Blanc
    parameters["outer_size"  ]= 12
    parameters["outer_color" ]= Noir
    parameters["gauss_radius"]= 0
    parameters["info_height" ]= 50
    parameters["font"        ]= "Courier"
    parameters["font_color"  ]= Blanc
    parameters["font_size"   ]= 12
    parameters["date"]          = strdate
    parameters["time"]          = strtime
    parameters["author"]        = "M27trognondepomme"
    parameters["object"]        = ""
    parameters["telescope"]     = ""
    parameters["guider"]        = ""
    parameters["camera"]        = ""
    parameters["filter"]        = ""
    parameters["exptime"]       = ""
    parameters["imgprocessing"] = ""
    parameters["info"]          = ""

    return parameters

cfg=[]
prefixe = "o "
parameters = InitParam()
parameters["id"  ]= 0
parameters["SF"] ={
        'CmdName' : "SimplifiedSolarCartridge",
        'MenuName': prefixe + _("Sun Spot"),
        'Info'    : _("Cartridge containing information about the solar image"),
        'Help'    : _("Draw a simplified cartridge containing some information about the solar image") }
parameters["object"]        = _("Sun spot ARxxxx")
parameters["telescope"]     = "Helioscope T150/1200mm + HEQ5"
parameters["camera"]        = "ASI183MM"
parameters["filter"]        = "Filtres ND3 + Continuum"
parameters["imgprocessing"] = "traitement AstroSuface + Gimp2.10"
cfg.append( parameters )

parameters = InitParam()
parameters["id"  ]= 1
parameters["SF"] ={
        'CmdName' : "SimplifiedSolarCartridgeHa",
        'MenuName': prefixe + _("Sun Spot") + " Ha",
        'Info'    : _("Cartridge containing information about the Halpha solar image"),
        'Help'    : _("Draw a simplified cartridge containing some information about the Halpha solar image") }
parameters["object"]        = _("Sun spot ARxxxx")
parameters["telescope"]     = "Lunette SW 80ED600 + HEQ5"
parameters["camera"]        = "QHY-III 174M"
parameters["filter"]        = "Filtre DayStar Quark 0.6nm (protubérance)"
parameters["imgprocessing"] = "traitement AstroSuface + Gimp2.10"
cfg.append( parameters )

parameters = InitParam()
parameters["id"  ]= 2
parameters["SF"] ={
        'CmdName' : "SimplifiedPlanetCartridge",
        'MenuName': prefixe + _("Planet"),
        'Info'    : _("Cartridge containing information about the planet image"),
        'Help'    : _("Draw a simplified cartridge containing some information about the planet image") }
parameters["object"]        = _("Planet")
parameters["telescope"]     = "Celestron C8 + barlow x? "
parameters["camera"]        = "GPCAM224C"
parameters["imgprocessing"] = "AS3! + R6 + Gimp2.10"
cfg.append( parameters )

parameters = InitParam()
parameters["id"  ]= 3
parameters["SF"] ={
        'CmdName' : "SimplifiedDeepSkyCartouche",
        'MenuName': prefixe + _("Deep Sky"),
        'Info'    : _("Cartridge containing Deep Sky Image information"),
        'Help'    : _("Draw a simplified cartridge containing some information about the deep sky image") }
parameters["object"]        = _("Object")
parameters["telescope"]     = "Telescope Carbon D 150mm/600mm F/D=4 + correcteur + HEQ5"
parameters["guider"]        = "Autoguidage GPCAM224C + Lunette SW50ED/242mm + PHD2guider"
parameters["camera"]        = "ASI183MM"
parameters["filter"]        = "L, R, G, B, Ha"
parameters["exptime"]       = "L:?x180s , RVB: ?x180s, Ha:?x180s"
parameters["imgprocessing"] = "SIRIL + Gimp2.10"
cfg.append( parameters )

parameters = InitParam()
parameters["id"  ]= 4
parameters["SF"] ={
        'CmdName' : "SimplifiedUserCartridge",
        'MenuName': prefixe + _("User"),
        'Info'    : _("Cartridge containing Deep Sky Image (SLA) information"),
        'Help'    : _("Draw a simplified cartridge containing some information about the deep sky image") }
parameters["object"]        = _("Object")
parameters["telescope"]     = "Lunette EQUINOX SW80ED500 + correcteur 0.85 + AVALON"
parameters["guider"]        = "Autoguidage ASI224C + Lunette SW80/400 + PHD2guider"
parameters["camera"]        = "ASI071"
parameters["filter"]        = "L, R, G, B, Ha"
parameters["exptime"]       = "L:?x180s , RVB: ?x180s, Ha:?x180s"
parameters["imgprocessing"] = "SIRIL + Gimp2.10"
parameters["author"       ] = "Gweltaz"
cfg.append( parameters )

app = CSimplifiedDrawingCartridge(cfg)
app.CreerPluginScm()
app.register()
