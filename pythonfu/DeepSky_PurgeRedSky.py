#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# ------------------------------------------------------------------------------
# This program is provided without any guarantee.
#
# The license is  LGPL-v3
# For details, see GNU General Public License, version 3 or later.
#                        "https://www.gnu.org/licenses/gpl.html"
# ------------------------------------------------------------------------------
#
#   Python-Purge-Red-Sky - removes red sky light pollution.
#   Copyright (C) 2016  Bill Smith
#

import sys
import os

# ------------------------------------------------------------------------------
# import local lib
path_exec=os.path.dirname(os.path.dirname( __file__ ))
if not path_exec :
    path_exec='..'
sys.path.append(path_exec)
sys.path.append(os.path.join(path_exec,"gimp2"))
sys.path.append(os.path.join(path_exec,"lib"))
dir_scriptfu = os.path.join(path_exec, "scriptfu")

from pyGapM27 import Pkg,bullet,menu_noise,init_language
from pyGapM27 import info_right1 as info_right
init_language()

import gp2_func  as gimp
from gp2_plugin    import CPlugin
from libTools      import check_compatibilty
from libTools      import copy_layer,create_layer,fill_layer
from plug_in       import gauss_iir

class CPurgeRedSky( CPlugin):    
    def __init__(self):
        shelp  = (_("Removes red sky light pollution") + ":"
               , bullet + _("Set the Clip limit on sky colour")
               , bullet + _("Set the Gaussian blurring parameter")
               , bullet + _("Set the Strength of red sky correction") )
        params = [
            self.Param("FLOAT" , "clip" , _('Red sky level'), 0.0),
            self.Param("INT32" , "blur" , _('Gaussian blur'), 0  ),
            self.Param("FLOAT" , "blend", _('Strength')     , 0.0),
            self.Param("INT32" , "pack" , _('Flatten image'), "" ) ]
        gui = {
            "clip" : self.Box("SF-ADJUSTMENT", '(list 25.0 1.0 100.00  1.0 10.0 2 0 )') ,
            "blur" : self.Box("SF-ADJUSTMENT", '(list 75.0 50.0 100.00 1.0 10.0 2 0 )') ,
            "blend": self.Box("SF-ADJUSTMENT", '(list 25.0 1.0 100.00  1.0 10.0 2 0 )') ,
            "pack" : self.Box("SF-TOGGLE"    , 'TRUE'                                 ) }

        CPlugin.__init__( self,
            Pkg+"PurgeRedSky"                   , # name
            _("Removes red sky light pollution"), # blurb
            shelp                               , # help
            info_right                          , # author,copyright, ...
            _("Removes red sky light pollution"), # menu_label
            menu_noise                          , # menu path
            "RGB*"                              , # image_types
            params                              , # params
            gui, dir_scriptfu                   ) # gui
            
    def Processing(self, timg, tdrawable):
        ''' This GIMP plugin removes red sky light pollution.

        Parameters:
        timg     : The currently selected image.
        tdrawable: The layer of the currently selected image.
        clip     : Clip limit on sky colour
        blur     : Gaussian blurring parameter
        blend    : Strength of red sky correction
        pack     : Flatten image at end Y/N
        '''
        (img, layer) = (timg, tdrawable)
        (clip,blur,blend,pack) = self.get_parameters_value()

        gimp.plugin_enable_precision() # important sinon traitement en bpp=8
        cr=check_compatibilty(self.proc_name,timg,tdrawable)
        if cr != None : return cr

        cmax    = clip / 100.0
        blur   = float(blur)
        blend = float(blend)
        
        mode_difference = "DIFFERENCE"
        mode_subtract   = "SUBTRACT"
        mode_merge      = "EXPAND_AS_NECESSARY"
        
        gimp.context_push()        
        gimp.image_undo_group_start(img)
        
        copy_layer(img, layer , "layer1", -1 )
        copy_layer(img, layer , "layer2", -1 )  
              
        mask = create_layer(img, layer,"Mask ...", mode_subtract, -1 )
        fill_layer(mask,(cmax, cmax, cmax))             
        layer_merge = gimp.image_merge_down(img, mask, mode_merge )        
        
        gimp.layer_set_mode(layer_merge, mode_subtract)
        layer_merge = gimp.image_merge_down(img, layer_merge, mode_merge)
        
        gauss_iir(img , layer_merge, blur, True, True)
        gimp.layer_set_opacity(layer_merge, blend)
        gimp.layer_set_mode(layer_merge, mode_difference)
        
        if pack:
            gimp.image_merge_down(img, layer_merge, mode_merge)

        gimp.progress_end()
        
        gimp.image_undo_group_end(img)       
        gimp.context_pop()
        gimp.displays_flush()

        return ( "SUCCESS", "")

app=CPurgeRedSky()
app.CreerPluginScm()
app.register()
